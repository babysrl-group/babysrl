# BabySRL
### Semantic Role Labeling from Scratch

This project is a continuation of the BabySRL [project by Mike Connor](http://cogcomp.cs.illinois.edu/page/publication_view/715) in collaboration with Cynthia Fisher and Dan Roth.

The dateset for the project can be downloaded from [this link](https://www.caitlincassidy.net/babysrl-data.zip).

Once unzipped, place under a directory named `data` under the root folder.

Make sure [maven](https://maven.apache.org/install.html) is installed. Compile by running:
```
mvn clean compile
```

To run all the experiments reported in the paper (using the basic configuration in `config/babySRL.properties`) 
use the following script:
```
sh scripts/run-all-expt-paper.sh
```