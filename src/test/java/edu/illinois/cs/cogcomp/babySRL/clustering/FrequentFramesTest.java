package edu.illinois.cs.cogcomp.babySRL.clustering;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class FrequentFramesTest {
	private BabySRLCorpus corpus;

	@Before
	public void setUp() throws Exception {
		corpus = new BabySRLCorpus();
		corpus.readFile("src/test/resources/test.adam", BabySRLCorpus.MultiplePredicates.all);
	}

	@Test
	public void testGenerateFrames() throws Exception {
		int topNFrames = 45;
		FrequentFrames ff = new FrequentFrames(topNFrames);
		ff.generateFrames(corpus);
		assertEquals(1400, ff.getSize());
		FrequentFrames.Frame frame = new FrequentFrames.Frame("going", "you", "to");
		assertEquals("you X to", frame.toString());
		assertEquals("have", ff.getTopWord(frame));
		assertEquals(4, ff.getAllFrames("it").size());
	}
}