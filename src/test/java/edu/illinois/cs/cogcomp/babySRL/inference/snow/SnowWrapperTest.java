package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import org.junit.After;
import org.junit.Test;

import static org.junit.Assert.*;

public class SnowWrapperTest {

    private final String netFile = "src/test/resources/test.net";

    @Test
    public void testSnow() throws Exception {
        try {
            SnowWrapper snowWrapper = SnowWrapper.buildWrapper(new Pair<>(1, 3), netFile, true);
            int best = snowWrapper.predictBest("3,4");
            assertEquals(1, best);
            snowWrapper.close();
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @After
    public void cleanUp() throws Exception {
        IOUtils.rm(netFile);
    }
}