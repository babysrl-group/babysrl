package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;

/**
 * A {@link SnowClassifier} with the NPat features (word, predicate-word, noun pattern)
 *
 * @author Christos Christodoulopoulos
 */
public class NCountClassifierSnow extends SnowClassifier {

    public NCountClassifierSnow() {
        super("ncnt", FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_COUNT);
    }
}
