package edu.illinois.cs.cogcomp.babySRL.clustering;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An abstract class for HMM/Brown/BMMM clustering methods. Provides a common interface and allows for
 * restricting the induced tag assignments (see {@link #reTagCorpus(List, List)}).
 */
public abstract class Tagger {
    public abstract void tagCorpus(BabySRLCorpus corpus, String inputFile)  throws IOException;

    protected static void tagCorpusHardClusters(BabySRLCorpus corpus, Map<String, String> wordTagMap, String mostFreqTag) {
        int unkWords = 0;
        for (BabySRLSentence s : corpus.getSentences()) {
            List<String> tags = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String word = s.getWord(wordInd);
                String tag = mostFreqTag;
                if (wordTagMap.containsKey(word)) tag = wordTagMap.get(word);
                else unkWords++;
                tags.add(tag);
            }
            s.setHmmTags(tags);
        }
        System.out.println(unkWords + " unknown words");
    }

    /**
     * Cleans up the automatically tagged corpus (from {@link #tagCorpus(BabySRLCorpus, String)}).
     * We look for the number of times each word is tagged with a specific tag; words that appear with a tag
     * fewer times than a percentage of the their most frequent tag are marked as not consistently tagged,
     * and are re-tagged to a designated "bin" tag.
     * @param trainSentences The corpus to collect the counts from
     * @param testSentences The corpus to re-tag
     * @return The number of words that were designated to the "bin" tag.
     */
    public static Counter<String> reTagCorpus(List<BabySRLSentence> trainSentences, List<BabySRLSentence> testSentences) {
        double threshold = BabySRLProperties.getInstance().getRetagThreshold();
        Map<String, Counter<String>> tagCounter = new HashMap<>();
        // Collect the counts from the training corpus
        collectCounts(trainSentences, tagCounter);
        // Add the counts of the main corpus
        collectCounts(testSentences, tagCounter);

        Counter<String> wordsRemoved = new Counter<>();
        for (BabySRLSentence s : testSentences) {
            List<String> newTags = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String word = s.getWord(wordInd);
                String tag = s.getHmmTag(wordInd);
                Counter<String> counter = tagCounter.get(word);
                double tagCount = counter.getCount(tag);
                double maxCount = counter.getMax().getSecond();
                if (tagCount/maxCount > threshold)
                    newTags.add(tag);
                else {
                    newTags.add("-1");
                    wordsRemoved.incrementCount(s.getGoldTag(wordInd));
                }
            }
            s.setHmmTags(newTags);
        }
        return wordsRemoved;
    }

    private static void collectCounts(List<BabySRLSentence> sentences, Map<String, Counter<String>> tagCounter) {
        for (BabySRLSentence s : sentences) {
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String word = s.getWord(wordInd);
                String tag = s.getHmmTag(wordInd);
                Counter<String> counter;
                if (tagCounter.containsKey(word))
                    counter = tagCounter.get(word);
                else counter = new Counter<>();
                counter.incrementCount(tag);
                tagCounter.put(word, counter);
            }
        }
    }
}
