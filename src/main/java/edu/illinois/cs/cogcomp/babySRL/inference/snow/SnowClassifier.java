package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;
import edu.illinois.cs.cogcomp.babySRL.inference.Classifier;
import edu.illinois.cs.cogcomp.babySRL.inference.FeatureExtractor;
import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;

import java.io.IOException;
import java.util.Map;

public class SnowClassifier extends Classifier {
    private final SnowWrapper snowWrapper;
    private FeatureTypes[] featureSet;

    /**
     * This lets me use the feature set for training (but not the classifiers themselves);
     * then I need to re-initialise the classifiers, this time for prediction
     *
     * @return The set of {@link SnowClassifier}s to use during training/evaluation
     */
    public static Classifier[] getClassifierSet() {
        return new SnowClassifier[]{
                new LexClassifierSnow(),
                new NPatClassifierSnow(),
                new NCountClassifierSnow(),
                new VPosClassifierSnow(),
                new VPosRandClassifierSnow(),
                new CmbClassifierSnow()
        };
    }

    public SnowClassifier(String name, FeatureTypes... featureSet) {
        this.featureSet = featureSet;
        this.fex = new FeatureExtractor(featureSet, true);
        this.name = name;
        Pair<Integer, Integer> learningTargets = fex.getLearningTargets();
        String networkFile = fex.getNetworkFileName();
        try {
            // This is always a prediction classifier, so don't override the training file
            this.snowWrapper = SnowWrapper.buildWrapper(learningTargets, networkFile, false);
        } catch (IOException | InterruptedException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String prediction(BabySRLArgumentInstance instance) {
        String featureVector = fex.getFeatureString(instance);
        int prediction;
        try {
            prediction = snowWrapper.predictBest(featureVector);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return fex.decode(prediction);
    }

    public FeatureTypes[] getFeatureSet() {
        return featureSet;
    }

    @Override
    public Pair<String, Double> predictionWithScore(BabySRLArgumentInstance instance) {
        String featureVector = fex.getFeatureString(instance);
        Pair<Integer, Double> pair;
        try {
            pair = snowWrapper.predictBestWithActivation(featureVector);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return new Pair<>(fex.decode(pair.getFirst()), pair.getSecond());
    }

    @Override
    public Map<String, Double> predict(BabySRLArgumentInstance instance) throws IOException {
        return null;
    }

    @Override
    public void promote(BabySRLArgumentInstance instance, String label) {

    }

    @Override
    public void demote(BabySRLArgumentInstance instance, String label) {

    }

    @Override
    public double getWeight(String label, String feature) {
        // Not implemented
        return 0.0;
    }

    public void close() throws IOException, InterruptedException {
        snowWrapper.close();
    }
}
