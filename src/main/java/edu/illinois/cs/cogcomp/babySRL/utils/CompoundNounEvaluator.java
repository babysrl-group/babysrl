package edu.illinois.cs.cogcomp.babySRL.utils;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class CompoundNounEvaluator {
    private static final BabySRLProperties properties = BabySRLProperties.getInstance();

    public static void main(String[] args) throws FileNotFoundException {
        String child = "adam";
        String resultsDir = "experiments/gold-noCV-stateAggr-noFix-none/temp" + File.separator;

        BabySRLCorpus corpus = new BabySRLCorpus();
        corpus.readFile(properties.getTrainTestDir() + File.separator + "test." + child);

        for (int seedNouns = 5; seedNouns < 76; seedNouns += 5) {
            OneVariableStats tagStats = new OneVariableStats();
            OneVariableStats srlStats = new OneVariableStats();
            for (int round = 0; round < 5; round++) {
                List<String> lines = LineIO.read(resultsDir + child + ":" + seedNouns + "-" + round);
                if (corpus.getSentences().size() != (lines.size() - 3) / 5)
                    throw new RuntimeException("Corpus sizes don't match");

                List<BabySRLSentence> sentences = corpus.getSentences();
                Counter<String> matchesCounter = new Counter<>();
                for (int sentInd = 0; sentInd < sentences.size(); sentInd++) {
                    BabySRLSentence sentence = sentences.get(sentInd);
                    // There are 5 lines per sentence
                    // 3rd line: Compound nouns
                    String compoundStr = lines.get((sentInd*5)+2).replace("Compound nouns:", "").trim();
                    if (compoundStr.isEmpty()) continue;

                    evaluateSentence(matchesCounter, sentence, compoundStr);
                }
                tagStats.add(matchesCounter.getCount("tags")/matchesCounter.getCount("total"));
                srlStats.add(matchesCounter.getCount("srl")/matchesCounter.getCount("total"));
            }
            System.out.println(seedNouns + "\tseeds:\t" +
                    StringUtils.getFormattedTwoDecimal(tagStats.mean()*100) + "% tags match\t" +
                    StringUtils.getFormattedTwoDecimal(srlStats.mean()*100) + "% SRLs match");
        }
    }

    private static void evaluateSentence(Counter<String> matchesCounter, BabySRLSentence sentence, String compoundStr) {
        for (String comStr : compoundStr.split("#")) {
            String[] compounds = comStr.split(" ");
            String[] words = new String[compounds.length];
            String[] tags = new String[compounds.length];
            boolean tagsMatch = true;
            for (int cInd = 0; cInd < compounds.length; cInd++) {
                words[cInd] = compounds[cInd].split("/")[0];
                tags[cInd] = compounds[cInd].split("/")[1];
                if (cInd == 0) continue;
                tagsMatch &= tags[cInd - 1].equals(tags[cInd]);
            }
            boolean srlMatch = false;
            int startIndex = findIndex(sentence, words);
            for (int predIndex : sentence.getGoldArgumentInstances().keySet()) {
                boolean localSRLMatch = true;
                for (int cInd = 0; cInd < compounds.length; cInd++) {
                    if (cInd == 0) continue;
                    localSRLMatch &= sentence.getGoldArg(startIndex + cInd - 1, predIndex).equals(sentence.getGoldArg(startIndex + cInd, predIndex));
                }
                srlMatch |= localSRLMatch;
            }
            if (tagsMatch) matchesCounter.incrementCount("tags");
            if (srlMatch) matchesCounter.incrementCount("srl");
            matchesCounter.incrementCount("total");
        }
    }

    private static int findIndex(BabySRLSentence sentence, String[] words) {
        for (int wordInd = 0; wordInd < sentence.getWordCount(); wordInd++) {
            String sentWord = sentence.getWord(wordInd);
            if (words[0].equals(sentWord))
                if (words[1].equals(sentence.getWord(wordInd + 1)))
                    return wordInd;
        }
        return -1;
    }
}
