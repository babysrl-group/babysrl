package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.data.Sentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.babySRL.utils.NounThreshold;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.transformers.ITransformer;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Uses a small set of known nouns to determine if an HMM state can be an argument.
 * ('known' nouns come a list of concrete nouns, either animate or inanimate)
 */
public class NounIdentifier {
    private static BabySRLProperties properties = BabySRLProperties.getInstance();
    private List<String> knownNounsSubset;
    private int numSeedNouns;
    private List<String> nounStates;

    public NounIdentifier(List<BabySRLSentence> sentences, String seedNounFile, int numSeedNouns, Random rand) {
        knownNounsSubset = new ArrayList<>();
        this.numSeedNouns = numSeedNouns;
        try {
            InputStream is = new FileInputStream(seedNounFile);
            ITransformer<String, String> transformer = new ITransformer<String, String>() {
                public String transform(String line) {return line.split("\\s+")[0];}
            };
            List<String> seedNounList = LineIO.read(is, Charset.defaultCharset().name(), transformer);
            if (!properties.useRandSeeds()) {
                Counter<String> counter = new Counter<>();
                for (String noun : seedNounList) {
                    counter.incrementCount(noun);
                }
                for (Sentence s : sentences) {
                    for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                        String word = s.getWord(wordInd);
                        if (seedNounList.contains(word))
                            counter.incrementCount(word);
                    }
                }
                seedNounList = counter.getSortedItemsHighestFirst();
            }
            for (int i = 0; i < numSeedNouns; i++) {
                int index = 0;
                if (properties.useRandSeeds())
                    index = rand.nextInt(seedNounList.size());
                knownNounsSubset.add(seedNounList.get(index));
                seedNounList.remove(index);
            }
        }
        catch (IOException e) {
            System.err.println("Cannot read " + seedNounFile);
            System.exit(-1);
        }
    }

    /**
     * Generates the statistics used to predict whether a given HMM tag is a noun. Needs to be run
     * before {@link #addNounPredictions(List)} (potentially on a different corpus).
     * @param sentences The sentences to derive the statistics from
     */
    public void generateNounStats(List<BabySRLSentence> sentences) {
        // First, get the list of HMM states that are potential arguments
        nounStates = new ArrayList<>();
        // State is considered noun if it appears with a known noun >= knownNounThreshold
        Counter<String> stateNounCounts = new Counter<>();
        for (BabySRLSentence s : sentences) {
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                if (isKnownNoun(s.getWord(wordInd)))
                    stateNounCounts.incrementCount(s.getHmmTag(wordInd));
            }
        }
        for (String state : stateNounCounts.items()) {
            NounThreshold thresholdType = new NounThreshold(properties.getThresholdType());
            int nounThreshold = thresholdType.get(numSeedNouns);

            if (stateNounCounts.getCount(state) >= nounThreshold)
                nounStates.add(state);
        }
    }

    /**
     * Adds noun predictions (using the statistics generated by {@link #generateNounStats(List)})
     * @param sentences The sentences to add the predictions to
     */
    public void addNounPredictions(List<BabySRLSentence> sentences) {
        if (nounStates == null) {
            System.err.println("Noun statistics haven't been generated. Please run `generateNounStats` first.");
            System.exit(-1);
        }
        // Now go through the corpus again and add the noun information
        for (BabySRLSentence s : sentences) {
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                // If it's a known noun is should be allowed
                if (!isKnownNoun(s.getWord(wordInd)) && !isAllowedNoun(s, wordInd)) continue;
                // Seed nouns automatically get to be arguments
                if (isKnownNoun(s.getWord(wordInd)) || nounStates.contains(s.getHmmTag(wordInd)))
                    s.setPredictedNoun(wordInd);
            }
        }
    }

	/**
	 * Adds "predictions" based on the gold PoS tags (this is an oracle limit for the {@link VerbIdentifier})
	 * @param sentences The sentences to add the predictions to
	 */
	public static void addGoldNounPredictions(List<BabySRLSentence> sentences) {
		for (BabySRLSentence s : sentences) {
			for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
				if (s.isNoun(wordInd)) s.setPredictedNoun(wordInd);
			}
		}
	}

    @SuppressWarnings("RedundantIfStatement")
    static boolean isAllowedNoun(BabySRLSentence sentence, int wordInd) {
        // Ignore punctuation (from HMMStateIdent.pl, line 548)
        if (sentence.getWord(wordInd).matches("\\p{Punct}")) return false;
        // Finally, ignore all function word states
        // (the property will be -1 if no content/function word split exists)
        if (Integer.parseInt(sentence.getHmmTag(wordInd)) < properties.getFunctionWordState()) return false;
        return true;
    }

    @SuppressWarnings("RedundantIfStatement")
    boolean isKnownNoun(String candidate) {
        for (String knownNoun : knownNounsSubset) {
            if (candidate.equals(knownNoun)) return true;
            if (properties.seedPluralsMatch()) {
                String variant = BabySRLCorpus.knownVariants.get(candidate);
                if (variant != null && variant.equals(knownNoun)) {
                    return true;
                }
            }
        }
        return false;
    }
}
