package edu.illinois.cs.cogcomp.babySRL.inference;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Labels;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.data.DataHandler;
import edu.illinois.cs.cogcomp.babySRL.inference.lbjava.LBJavaClassifier;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Supervision.*;

/**
 * A programmatic trainer for SNoW/LBJava {@link Classifier}s (ran in interactive mode).
 * Right now, trains and tests at the same time.
 */
public class Trainer {
	private static BabySRLProperties properties = BabySRLProperties.getInstance();
	private static int totalTrainingExamples, skippedTrainingExamples;

    public static void train(int numRounds, DataHandler trainParser, Classifier classifier)
			throws IOException, InterruptedException {
        for (int i = 0; i < numRounds; i++) {
        	resetExampleCounts();
			for (BabySRLSentence sentence : trainParser.getTrainSentences()) {
				if (properties.getSupervision() == animacy) {
					sentence.storeAnimacyAssignments();

					// FEEDBACK LEVEL 3 (the other 2 levels happen in DataHandler.setAnimacyFeedback() lines 126-153)
					// If there are two (or more) animate nouns in the sentence:
					if (sentence.numAnimates() > 1) {
						feedbackTwoAnimates(classifier, sentence);
					}
					trainSentence(classifier, sentence);
					sentence.resetAnimacyAssignments();
                }
				else trainSentence(classifier, sentence);
			}
        }
    }

	private static void feedbackTwoAnimates(Classifier classifier, BabySRLSentence sentence) throws IOException {
		double maxActivation = -Double.MAX_VALUE;
		int maxCandidate = -1;
		// 1) we let the classifier decide (only between the two animates)
		for (BabySRLArgumentInstance trainInstance : sentence.getArgumentInstances()) {
			if (!sentence.getAnimacyLabel(trainInstance.getToken()).equals("A0")) continue;
			double activation = classifier.predict(trainInstance).get("A0");
			if (activation > maxActivation) {
				maxActivation = activation;
				maxCandidate = trainInstance.getToken();
			}
		}
		if (maxCandidate == -1) return;
		for (BabySRLArgumentInstance trainInstance : sentence.getArgumentInstances()) {
			int wordInd = trainInstance.getToken();
			if (wordInd == maxCandidate) continue;
			if (sentence.getAnimacyLabel(wordInd).equals("A0"))
				sentence.setAnimacyLabel(wordInd, "A1");
		}
	}

	public static void trainSentence(Classifier classifier, BabySRLSentence sentence) throws IOException {
		for (BabySRLArgumentInstance trainInstance : sentence.getArgumentInstances()) {
			totalTrainingExamples++;
			if (trainInstance.getLabel().equals("UNK")) {
				skippedTrainingExamples++;
				continue;
			}

			for (Map.Entry<String, Double> prediction : classifier.predict(trainInstance).entrySet()) {
				String predictedLabel = prediction.getKey();
				double activation = prediction.getValue();
				// If the label is correct, but the prediction is negative: promote
				boolean correctLabel = predictedLabel.equals(trainInstance.getLabel());
				if (correctLabel) {
					if (activation <= LBJavaClassifier.THRESHOLD + LBJavaClassifier.THICKNESS)
						classifier.promote(trainInstance, predictedLabel);
				}
				// If the label is incorrect, but the prediction is positive: demote
				else {
					if (activation >= LBJavaClassifier.THRESHOLD - LBJavaClassifier.THICKNESS)
						classifier.demote(trainInstance, predictedLabel);
				}
			}
		}
	}

	public static EvaluationRecord test(DataHandler testParser, Classifier classifier) throws IOException, InterruptedException {
        EvaluationRecord eval = new EvaluationRecord();

        for (BabySRLSentence sentence : testParser.getTestSentences()) {
			Map<Integer, List<BabySRLArgumentInstance>> goldArgumentInstances = sentence.getGoldArgumentInstances();

			for (int pred : goldArgumentInstances.keySet()) {
				if (properties.getTestLabels() == Labels.all)
					eval.incrementGold(goldArgumentInstances.get(pred).size());
				else if (properties.getTestLabels() == Labels.core)
					eval.incrementGold(sentence.getGoldCoreArgNum(pred));
				else if (properties.getTestLabels() == Labels.a0a1)
					eval.incrementGold(sentence.getGoldA0A1ArgNum(pred));
			}

			// Evaluate with unique agent constraint
			if (properties.getSupervision() == BabySRLCorpus.Supervision.animacy) {
				@SuppressWarnings("unchecked")
				Pair<String, Double>[] predictions = new Pair[sentence.getArgumentInstances().size()];
				List<BabySRLArgumentInstance> argumentInstances = sentence.getArgumentInstances();
				for (int i = 0; i < argumentInstances.size(); i++) {
					BabySRLArgumentInstance testInstance = argumentInstances.get(i);
					Pair<String, Double> pair = classifier.predictionWithScore(testInstance);
					predictions[i] = new Pair<>(pair.getFirst(), pair.getSecond());
				}
				// If there are more than one agent predictions, choose the highest scoring one
				testWithConstraint(predictions);
				for (int i = 0; i < predictions.length; i++) {
					BabySRLArgumentInstance testInstance = sentence.getArgumentInstances().get(i);
					String label = sentence.getGoldArg(testInstance.getToken(), testInstance.getPredIndex());
					String prediction = predictions[i].getFirst();
					evaluatePrediction(eval, label, prediction);
				}
			}
			else {
				for (BabySRLArgumentInstance testInstance : sentence.getArgumentInstances()) {
					String label = sentence.getGoldArg(testInstance.getToken(), testInstance.getPredIndex());
					String prediction = classifier.prediction(testInstance);
					evaluatePrediction(eval, label, prediction);
				}
			}
		}
		return eval;
    }

	static void testWithConstraint(Pair<String, Double>[] predictions) {
		int agents = 0;
		for (Pair<String, Double> p : predictions) if (p.getFirst().equals("A0")) agents++;
		if (agents > 1) {
            double maxActivation = -Double.MAX_VALUE;
            int maxCandidate = -1;
            for (int i = 0; i < predictions.length; i++) {
                Pair<String, Double> p = predictions[i];
                if (p.getFirst().equals("A0") && p.getSecond() > maxActivation) {
                    maxActivation = p.getSecond();
                    maxCandidate = i;
                }
            }
            for (int i = 0; i < predictions.length; i++) {
                if (i == maxCandidate) continue;
                predictions[i] = new Pair<>("A1", 0.0);
            }
        }
	}

	private static void evaluatePrediction(EvaluationRecord eval, String label, String prediction) {
		if (properties.getTestLabels() == Labels.all) {
            eval.incrementPredicted();
            if (label.equals(prediction))
                eval.incrementCorrect();
        }
        else if (properties.getTestLabels() == Labels.core) {
            if (prediction.matches("(B-)*A[0-4]")) {
                eval.incrementPredicted();
                if (label.equals(prediction))
                    eval.incrementCorrect();
            }
        }
        else if (properties.getTestLabels() == Labels.a0a1){
            if (prediction.matches("(B-)*A[0-1]")) {
                eval.incrementPredicted();
                if (label.equals(prediction))
                    eval.incrementCorrect();
            }
        }
	}

	public static int getSkippedTrainingExamples() {
		return skippedTrainingExamples;
	}

	public static int getTotalTrainingExamples() {
		return totalTrainingExamples;
	}

	private static void resetExampleCounts() {
		totalTrainingExamples = 0;
		skippedTrainingExamples = 0;
	}
}
