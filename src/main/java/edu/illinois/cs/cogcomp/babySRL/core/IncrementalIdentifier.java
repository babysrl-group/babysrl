package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.util.*;

public class IncrementalIdentifier {
    private static BabySRLProperties properties = BabySRLProperties.getInstance();
    private int knownNounThreshold;
    private NounIdentifier nounIdentifier;
    private List<String> nounStates;
    private Map<String, Double> verbProbabilities;
    private Counter<String> verbHistograms;
    private Counter<String> stateNounCounts;
    // Counts the number of times each state has been predicted as predicate
    private Map<String, Counter<Boolean>> predictionMap;
    private Map<String, Counter<Boolean>> randomPredictionMap;

    public IncrementalIdentifier(String seedNounFile, int numSeedNouns, Random rand) {
        knownNounThreshold = properties.getKnownNounThreshold();
        // Corpus is NULL so cannot use non random seeds
        nounIdentifier = new NounIdentifier(null, seedNounFile, numSeedNouns, rand);

        nounStates = new ArrayList<>();
        stateNounCounts = new Counter<>();
        verbProbabilities = new HashMap<>();
        // This is a histogram of verb candidates over number of arguments in a sentence
        verbHistograms = new Counter<>();
        predictionMap = new HashMap<>();
        randomPredictionMap = new HashMap<>();
    }

    public void addIncrementalPredictions(BabySRLSentence s) {
        /* NOUN PREDICTION */
        // First, get the list of HMM states that are potential nouns
        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            String word = s.getWord(wordInd);
            String hmmTag = s.getHmmTag(wordInd);
            if (nounIdentifier.isKnownNoun(word)) {
                stateNounCounts.incrementCount(hmmTag);
            }
            // Update the noun-state stats
            // State is considered noun if it appears with a known noun >= knownNounThreshold
            for (String state : stateNounCounts.items()) {
                if (stateNounCounts.getCount(state) >= knownNounThreshold && !nounStates.contains(state))
                    nounStates.add(state);
            }
            // Add noun prediction
            if (NounIdentifier.isAllowedNoun(s, wordInd) && (nounIdentifier.isKnownNoun(word) || nounStates.contains(hmmTag)))
                s.setPredictedNoun(wordInd);
        }

        /* VERB PREDICTION */
        int predictedArgs = s.getPredictedArgNum();

        List<Integer> candidates = new ArrayList<>();
        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            if (!VerbIdentifier.isAllowedVerb(s, wordInd)) continue;
            candidates.add(wordInd);
        }

        // If we need one verb/sentence we shouldn't make a prediction for sentences with more than 3 predicates
        if (predictedArgs > 3) {
            s.setPredictedVerbPred(RandomVerbIdentifier.getRandomVerbIndex(candidates));
            // TODO: 8/5/16 Missing predictions for other types of RandomVerbIdentifiers
            s.setPredictedVerbPredicatesRandom(RandomVerbIdentifier.RAND_NOUNS, RandomVerbIdentifier.getRandomVerbIndex(candidates));
            return;
        }

        int bestVerbIndex = - 1;
        double maxProb = 0;

        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            // Generate predicate histograms
            if (!VerbIdentifier.isAllowedVerb(s, wordInd)) continue;
            String hmmTag = s.getHmmTag(wordInd);
            // Increment the base frequency count
            verbHistograms.incrementCount(hmmTag);
            // Increment the candidate-argNum pattern count
            String pattern = hmmTag + "-" + predictedArgs;
            verbHistograms.incrementCount(pattern);

            // Aggregate counts into probabilities
            verbProbabilities = VerbIdentifier.getVerbProbs(verbHistograms);

            double prob = verbProbabilities.get(pattern);
            if (prob > maxProb) {
                bestVerbIndex = wordInd;
                maxProb = prob;
            }
        }
        int randomVerbIndex = RandomVerbIdentifier.getRandomVerbIndex(candidates);
        if (!properties.aggregateVerbPred()) {
            // Set predicted predicate
            if (bestVerbIndex >= 0) s.setPredictedVerbPred(bestVerbIndex);
            else s.setPredictedVerbPred(RandomVerbIdentifier.getRandomVerbIndex(candidates));
            // TODO: 8/5/16 Missing predictions for other types of RandomVerbIdentifiers
            s.setPredictedVerbPredicatesRandom(RandomVerbIdentifier.RAND_NOUNS, randomVerbIndex);
        }
        else {
            if (bestVerbIndex >= 0) VerbIdentifier.aggregatePredictions(predictionMap, s, bestVerbIndex);

            // Choose the aggregation winner immediately
            s.setPredictedVerbPred(VerbIdentifier.getBestVerbIndex(predictionMap, s));

            // Aggregate random predictions
            if (randomVerbIndex >= 0) VerbIdentifier.aggregatePredictions(randomPredictionMap, s, randomVerbIndex);
            // TODO: 8/5/16 Missing predictions for other types of RandomVerbIdentifiers
            s.setPredictedVerbPredicatesRandom(RandomVerbIdentifier.RAND_NOUNS, VerbIdentifier.getBestVerbIndex(randomPredictionMap, s));
        }
    }
}
