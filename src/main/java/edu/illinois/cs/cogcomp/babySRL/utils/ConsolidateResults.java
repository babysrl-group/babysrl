package edu.illinois.cs.cogcomp.babySRL.utils;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.transformers.ITransformer;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Code to consolidate the results of an experiment run. Creates an average of the 3 children and
 * adds all the elements in a single tab-separated file (to be used in R).
 */
public class ConsolidateResults {

    /**
     * Receives a folder containing the idividual results a single experiment and consolidates them.
     * @param args A single argument: the full path of the folder containing adam.txt eve.txt and sarah.txt.
     */
    public static void main(String[] args) throws IOException {
        String folder = args[0];
        ITransformer<String, List<Double>> line2NumbersTransformer = new ITransformer<String, List<Double>>() {
            @Override
            public List<Double> transform(String s) {
                List<Double> numbers = new ArrayList<>();
                for (String numStr : s.split("\\s+|\\t")) numbers.add(Double.parseDouble(numStr));
                return numbers;
            }
        };
        ITransformer<List<Double>, String> numbers2LineTransformer = new ITransformer<List<Double>, String>() {
            @Override
            public String transform(List<Double> l) {
                String s = "";
                s += ((int) Math.round(l.get(0))) + "\t";
                for (int i = 1; i < l.size(); i++) {
                    s += StringUtils.getFormattedTwoDecimal(l.get(i)) + "\t";
                }
                return s;
            }
        };

        List<String> adamLines = cleanFile(LineIO.read(folder + "/adam.txt"));
        List<String> eveLines = cleanFile(LineIO.read(folder + "/eve.txt"));
        List<String> sarahLines = cleanFile(LineIO.read(folder + "/sarah.txt"));
        List<String> allLines = new ArrayList<>();
        // First line contains the titles (remove leading #)
        String titleLines = adamLines.get(0);
        if (titleLines.startsWith("#"))
			titleLines = titleLines.substring(1);
        allLines.add("child\t" + titleLines);

        for (int i = 1; i < adamLines.size(); i++) {
            String adamLine = adamLines.get(i);
            allLines.add("adam\t" + adamLine);
        }
        for (int i = 1; i < adamLines.size(); i++) {
            String eveLine = eveLines.get(i);
            allLines.add("eve\t" + eveLine);
        }
        for (int i = 1; i < adamLines.size(); i++) {
            String sarahLine = sarahLines.get(i);
            allLines.add("sarah\t" + sarahLine);
        }
        for (int i = 1; i < adamLines.size(); i++) {
            List<Double> adamNumbers = line2NumbersTransformer.transform(adamLines.get(i));
            List<Double> eveNumbers = line2NumbersTransformer.transform(eveLines.get(i));
            List<Double> sarahNumbers = line2NumbersTransformer.transform(sarahLines.get(i));

            List<Double> averageNumbers = new ArrayList<>(adamNumbers.size());
            // First number is the number of seed nouns
            averageNumbers.add(adamNumbers.get(0));
            for (int num = 1; num < adamNumbers.size(); num++) {
                double adam = adamNumbers.get(num);
                double eve = eveNumbers.get(num);
                double sarah = sarahNumbers.get(num);
                averageNumbers.add((adam + eve + sarah)/3);
            }
            allLines.add("average\t" + numbers2LineTransformer.transform(averageNumbers));
        }

        LineIO.write(folder + "/all.txt", allLines);
    }

    private static List<String> cleanFile(List<String> input) {
        List<String> output = new ArrayList<>();
        for (String s : input) {
            if (s.startsWith("[") || s.startsWith("Message")) continue;
            output.add(s);
        }
        return output;
    }
}
