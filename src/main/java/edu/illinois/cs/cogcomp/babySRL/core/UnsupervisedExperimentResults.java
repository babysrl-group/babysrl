package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * A class that contains the results for each round of the unsupervised experiment and can produce aggregate statistics
 */
public class UnsupervisedExperimentResults {

    private OneVariableStats verbPrec = new OneVariableStats();
    private OneVariableStats verbRec = new OneVariableStats();
    private OneVariableStats verbF1 = new OneVariableStats();
    private Map<String, OneVariableStats> verbRandPrec = new HashMap<>();
    private Map<String, OneVariableStats> verbRandRec = new HashMap<>();
    private Map<String, OneVariableStats> verbRandF1 = new HashMap<>();

    private OneVariableStats nounPrec = new OneVariableStats();
    private OneVariableStats nounRec = new OneVariableStats();
    private OneVariableStats nounF1 = new OneVariableStats();

    private OneVariableStats avgCandidatesNum = new OneVariableStats();
    private OneVariableStats goldInCandidatesNum = new OneVariableStats();

    private OneVariableStats maxArgsFound = new OneVariableStats();

    private OneVariableStats heuristicTie = new OneVariableStats();

    private String latestVerbResults, latestNounResults, latestVerbTrueRandResults, latestVerbFunctRandResults,
            latestVerbNounRandResults, latestAvgCandidates, latestGoldInCandidatesNum, latestMaxArgsFound,
            latestHeuristicTie;

    public void addVerbIdentifierResult(EvaluationRecord record) {
        verbPrec.add(record.getPrecision());
        verbRec.add(record.getRecall());
        verbF1.add(record.getF1());
        latestVerbResults = format(record.getPrecision()) + "\t" + format(record.getRecall()) + "\t" + format(record.getF1());
    }
    public void addNounIdentifierResult(EvaluationRecord record) {
        nounPrec.add(record.getPrecision());
        nounRec.add(record.getRecall());
        nounF1.add(record.getF1());
        latestNounResults = format(record.getPrecision()) + "\t" + format(record.getRecall()) + "\t" + format(record.getF1());
    }
    public void addVerbIdentifierRandResult(Map<String, EvaluationRecord> records) {
        addVerbIdentifierRandResult(records, RandomVerbIdentifier.RAND_TRUE);
        latestVerbTrueRandResults = formatLatest(records.get(RandomVerbIdentifier.RAND_TRUE));
        addVerbIdentifierRandResult(records, RandomVerbIdentifier.RAND_FUNCTION_WORDS);
        latestVerbFunctRandResults = formatLatest(records.get(RandomVerbIdentifier.RAND_FUNCTION_WORDS));
        addVerbIdentifierRandResult(records, RandomVerbIdentifier.RAND_NOUNS);
        latestVerbNounRandResults = formatLatest(records.get(RandomVerbIdentifier.RAND_NOUNS));
    }
    private void addVerbIdentifierRandResult(Map<String, EvaluationRecord> records, String identifier) {
        EvaluationRecord record = records.get(identifier);
        if (!verbRandPrec.containsKey(identifier))
            verbRandPrec.put(identifier, new OneVariableStats());
        if (!verbRandRec.containsKey(identifier))
            verbRandRec.put(identifier, new OneVariableStats());
        if (!verbRandF1.containsKey(identifier))
            verbRandF1.put(identifier, new OneVariableStats());
        verbRandPrec.get(identifier).add(record.getPrecision());
        verbRandRec.get(identifier).add(record.getRecall());
        verbRandF1.get(identifier).add(record.getF1());
    }

    public void addAvgCandidatesNum(double v) {
        avgCandidatesNum.add(v);
        latestAvgCandidates = StringUtils.getFormattedTwoDecimal(v);
    }

    public void addGoldInCandidatesNum(double v) {
        goldInCandidatesNum.add(v);
        latestGoldInCandidatesNum = StringUtils.getFormattedTwoDecimal(v);
    }

    public void addMaxArgsFound(double v) {
        maxArgsFound.add(v);
        latestMaxArgsFound = StringUtils.getFormattedTwoDecimal(v);
    }

    public void addHeuristicTieCounter(double v) {
        heuristicTie.add(v);
        latestHeuristicTie = StringUtils.getFormattedTwoDecimal(v);
    }

    public static String getMeanResultsCompactTitle() {
        return "#nouns\tnoun-P\tnoun-P-std\tnoun-R\tnoun-R-std\tnoun-F\tnoun-F-std\tverb-P\tverb-P-std\tverb-R\t" +
                "verb-R-std\tverb-F\tverb-F-std\t" +
                "verbRandTrue-P\tverbRandTrue-P-std\tverbRandTrue-R\tverbRandTrue-R-std\tverbRandTrue-F\tverbRandTrue-F-std\t" +
                "verbRandFunction-P\tverbRandFunction-P-std\tverbRandFunction-R\tverbRadFunction-R-std\tverbRandFunction-F\tverbRandFunction-F-std\t" +
                "verbRandNoun-P\tverbRandNoun-P-std\tverbRandNoun-R\tverbRadNoun-R-std\tverbRandNoun-F\tverbRandNoun-F-std\t" +
                "avg-candidates\tavg-candidates-std\tgoldInCandidates\tgoldInCandidates-std\tmaxArgFound\tmaxArgFound-std\t" +
                "heuristicsTie\theuristicsTie-std";
    }

    public static String getLatestMeanResultsCompactTitle() {
        return "#nouns\tnoun-P\tnoun-R\tnoun-F\tverb-P\tverb-R\tverb-F\t" +
                "verbRandTrue-P\tverbRandTrue-R\tverbRandTrue-F\t" +
                "verbRandFunction-P\tverbRandFunction-R\tverbRandFunction-F\t" +
                "verbRandNoun-P\tverbRandNoun-R\tverbRandNoun-F\t" +
                "avg-candidates\tgoldInCandidates\tmaxArgFound\theuristicsTie";
    }

    public String getMeanResultsCompact() {
        return getNounIdentifierMean(true) + "\t" +
                getVerbIdentifierMean(true) + "\t" +
                getVerbIdentifierRandMean(RandomVerbIdentifier.RAND_TRUE, true) + "\t" +
                getVerbIdentifierRandMean(RandomVerbIdentifier.RAND_FUNCTION_WORDS, true) + "\t" +
                getVerbIdentifierRandMean(RandomVerbIdentifier.RAND_NOUNS, true) + "\t" +
                StringUtils.getFormattedTwoDecimal(avgCandidatesNum.mean()) + "\t" +
                StringUtils.getFormattedTwoDecimal(avgCandidatesNum.stdErr()) + "\t" +
                format(goldInCandidatesNum.mean()) + "\t" +
                format(goldInCandidatesNum.stdErr()) + "\t" +
                format(maxArgsFound.mean()) + "\t" +
                format(maxArgsFound.stdErr()) + "\t" +
                format(heuristicTie.mean()) + "\t" +
                format(heuristicTie.stdErr());
    }

    public String getLatestResultsCompact() {
        return latestNounResults + "\t" + latestVerbResults + "\t" + latestVerbTrueRandResults + "\t" + latestVerbFunctRandResults + "\t" +
                latestVerbNounRandResults + "\t" + latestAvgCandidates + "\t" + latestGoldInCandidatesNum + "\t" + latestMaxArgsFound + "\t" + latestHeuristicTie;
    }

    private String formatLatest(EvaluationRecord record) {
        return format(record.getPrecision()) + "\t" + format(record.getRecall()) + "\t" + format(record.getF1());
    }

	private String format(double num) {
		return StringUtils.getFormattedTwoDecimal(num*100);
	}

    private String getVerbIdentifierMean(boolean compact) {
        return getMean(compact, true, verbPrec, verbRec, verbF1);
    }
    private String getVerbIdentifierRandMean(String identifier, boolean compact) {
        return getMean(compact, true, verbRandPrec.get(identifier), verbRandRec.get(identifier), verbRandF1.get(identifier));
    }
    private String getNounIdentifierMean(boolean compact) {
        return getMean(compact, true, nounPrec, nounRec, nounF1);
    }

	private String getMean(boolean compact, boolean addStd, OneVariableStats prec, OneVariableStats rec, OneVariableStats f1) {
		String result = "";
		if (compact) {
            if (addStd) return format(prec.mean()) + "\t" + format(prec.stdErr()) + "\t" +
                    format(rec.mean()) + "\t" + format(rec.stdErr()) + "\t" +
                    format(f1.mean()) + "\t" + format(f1.stdErr());
            else return format(prec.mean()) + "\t" + format(rec.mean()) + "\t" + format(f1.mean());
        }

		result += "P\t" + format(prec.mean()) + "\tstdErr\t" + format(prec.stdErr()) + "\n";
		result += "R\t" + format(rec.mean()) + "\tstdErr\t" + format(rec.stdErr()) + "\n";
		result += "F1\t" + format(f1.mean()) + "\tstdErr\t" + format(f1.stdErr());
		return result;
	}
}
