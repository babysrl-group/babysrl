package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;

/**
 * A {@link SnowClassifier} with the VPos features (word, predicate-word, verb position)
 *
 * @author Christos Christodoulopoulos
 */
public class VPosRandClassifierSnow extends SnowClassifier {

    public VPosRandClassifierSnow() {
        super("vpos-rand", FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_RANDOM_VERB_POSITION);
    }
}
