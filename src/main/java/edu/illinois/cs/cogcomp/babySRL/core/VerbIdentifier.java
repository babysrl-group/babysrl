package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.math.ArgMax;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.util.*;

/**
 * Uses the predicted argument information from {@link NounIdentifier} to determine
 * whether a non-argument HMM state is a predicate, based on the number of times it appears
 * with a specific number of arguments.
 * <br/>
 * NB: Only predicates that occur with 1-3 arguments are considered
 */
public class VerbIdentifier {
    private static BabySRLProperties properties = BabySRLProperties.getInstance();
    public Map<String, Double> predicateHistograms;
    private int candidatesNum = 0;
    // Captures whether the gold predicate is among the identified candidates (can only be 0 or 1)
    private int goldPredAmongCandidates = 0;

    public void generateVerbStats(List<BabySRLSentence> sentences) {
        // Get the histogram of predicate candidates over number of arguments in a sentence
        predicateHistograms = getPredicateHistograms(sentences);
    }

    public void addVerbPredictions(List<BabySRLSentence> sentences) {
		// Counts the number of times each state has been predicted as predicate
		Map<String, Counter<Boolean>> predictionMap = new HashMap<>();

        for (BabySRLSentence s : sentences) {
            int bestVerbIndex = maxProbPredicates(s);
            // Assign candidate-related values to sentence and reset
            s.candidateNum = candidatesNum;
            s.goldPredAmongCandidates = goldPredAmongCandidates;
            candidatesNum = 0;
            goldPredAmongCandidates = 0;
            if (bestVerbIndex < 0)
                continue;
			if (!properties.aggregateVerbPred()) {
				// Set predicted predicate
                s.setPredictedVerbPred(bestVerbIndex);
			}
			else aggregatePredictions(predictionMap, s, bestVerbIndex);
        }

		// If we are aggregating the verb predictions we need to revisit the sentences and assign the
		// state with the best ratio (#times max pred/#times appeared) as our prediction
		if (properties.aggregateVerbPred()) {
			for (BabySRLSentence s : sentences) {
                int bestVerbIndex = getBestVerbIndex(predictionMap, s);
				// Set predicted predicate
				if (bestVerbIndex >= 0) s.setPredictedVerbPred(bestVerbIndex);
			}
		}
    }

    static int getBestVerbIndex(Map<String, Counter<Boolean>> predictionMap, BabySRLSentence s) {
        int bestVerbIndex = -1;
        double maxRatio = Double.MIN_VALUE;
        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            if (!isAllowedVerb(s, wordInd)) continue;
            String candidate;
            if (properties.aggregateLexical())
                candidate = s.getWord(wordInd);
            else candidate = s.getHmmTag(wordInd);
            Counter<Boolean> predCounter = predictionMap.get(candidate);
            double ratio = predCounter.getCount(true)/predCounter.getTotal();
            if (ratio > maxRatio) {
                maxRatio = ratio;
                bestVerbIndex = wordInd;
            }
        }
        return bestVerbIndex;
    }

    static void aggregatePredictions(Map<String, Counter<Boolean>> predictionMap, BabySRLSentence s, int predIndex) {
        String prediction;
        if (properties.aggregateLexical())
            prediction = s.getWord(predIndex);
        else prediction = s.getHmmTag(predIndex);
        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            if (!isAllowedVerb(s, wordInd)) continue;
            String candidate;
            if (properties.aggregateLexical())
                candidate = s.getWord(wordInd);
            else candidate = s.getHmmTag(wordInd);

            Counter<Boolean> predCounter;
            if (predictionMap.containsKey(candidate))
                predCounter = predictionMap.get(candidate);
            else predCounter = new Counter<>();

            predCounter.incrementCount(candidate.equals(prediction));
            predictionMap.put(candidate, predCounter);
        }
    }

    private Map<String, Double> getPredicateHistograms(List<BabySRLSentence> sentences) {
        // This is a histogram of predicate candidates over number of arguments in a sentence
        Counter<String> predicateCandidateCounts = new Counter<>();

        for (BabySRLSentence s : sentences) {
            int predictedArgs = s.getPredictedArgNum();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String state = s.getHmmTag(wordInd);
                if (!isAllowedVerb(s, wordInd)) continue;
                // Increment the base frequency count
                predicateCandidateCounts.incrementCount(state);
                // Increment the candidate-argNum pattern count
                String pattern = state + "-" + predictedArgs;
                predicateCandidateCounts.incrementCount(pattern);
            }
        }
        return getVerbProbs(predicateCandidateCounts);
    }

    static Map<String, Double> getVerbProbs(Counter<String> predicateCandidateCounts) {
        // Aggregate counts into probabilities
        Map<String, Double> predicateProbabilities = new HashMap<>();
        for (String statePattern : predicateCandidateCounts.items()) {
            if (!statePattern.contains("-")) continue;
            String state = statePattern.split("-")[0];
            double prob = predicateCandidateCounts.getCount(statePattern) / predicateCandidateCounts.getCount(state);
            predicateProbabilities.put(statePattern, prob);
        }
        return predicateProbabilities;
    }

    /**
     * Searches the current sentence and determines the most likely predicate, by maximizing the
     * probability of any word being predicate given the number of arguments it currently has.<br/>
     * <b>Assumptions:</b>
     * <ul>
     *     <li>Exactly 1 verb predicate</li>
     *     <li>1--3 arguments for the verb predicate</li>
     * </ul>
     * @param s The current sentence containing argument information
     * @return The index of the most likely verb predicate candidate (or random if #args &gt; 3)
     */
    private int maxProbPredicates(BabySRLSentence s) {
        List<Integer> candidates = new ArrayList<>();
        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            if (!isAllowedVerb(s, wordInd)) continue;
            // If any of the gold predicates makes it into the candidates list set the value to 1
            if (s.isGoldVerbPred(wordInd))
                goldPredAmongCandidates = 1;
            candidates.add(wordInd);
        }
        candidatesNum += candidates.size();

        int predictedArgNum = s.getPredictedArgNum();

        if (predictedArgNum > 3) {
            s.maxArgsFound = 1;
            return RandomVerbIdentifier.getRandomVerbIndex(candidates);
        }

        ArgMax<Integer, Double> argMax = new ArgMax<>(-1, Double.NEGATIVE_INFINITY, true);
        for (int wordInd : candidates) {
            String hmmTag = s.getHmmTag(wordInd);
            String pattern = hmmTag + "-" + predictedArgNum;
            if (!predicateHistograms.containsKey(pattern)) continue;
            double prob = predicateHistograms.get(pattern);
            if (prob == argMax.getMaxValue())
                s.heuristicTie = 1;
            argMax.update(wordInd, prob);
        }
        int verbPredicateIndex = argMax.getArgmax();
        if (verbPredicateIndex == -1) {
            return RandomVerbIdentifier.getRandomVerbIndex(candidates);
        }
        return verbPredicateIndex;
    }

    @SuppressWarnings("RedundantIfStatement")
	public static boolean isAllowedVerb(BabySRLSentence sentence, int wordInd) {
        // Ignore punctuation (from HMMStateIdent.pl, line 548)
        if (sentence.getWord(wordInd).matches("\\p{Punct}")) return  false;
        // Also, ignore all argument states
        if (sentence.isPredictedNoun(wordInd)) return false;
        // Finally, ignore all function word states
        // (the property will be -1 if no content/function word split exists)
        if (Integer.parseInt(sentence.getHmmTag(wordInd)) < properties.getFunctionWordState()) return false;
        return true;
    }
}
