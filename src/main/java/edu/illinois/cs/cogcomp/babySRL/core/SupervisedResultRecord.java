package edu.illinois.cs.cogcomp.babySRL.core;

import org.apache.commons.lang.StringUtils;

public class SupervisedResultRecord {
	private String seedNouns;
	private String child;
	private String classifierFeatures;
	private String supervision;
	private String aggregation;

	private String roundNum;
	private String corpus;
	private String sentence;
	private String isCorrectPattern;

	public SupervisedResultRecord(int seedNouns, String child, String classifierFeatures, String supervision,
								  String aggregation, String corpus, String sentence,
								  boolean isCorrectPattern, int roundNum) {
		this.seedNouns = String.valueOf(seedNouns);
		this.child = child;
		this.classifierFeatures = classifierFeatures;
		this.supervision = supervision;
		this.aggregation = aggregation;
		this.corpus = corpus;
		this.sentence = sentence;
		this.isCorrectPattern = (isCorrectPattern)? "1" : "0";
		this.roundNum = String.valueOf(roundNum);
	}

	public static String getTitleLine() {
		return StringUtils.join(new String[]{"seed-nouns", "child", "feature", "supervision", "aggregation", "round",
				"corpus", "sentence", "correctPattern"}, "\t");
	}

	public String toString() {
		return StringUtils.join(new String[]{seedNouns, child, classifierFeatures, supervision, aggregation, roundNum,
				corpus, sentence, isCorrectPattern}, "\t");
	}
}
