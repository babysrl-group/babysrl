package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;

/**
 * A {@link SnowClassifier} with the Lex features (word, predicate-word)
 *
 * @author Christos Christodoulopoulos
 */
public class LexClassifierSnow extends SnowClassifier {

    public LexClassifierSnow() {
        super("lex", FeatureTypes.WORD, FeatureTypes.PREDICATE);
    }
}
