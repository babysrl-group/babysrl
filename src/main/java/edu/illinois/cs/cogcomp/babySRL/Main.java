package edu.illinois.cs.cogcomp.babySRL;

import edu.illinois.cs.cogcomp.babySRL.clustering.HMMWrapper;
import edu.illinois.cs.cogcomp.babySRL.core.*;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.data.DataHandler;
import edu.illinois.cs.cogcomp.babySRL.inference.Classifier;
import edu.illinois.cs.cogcomp.babySRL.inference.Evaluator;
import edu.illinois.cs.cogcomp.babySRL.inference.FeatureExtractor;
import edu.illinois.cs.cogcomp.babySRL.inference.Trainer;
import edu.illinois.cs.cogcomp.babySRL.inference.lbjava.LBJavaClassifier;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandDescription;
import edu.illinois.cs.cogcomp.core.utilities.commands.CommandIgnore;
import edu.illinois.cs.cogcomp.core.utilities.commands.InteractiveShell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * The entry-point class for all the BabySRL experiments.
 */
public class Main {
	private static Logger logger = LoggerFactory.getLogger(Main.class);
    private static BabySRLProperties properties = BabySRLProperties.getInstance();
    private static String tempDir;

    @CommandIgnore
    public static  void main(String args[]) {
        InteractiveShell<Main> shell = new InteractiveShell<>(Main.class);

        if (args.length == 0) {
            System.err.println("Usage: Main <command>");
            System.err.println("Required parameter command missing.");
            shell.showDocumentation();
        } else {
            tempDir = properties.getTempDir();

            // Create temp dir if it doesn't exist
            if (!IOUtils.mkdir(tempDir)) {
                System.err.println("Cannot create temporary directory");
                System.exit(-1);
            }

            // Uncomment this to receive notifications (see the documentation of NotificationSender for details)
            //NotificationSender notifier = new NotificationSender("config/notifier.key", "BabySRL", args[0]);
            // Run the required command
            try {
                shell.runCommand(args);
            } catch (Exception e) {
                //notifier.notify("Failed because of " + e.getCause());
                e.printStackTrace();
                System.exit(-1);
            }
            //notifier.notify("Finished: " + Arrays.toString(properties.getChildren()));
        }
    }

    @CommandDescription(usage = "trainHMM",
            description = "Trains 10 instances of the HMM and picks the best and tags all the train/test files.")
    public static void trainHMM() throws InterruptedException, ExecutionException, IOException {
        int numThreads = 10;
        ExecutorService executor = Executors.newFixedThreadPool(numThreads);
        List<HMMWrapper.HMMTrainer> hmmTrainers = new ArrayList<>();

        // The HMM options
        String trainFile = properties.getHmmTrainFile();
        if (!IOUtils.exists(trainFile)) throw new RuntimeException("File " + trainFile + " not found!");

        String modelName = tempDir + "hmmModel";
        String hmmStates = "H=" + properties.getHmmStates() + " ";
        String iterations = "I=" + 500 + " ";
        String functionWordsFile = properties.getHmmFunctionWordsFile();
        String functionWordSetting = "";
        if (!functionWordsFile.isEmpty())
            functionWordSetting = " S=" + functionWordsFile + ",5,30";
        // These have to do with VB and various thresholds
        String miscSettings = "M=0 C=1e-4" + functionWordSetting + " V=1 p=1 e=0.1 t=0.0001";
        String args = hmmStates + iterations + miscSettings;
        for (int i = 0; i < numThreads; i++)
            hmmTrainers.add(new HMMWrapper.HMMTrainer(i, trainFile, modelName, args));

        // Train all the HMMs in parallel and collect the LogLikelihood for each one
        List<Future<Pair<Integer, Double>>> results = executor.invokeAll(hmmTrainers);
        executor.shutdown();
        System.out.println();
        double bestLL = Double.NEGATIVE_INFINITY;
        int bestModel = -1;
        for (Future<Pair<Integer, Double>> result : results) {
            Pair<Integer, Double> pair = result.get();
            double logLikelihood  = pair.getSecond();
            if (logLikelihood > bestLL) {
                bestLL = logLikelihood;
                bestModel = pair.getFirst();
            }
            logger.info("Model {}, LL {}", pair.getFirst(), pair.getSecond());
        }
		logger.info("Best model: {} (LL: {})", bestModel, bestLL);

        //Copy the best model
        String bestHMMFileName = tempDir + "hmmModel-best";
        IOUtils.cp(modelName + bestModel, bestHMMFileName);

        // To tag the train/test files use
        // edu.illinois.cs.cogcomp.babySRL.clustering.HMMWrapper.main()
    }

    @CommandDescription(usage = "runUnsupervisedExperiment",
            description = "Run the BabySRL verb/noun induction experiment on an HMM-tagged corpus.")
    public static void runUnsupervisedExperiment() {
        // COMPACT RESULTS
        System.out.println(UnsupervisedExperimentResults.getMeanResultsCompactTitle());
        DataHandler testParser = new DataHandler();

        for (int numSeedNouns = 5; numSeedNouns <= properties.getNumSeedNouns(); numSeedNouns+=5) {
            UnsupervisedExperimentResults results = new UnsupervisedExperimentResults();
            // Repeat for N rounds (each round will generate a different random sub-list of nouns)
            for (int round = 0; round < properties.getNumRoundsUnsupervised(); round++) {
                // Run the heuristics
                testParser.runHeuristics(numSeedNouns, round);
                // Evaluate
                Evaluator.evaluateUnsupervised(testParser, results);
                testParser.resetPredictions();
            }
            System.out.println(numSeedNouns + "\t" + results.getMeanResultsCompact());
        }
    }

    @CommandDescription(usage = "runIncremental <num-seeds>",
            description = "Run the full BabySRL experiment with incremental predictions (for a given # of seeds).")
    public static void runIncremental(String numSeeds) throws IOException, InterruptedException {
        int numSeedNouns = Integer.parseInt(numSeeds);

        BabySRLCorpus trainTestCorpus = new BabySRLCorpus();
        // Add all the training and test files to the corpus
        for (String child : properties.getTrainFileNames())
            trainTestCorpus.readFile(child, properties.getMultiplePredicates());
        for (String child : properties.getTestFileNames())
            trainTestCorpus.readFile(child, properties.getMultiplePredicates());

        // Set the random seed to be used to select the seed nouns
        Random rand = new Random(23);
        String seedNounFile = properties.getSeedNounFile();

        UnsupervisedExperimentResults results = new UnsupervisedExperimentResults();
        EvaluationRecord nounRecord = new EvaluationRecord();
        EvaluationRecord verbRecord = new EvaluationRecord();
        Map<String, EvaluationRecord> randomRecord = new HashMap<>();

        Classifier[] classifiers = LBJavaClassifier.getClassifierSet();

        System.out.print(UnsupervisedExperimentResults.getMeanResultsCompactTitle() + "\t");
        for (Classifier classifier : classifiers) {
            // COMPACT RESULTS
            System.out.print(SupervisedExperimentResults.getMeanResultsCompactTitle(classifier.getName()) + "\t");
        }
        System.out.println();

        Map<String, SupervisedExperimentResults> classifierResults = new HashMap<>(classifiers.length);
        for (Classifier classifier : classifiers)
            classifierResults.put(classifier.getName(), new SupervisedExperimentResults());

        IncrementalIdentifier incrementalIdentifier = new IncrementalIdentifier(seedNounFile, numSeedNouns, rand);

        int sentNo = 1;
        for (BabySRLSentence s : trainTestCorpus.getSentences()) {
            incrementalIdentifier.addIncrementalPredictions(s);

            for (Classifier classifier : classifiers) {
                Trainer.trainSentence(classifier, s);
                classifier.close();
            }

            // Evaluate
            Evaluator.evaluateIncremental(s, results, nounRecord, verbRecord, randomRecord);
            for (Classifier classifier : classifiers)
                Evaluator.evaluateGorp(0, numSeedNouns, classifier, classifierResults.get(classifier.getName()));

            if (sentNo % 100 == 0) {
                System.out.print(sentNo + "\t" + results.getMeanResultsCompact() + "\t");
                for (Classifier classifier : classifiers)
                    System.out.print(classifierResults.get(classifier.getName()).getMeanResultsCompact() + "\t");
                System.out.println();
            }
            sentNo++;
        }
    }

    @CommandDescription(usage = "runSupervisedExperiment <test-gorp-sentences=true|false>",
            description = "Run the BabySRL supervised experiment.")
    public static void runSupervisedExperiment(String testGorp) throws IOException, InterruptedException {
		boolean isTestGorp = Boolean.parseBoolean(testGorp);
        Classifier[] classifiers = LBJavaClassifier.getClassifierSet();

		if (properties.getTags() == BabySRLCorpus.Tags.hmm)
        	System.out.print(UnsupervisedExperimentResults.getMeanResultsCompactTitle() + "\t");
        for (Classifier classifier : classifiers) {
            // COMPACT RESULTS
            System.out.print(SupervisedExperimentResults.getMeanResultsCompactTitle(classifier.getName()) + "\t");
        }
        System.out.print("avg-skipped-examples\tavg-skipped-examples-std\tavg-total-examples\tavg-total-examples-std");
        System.out.println();

        // Print evaluation record heading -- but only if the file doesn't exist
		String resultRecordsFile = properties.getTempDir() + "results-record.tsv";
		if (!IOUtils.exists(resultRecordsFile))
			LineIO.append(resultRecordsFile, SupervisedResultRecord.getTitleLine());

		String weightsFile = properties.getTempDir() + "classifier-weights.tsv";
		if (!IOUtils.exists(weightsFile))
			LineIO.append(weightsFile, SupervisedExperimentWeights.getTitleString());

        DataHandler dataHandler = new DataHandler();
        if (properties.getTags() == BabySRLCorpus.Tags.gold) {
            trainTestSupervised(isTestGorp, classifiers, -1, dataHandler);
        }

		else {
			for (int numSeedNouns = 5; numSeedNouns <= properties.getNumSeedNouns(); numSeedNouns += 5)
				trainTestSupervised(isTestGorp, classifiers, numSeedNouns, dataHandler);
		}
	}

    @CommandDescription(usage = "trainTestSupervised <numSeedNouns>",
            description = "Run the BabySRL supervised experiment.")
    public static void trainTestSupervised(String numSeedNouns) throws IOException, InterruptedException {
        trainTestSupervised(true, LBJavaClassifier.getClassifierSet(), Integer.parseInt(numSeedNouns), new DataHandler());
    }

	private static void trainTestSupervised(boolean isTestGorp, Classifier[] classifiers, int numSeedNouns,
                                            DataHandler dataHandler) throws IOException, InterruptedException {
		UnsupervisedExperimentResults heuristicsResults = new UnsupervisedExperimentResults();
		Map<String, SupervisedExperimentResults> classifierResults = new HashMap<>(classifiers.length);
		Map<String, SupervisedExperimentWeights> classifierWeightsA0 = new HashMap<>(classifiers.length);
		Map<String, SupervisedExperimentWeights> classifierWeightsA1 = new HashMap<>(classifiers.length);
		for (Classifier classifier : classifiers) {
			classifierResults.put(classifier.getName(), new SupervisedExperimentResults());
			classifierWeightsA0.put(classifier.getName(), new SupervisedExperimentWeights("A0", classifier, numSeedNouns));
			classifierWeightsA1.put(classifier.getName(), new SupervisedExperimentWeights("A1", classifier, numSeedNouns));
		}

		// Repeat for N rounds (for statistical significance) -- also the number of training rounds
		int numRounds = properties.getNumRoundsSupervised();
        OneVariableStats avgTrainingExamples = new OneVariableStats();
        OneVariableStats avgSkippedExamples = new OneVariableStats();
        for (int round = 0; round < numRounds; round++) {
            // Need to rerun the verb/nom heuristics (if needed)
            dataHandler.runHeuristics(numSeedNouns, round);
            if (properties.getTags() == BabySRLCorpus.Tags.hmm)
            	Evaluator.evaluateUnsupervised(dataHandler, heuristicsResults);
            // Before training, set the animacy feedback
            if (properties.getSupervision() == BabySRLCorpus.Supervision.animacy)
                dataHandler.setAnimacyFeedback();
			for (Classifier classifier : classifiers) {
				logger.debug("Training {}", classifier.getName());
				Trainer.train(numRounds, dataHandler, classifier);
				classifierWeightsA0.get(classifier.getName()).updateWeights();
				classifierWeightsA1.get(classifier.getName()).updateWeights();
			}
            // Get the counts for the training examples (should be the same for all Classifiers)
            avgTrainingExamples.add(Trainer.getTotalTrainingExamples());
            avgSkippedExamples.add(Trainer.getSkippedTrainingExamples());

			// Need to re-initialise the classifiers so that they read from the trained models
			for (Classifier classifier : LBJavaClassifier.getClassifierSet()) {
				logger.debug("Testing {}", classifier.getName());
				SupervisedExperimentResults results = classifierResults.get(classifier.getName());
				Evaluator.testSupervisedClassifier(classifier, dataHandler, results);
				if (isTestGorp) Evaluator.evaluateGorp(round, numSeedNouns, classifier, results);
                classifier.close();
			}
            // Create the per-round/per-seed-nouns report
			if (properties.createReports() && properties.getTags() == BabySRLCorpus.Tags.hmm) {
				String file = DataHandler.getReportFileName(numSeedNouns, round);
				// Add the title
				String line = "\n" + UnsupervisedExperimentResults.getLatestMeanResultsCompactTitle() + "\t";
				for (Classifier classifier : classifiers)
					line += SupervisedExperimentResults.getLatestMeanResultsCompactTitle(classifier.getName()) + "\t";
				line += "avg-skipped-examples\tavg-total-examples";

				// Add the results
				if (properties.getTags() == BabySRLCorpus.Tags.hmm)
					line += "\n" + numSeedNouns + "\t" + heuristicsResults.getLatestResultsCompact() + "\t";
				for (Classifier classifier : classifiers)
					line += classifierResults.get(classifier.getName()).getLatestResultsCompact() + "\t";
				// Add the training example counts
				line += Trainer.getSkippedTrainingExamples() + "\t" + Trainer.getTotalTrainingExamples();
				LineIO.append(file, line);
				LineIO.append(file + "-hist.txt", line);
			}
            dataHandler.resetPredictions();
		}
		if (properties.getTags() == BabySRLCorpus.Tags.hmm) {
			System.out.print(numSeedNouns + "\t");
			System.out.print(heuristicsResults.getMeanResultsCompact() + "\t");
		}
		for (Classifier classifier : classifiers) {
			System.out.print(classifierResults.get(classifier.getName()).getMeanResultsCompact() + "\t");
			FeatureExtractor.removeFiles(classifier.getFex().getFeatureTypes());

			// Print the full evaluation record for all the rounds with this number of seed nouns
			String resultRecordsFile = properties.getTempDir() + "results-record.tsv";
			classifierResults.get(classifier.getName()).printResultRecords(resultRecordsFile);

			// Print the classifier weights
			String weightsFile = properties.getTempDir() + "classifier-weights.tsv";
			LineIO.append(weightsFile, classifierWeightsA0.get(classifier.getName()).getWeightsString());
			LineIO.append(weightsFile, classifierWeightsA1.get(classifier.getName()).getWeightsString());
		}
        System.out.print(getExampleCounts(avgTrainingExamples, avgSkippedExamples));
		System.out.println();
	}

    private static String getExampleCounts(OneVariableStats avgTrainingExamples, OneVariableStats avgSkippedExamples) {
        return StringUtils.getFormattedTwoDecimal(avgSkippedExamples.mean()) + "\t" +
                StringUtils.getFormattedTwoDecimal(avgSkippedExamples.stdErr()) + "\t" +
                StringUtils.getFormattedTwoDecimal(avgTrainingExamples.mean()) + "\t" +
                StringUtils.getFormattedTwoDecimal(avgTrainingExamples.stdErr());
    }
}
