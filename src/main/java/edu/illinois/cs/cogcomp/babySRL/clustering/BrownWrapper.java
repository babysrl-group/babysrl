package edu.illinois.cs.cogcomp.babySRL.clustering;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A wrapper for the Brown clusters (just tagging for now).
 */
public class BrownWrapper extends Tagger {

    /**
     * Tags a BabySRL corpus with Brown categories. Since Brown provides a hard clustering, there is no
     * need to tag in context.
     * <br/>
     * <b>NB:</b> Unknown words will be tagged with most frequent category.
     *
     * @param corpus The BabySRL corpus to be tagged
     * @param brownFile The output of Liang's Brown clustering algorithm; contains a large collection of tagged words
     * @throws java.io.FileNotFoundException
     */
    public void tagCorpus(BabySRLCorpus corpus, String brownFile) throws IOException {
        // Use a list to get a unique index for each tag
        List<String> knownTags = new ArrayList<>();
        // Create the word/tag map
        Map<String, String> wordTagMap = new HashMap<>();
        List<String> lines = LineIO.read(brownFile);
        Counter<String> tagCounter = new Counter<>();

        for (String line : lines) {
            String tag = line.split("\t")[0];
            String tagIndex;
            if (knownTags.contains(tag))
                tagIndex = String.valueOf(knownTags.indexOf(tag));
            else {
                knownTags.add(tag);
                tagIndex = String.valueOf(knownTags.size()-1);
            }
            String word = line.split("\t")[1];

            tagCounter.incrementCount(tagIndex);
            wordTagMap.put(word, tagIndex);
        }
        // Get the most frequent tag
        String mostFreqTag = tagCounter.getMax().getFirst();

        // Now tag the corpus
        tagCorpusHardClusters(corpus, wordTagMap, mostFreqTag);
    }

    public static void main(String[] args) throws IOException {
        BabySRLCorpus corpus = new BabySRLCorpus();
        BabySRLProperties properties = BabySRLProperties.getInstance();
        for (String child : properties.getTestFileNames()) {
            corpus.readFile("data/original/test." + child + ".col");
            String outputFile = "data/brown/test." + child + ".tagged";
            new BrownWrapper().tagCorpus(corpus, "data/brown/childesNQ.brown");
            corpus.writeCorpus(outputFile);
        }
    }
}
