package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.babySRL.inference.Classifier;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

public class SupervisedExperimentWeights {
	private String supervision;
	private String aggregation;
	private static String child = BabySRLProperties.getInstance().getChildren()[0];
	private static String[] features = {"word:"+child, "npat:_N", "npat:N_", "vpos:Before", "vpos:After"};
	private static String[] featuresNames = {"childName","npatFirst", "npatSecond", "vposBefore", "vposAfter"};
	private Map<String, OneVariableStats> statsMap = new HashMap<>();
	private String label;
	private Classifier classifier;
	private int numSeedNouns;

	public SupervisedExperimentWeights(String label, Classifier classifier, int numSeedNouns) {
		this.label = label;
		this.classifier = classifier;
		this.numSeedNouns = numSeedNouns;
		for (String feature : features) {
			statsMap.put(feature, new OneVariableStats());
		}
		BabySRLProperties properties = BabySRLProperties.getInstance();
		this.supervision = properties.getSupervision().name();
		this.aggregation = properties.getPredAggregationString();
	}

	public void updateWeights() {
		for (String feature : features) {
			double weight = classifier.getWeight(label, feature);
			if (weight == 0)
				continue;
			statsMap.get(feature).add(weight);
		}
	}

	public String getWeightsString() {
		StringJoiner sj = new StringJoiner("\t");
		sj.add(child).add(Integer.toString(numSeedNouns)).add(classifier.getName());
		sj.add(supervision).add(aggregation).add(label);
		for (String feature : features) {
			sj.add(StringUtils.getFormattedString(statsMap.get(feature).mean(), 3));
			sj.add(StringUtils.getFormattedString(statsMap.get(feature).stdErr(), 3));
		}
		return sj.toString();
	}

	public static String getTitleString() {
		StringJoiner sj = new StringJoiner("\t");
		sj.add("child").add("seed-nouns").add("classifier").add("supervision").add("aggregation").add("label");
		for (String feature : featuresNames) sj.add(feature).add(feature+"-std");
		return sj.toString();
	}
}
