package edu.illinois.cs.cogcomp.babySRL.clustering;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * An implementation of the Frequent Frames as described in Mintz (2003)
 */
public class FrequentFrames {
	private static BabySRLProperties properties = BabySRLProperties.getInstance();
	private FrameCounter framesCounter = new FrameCounter();
	private int topNFrames;

	public FrequentFrames(int topNFrames) {
		this.topNFrames = topNFrames;
	}

	public static void main(String[] args) throws IOException {
		BabySRLCorpus corpus = new BabySRLCorpus();
		for (String file : properties.getTrainFileNames())
			corpus.readFile(file);
		for (String file : properties.getTestFileNames())
			corpus.readFile(file);

		FrequentFrames frequentFrames = new FrequentFrames(45);
		frequentFrames.generateFrames(corpus);
		frequentFrames.printFFs();
		frequentFrames.tagCorpus(corpus);
	}

	public void generateFrames(BabySRLCorpus corpus) {
		for (BabySRLSentence sentence : corpus.getSentences()) {
			for (int wordInd = 0; wordInd < sentence.getWordCount(); wordInd++) {
				String word = sentence.getWord(wordInd);
				if (word.matches("\\p{Punct}")) continue;

				String wordPrev = getWord(sentence, wordInd, -1);
				String wordNext = getWord(sentence, wordInd, 1);
				Frame f = new Frame(word, wordPrev, wordNext);
				framesCounter.incrementCount(f);
			}
		}
		framesCounter.prune(topNFrames);
	}

	public void printFFs() {
		for (FrequentFrames.Frame f : framesCounter.getSortedItemsHighestFirst().subList(0, topNFrames)) {
			List<String> words = framesCounter.getWords(f).getSortedItemsHighestFirst();
			int size = Math.min(10, words.size());
			System.out.println(framesCounter.getFrameIndex(f) + ") " + f + ": " + words.subList(0, size));
		}
	}

	public int getSize() {
		return framesCounter.globalIndex;
	}

	public List<Frame> getAllFrames(String word) {
		return framesCounter.getAllFrames(word);
	}

	public void tagCorpus(BabySRLCorpus corpus) throws IOException {
		for (BabySRLSentence sentence : corpus.getSentences()) {
			List<String> tags = sentence.getHmmTags();
			for (int wordInd = 0; wordInd < sentence.getWordCount(); wordInd++) {
				String word = sentence.getWord(wordInd);
				if (word.matches("\\p{Punct}")) continue;
				if (Integer.parseInt(sentence.getHmmTag(wordInd)) < properties.getFunctionWordState())
					continue;

				String wordPrev = getWord(sentence, wordInd, -1);
				String wordNext = getWord(sentence, wordInd, 1);
				Frame f = new Frame(word, wordPrev, wordNext);
				String ffTag = "FF:" + framesCounter.getFrameIndex(f);
				tags.set(wordInd, ffTag);
			}
			sentence.setHmmTags(tags);
		}
	}

	private String getWord(BabySRLSentence sentence, int wordInd, int direction) {
		String newWord = "*";
		int newWordInd = wordInd + direction;
		if (newWordInd >= 0 && newWordInd < sentence.getWordCount())
			newWord = sentence.getWord(wordInd + direction);
		else return newWord;

		if (newWord.matches("\\p{Punct}")) {
			int newDirection = (direction > 0) ? direction + 1 : direction - 1;
			return getWord(sentence, wordInd, newDirection);
		}

		return newWord;
	}

	public String getTopWord(Frame frame) {
		return framesCounter.getWords(frame).getSortedItemsHighestFirst().get(0);
	}

	public class FrameCounter extends Counter<Frame> {
		Map<Frame, Counter<String>> wordMap = new HashMap<>();
		Map<Frame, Integer> frameIndex = new HashMap<>();
		int globalIndex;

		public FrameCounter() {
			globalIndex = 0;
		}

		@Override
		public void incrementCount(Frame frame) {
			super.incrementCount(frame);
			Counter<String> frameWords;
			if (wordMap.containsKey(frame))
				frameWords = wordMap.get(frame);
			else {
				frameWords = new Counter<>();
				frameIndex.put(frame, globalIndex++);
			}
			frameWords.incrementCount(frame.getWord());
			wordMap.put(frame, frameWords);
		}

		public void prune(int topN) {
			for (Frame f : getSortedItemsHighestFirst().subList(topN, size())) {
				wordMap.remove(f);
				frameIndex.remove(f);
			}
		}

		public Counter<String> getWords(Frame frame) {
			return wordMap.get(frame);
		}

		public int getFrameIndex(Frame frame) {
			if (!frameIndex.containsKey(frame))
				return -1;
			return frameIndex.get(frame);
		}

		public List<Frame> getAllFrames(String word) {
			List<Frame> frames = new ArrayList<>();
			for (Frame f : wordMap.keySet()) {
				if (wordMap.get(f).contains(word)) frames.add(f);
			}
			return frames;
		}
	}

	public static class Frame implements Serializable {
		String word, wordPrev, wordNext;

		Frame(String word, String wordPrev, String wordNext) {
			this.word = word;
			this.wordPrev = wordPrev;
			this.wordNext = wordNext;
		}

		public String getWord() {
			return word;
		}

		public boolean equals(Object o) {
			if (!(o instanceof Frame)) return false;
			Frame otherFrame = (Frame) o;
			return wordPrev.equals(otherFrame.wordPrev) && wordNext.equals(otherFrame.wordNext);
		}

		@Override
		public int hashCode() {
			return wordPrev.hashCode() + wordNext.hashCode();
		}

		public String toString() {
			return wordPrev + " X " + wordNext;
		}
	}
}
