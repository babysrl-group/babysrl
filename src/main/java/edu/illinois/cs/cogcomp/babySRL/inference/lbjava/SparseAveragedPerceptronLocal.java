package edu.illinois.cs.cogcomp.babySRL.inference.lbjava;

import edu.illinois.cs.cogcomp.lbjava.classify.Feature;
import edu.illinois.cs.cogcomp.lbjava.learn.*;
import edu.illinois.cs.cogcomp.lbjava.util.DVector;
import edu.illinois.cs.cogcomp.lbjava.util.ExceptionlessInputStream;
import edu.illinois.cs.cogcomp.lbjava.util.ExceptionlessOutputStream;

import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;


/**
 * A local copy of {@link SparseAveragedPerceptron} so that we can expose the
 * weight vector.
 */
public class SparseAveragedPerceptronLocal extends SparsePerceptron
{
  /** Default for {@link LinearThresholdUnit#weightVector}. */
  private static final AveragedWeightVector defaultWeightVector =
          new AveragedWeightVector();

  /**
   * Holds the same reference as {@link LinearThresholdUnit#weightVector}
   * casted to {@link SparseAveragedPerceptronLocal.AveragedWeightVector}.
   **/
  AveragedWeightVector awv;
  /** Keeps the extra information necessary to compute the averaged bias. */
  private double averagedBias;


  /**
   * The learning rate and threshold take default values, while the name of
   * the classifier gets the empty string.
   **/
  public SparseAveragedPerceptronLocal() { this(""); }

  /**
   * Sets the learning rate to the specified value, and the threshold takes
   * the default, while the name of the classifier gets the empty string.
   *
   * @param r  The desired learning rate value.
   **/
  public SparseAveragedPerceptronLocal(double r) { this("", r); }

  /**
   * Sets the learning rate and threshold to the specified values, while the
   * name of the classifier gets the empty string.
   *
   * @param r  The desired learning rate value.
   * @param t  The desired threshold value.
   **/
  public SparseAveragedPerceptronLocal(double r, double t) { this("", r, t); }

  /**
   * Use this constructor to fit a thick separator, where both the positive
   * and negative sides of the hyperplane will be given the specified
   * thickness, while the name of the classifier gets the empty string.
   *
   * @param r  The desired learning rate value.
   * @param t  The desired threshold value.
   * @param pt The desired thickness.
   **/
  SparseAveragedPerceptronLocal(double r, double t, double pt) {
    this("", r, t, pt);
  }

  /**
   * Use this constructor to fit a thick separator, where the positive and
   * negative sides of the hyperplane will be given the specified separate
   * thicknesses, while the name of the classifier gets the empty string.
   *
   * @param r  The desired learning rate value.
   * @param t  The desired threshold value.
   * @param pt The desired positive thickness.
   * @param nt The desired negative thickness.
   **/
  public SparseAveragedPerceptronLocal(double r, double t, double pt, double nt) {
    this("", r, t, pt, nt);
  }

  /**
   * Initializing constructor.  Sets all member variables to their associated
   * settings in the {@link SparseAveragedPerceptronLocal.Parameters} object.
   *
   * @param p  The settings of all parameters.
   **/
  public SparseAveragedPerceptronLocal(SparseAveragedPerceptronLocal.Parameters p) {
    this("", p);
  }


  /**
   * The learning rate and threshold take default values.
   *
   * @param n  The name of the classifier.
   **/
  private SparseAveragedPerceptronLocal(String n) { this(n, defaultLearningRate); }

  /**
   * Sets the learning rate to the specified value, and the threshold takes
   * the default.
   *
   * @param n  The name of the classifier.
   * @param r  The desired learning rate value.
   **/
  private SparseAveragedPerceptronLocal(String n, double r) {
    this(n, r, defaultThreshold);
  }

  /**
   * Sets the learning rate and threshold to the specified values.
   *
   * @param n  The name of the classifier.
   * @param r  The desired learning rate value.
   * @param t  The desired threshold value.
   **/
  private SparseAveragedPerceptronLocal(String n, double r, double t) {
    this(n, r, t, defaultThickness);
  }

  /**
   * Use this constructor to fit a thick separator, where both the positive
   * and negative sides of the hyperplane will be given the specified
   * thickness.
   *
   * @param n  The name of the classifier.
   * @param r  The desired learning rate value.
   * @param t  The desired threshold value.
   * @param pt The desired thickness.
   **/
  private SparseAveragedPerceptronLocal(String n, double r, double t, double pt) {
    this(n, r, t, pt, pt);
  }

  /**
   * Use this constructor to fit a thick separator, where the positive and
   * negative sides of the hyperplane will be given the specified separate
   * thicknesses.
   *
   * @param n  The name of the classifier.
   * @param r  The desired learning rate value.
   * @param t  The desired threshold value.
   * @param pt The desired positive thickness.
   * @param nt The desired negative thickness.
   **/
  private SparseAveragedPerceptronLocal(String n, double r, double t, double pt,
                                        double nt) {
    super(n);
    Parameters p = new Parameters();
    p.learningRate = r;
    p.threshold = t;
    p.positiveThickness = pt;
    p.negativeThickness = nt;
    setParameters(p);
  }

  /**
   * Initializing constructor.  Sets all member variables to their associated
   * settings in the {@link SparseAveragedPerceptronLocal.Parameters} object.
   *
   * @param n  The name of the classifier.
   * @param p  The settings of all parameters.
   **/
  private SparseAveragedPerceptronLocal(String n,
                                        SparseAveragedPerceptronLocal.Parameters p) {
    super(n);
    setParameters(p);
  }


  /**
   * Retrieves the parameters that are set in this learner.
   *
   * @return An object containing all the values of the parameters that
   *         control the behavior of this learning algorithm.
   **/
  public Learner.Parameters getParameters() {
    Parameters p =
            new Parameters((SparsePerceptron.Parameters) super.getParameters());
    return p;
  }


  /**
   * Sets the values of parameters that control the behavior of this learning
   * algorithm.
   *
   * @param p  The parameters.
   **/
  private void setParameters(Parameters p) {
    super.setParameters(p);
    awv = (AveragedWeightVector) weightVector;
  }


  /**
   * The score of the specified object is equal to <code>w * x + bias</code>
   * where <code>*</code> is dot product, <code>w</code> is the weight
   * vector, and <code>x</code> is the feature vector produced by the
   * extractor.
   *
   * @param exampleFeatures  The example's array of feature indices.
   * @param exampleValues    The example's array of feature values.
   * @return The result of the dot product plus the bias.
   **/
  public double score(int[] exampleFeatures, double[] exampleValues) {
    double result = awv.dot(exampleFeatures, exampleValues, initialWeight);
    int examples = awv.getExamples();

    if (examples > 0)
      result += (examples * bias - averagedBias) / (double) examples;
    return result;
  }


  /**
   * Scales the feature vector produced by the extractor by the learning rate
   * and adds it to the weight vector.
   *
   * @param exampleFeatures  The example's array of feature indices.
   * @param exampleValues    The example's array of feature values.
   **/
  public void promote(int[] exampleFeatures, double[] exampleValues,
                      double rate) {
    bias += rate;

    int examples = awv.getExamples();
    averagedBias += examples * rate;
    awv.scaledAdd(exampleFeatures, exampleValues, rate, initialWeight);
  }


  /**
   * Scales the feature vector produced by the extractor by the learning rate
   * and subtracts it from the weight vector.
   *
   * @param exampleFeatures  The example's array of feature indices.
   * @param exampleValues    The example's array of feature values.
   **/
  public void demote(int[] exampleFeatures, double[] exampleValues,
                     double rate) {
    bias -= rate;

    int examples = awv.getExamples();
    averagedBias -= examples * rate;
    awv.scaledAdd(exampleFeatures, exampleValues, -rate, initialWeight);
  }


  /**
   * This method works just like
   * {@link LinearThresholdUnit#learn(int[],double[],int[],double[])}, except
   * it notifies its weight vector when it got an example correct in addition
   * to updating it when it makes a mistake.
   *
   * @param exampleFeatures  The example's array of feature indices
   * @param exampleValues    The example's array of feature values
   * @param exampleLabels    The example's label(s)
   * @param labelValues      The labels' values
   **/
  public void learn(int[] exampleFeatures, double[] exampleValues,
                    int[] exampleLabels, double[] labelValues) {
    assert exampleLabels.length == 1
            : "Example must have a single label.";
    assert exampleLabels[0] == 0 || exampleLabels[0] == 1
            : "Example has unallowed label value.";

    boolean label = (exampleLabels[0] == 1);

    double s =
            awv.simpleDot(exampleFeatures, exampleValues, initialWeight) + bias;
    if (label && s < threshold + positiveThickness)
      promote(exampleFeatures, exampleValues, getLearningRate());
    else if (!label && s >= threshold - negativeThickness)
      demote(exampleFeatures, exampleValues, getLearningRate());
    else awv.correctExample();
  }


  /**
   * Initializes the weight vector array to the size of
   * the supplied number of features, with each cell taking
   * the default value of {@link #initialWeight}.
   *
   * @param numExamples   The number of examples
   * @param numFeatures   The number of features
   **/
  public void initialize(int numExamples, int numFeatures) {
    double[] weights = new double[numFeatures];
    Arrays.fill(weights, initialWeight);
    weightVector = awv = new AveragedWeightVector(weights);
  }


  /** Resets the weight vector to all zeros. */
  public void forget() {
    super.forget();
    awv = (AveragedWeightVector) weightVector;
    averagedBias = 0;
  }


  /**
   * Writes the algorithm's internal representation as text.  In the first
   * line of output, the name of the classifier is printed, followed by
   * {@link SparsePerceptron#learningRate},
   * {@link LinearThresholdUnit#initialWeight},
   * {@link LinearThresholdUnit#threshold},
   * {@link LinearThresholdUnit#positiveThickness},
   * {@link LinearThresholdUnit#negativeThickness},
   * {@link LinearThresholdUnit#bias}, and finally {@link #averagedBias}.
   *
   * @param out  The output stream.
   **/
  public void write(PrintStream out) {
    out.println(name + ": " + learningRate + ", " + initialWeight + ", "
            + threshold + ", " + positiveThickness + ", "
            + negativeThickness + ", " + bias + ", " + averagedBias);
    if (lexicon == null || lexicon.size() == 0) awv.write(out);
    else awv.write(out, lexicon);
  }


  /**
   * Writes the learned function's internal representation in binary form.
   *
   * @param out  The output stream.
   **/
  public void write(ExceptionlessOutputStream out) {
    super.write(out);
    out.writeDouble(averagedBias);
  }


  /**
   * Reads the binary representation of a learner with this object's run-time
   * type, overwriting any and all learned or manually specified parameters
   * as well as the label lexicon but without modifying the feature lexicon.
   *
   * @param in The input stream.
   **/
  public void read(ExceptionlessInputStream in) {
    super.read(in);
    awv = (AveragedWeightVector) weightVector;
    averagedBias = in.readDouble();
  }


  /**
   * Simply a container for all of {@link SparseAveragedPerceptronLocal}'s
   * configurable parameters.  Using instances of this class should make code
   * more readable and constructors less complicated.  Note that if the
   * object referenced by {@link LinearThresholdUnit.Parameters#weightVector}
   * is replaced via an instance of this class, it must be replaced with an
   * {@link SparseAveragedPerceptronLocal.AveragedWeightVector}.
   *
   * @author Nick Rizzolo
   **/
  public static class Parameters extends SparsePerceptron.Parameters
  {
    /** Sets all the default values. */
    Parameters() {
      weightVector = (AveragedWeightVector) defaultWeightVector.clone();
    }


    /**
     * Sets the parameters from the parent's parameters object, giving
     * defaults to all parameters declared in this object.
     **/
    Parameters(SparsePerceptron.Parameters p) { super(p); }


    /** Copy constructor. */
    public Parameters(Parameters p) { super(p); }


    /**
     * Calls the appropriate <code>Learner.setParameters(Parameters)</code>
     * method for this <code>Parameters</code> object.
     *
     * @param l  The learner whose parameters will be set.
     **/
    public void setParameters(Learner l) {
      ((SparseAveragedPerceptronLocal) l).setParameters(this);
    }
  }


  /**
   * This implementation of a sparse weight vector associates two
   * <code>double</code>s with each {@link Feature}.  The first plays the
   * role of the usual weight vector, and the second accumulates multiples of
   * examples on which mistakes were made to help implement the weighted
   * average.
   *
   * @author Nick Rizzolo
   **/
  public static class AveragedWeightVector extends SparseWeightVector
  {
    /**
     * Together with {@link SparseWeightVector#weights}, this vector provides
     * enough information to reconstruct the average of all weight vectors
     * arrived at during the course of learning.
     **/
    DVector averagedWeights;
    /** Counts the total number of training examples this vector has seen. */
    int examples;


    /** Simply instantiates the weight vectors. */
    AveragedWeightVector() { this(new DVector(defaultCapacity)); }

    /**
     * Simply initializes the weight vectors.
     *
     * @param w  An array of weights.
     **/
    AveragedWeightVector(double[] w) { this(new DVector(w)); }

    /**
     * Simply initializes the weight vectors.
     *
     * @param w  A vector of weights.
     **/
    AveragedWeightVector(DVector w) {
      super((DVector) w.clone());
      averagedWeights = w;
    }


    /** Increments the {@link #examples} variable. */
    void correctExample() { ++examples; }
    /** Returns the {@link #examples} variable. */
    int getExamples() { return examples; }


    /**
     * Returns the averaged weight of the given feature.
     *
     * @param featureIndex The feature index.
     * @param defaultW     The default weight.
     * @return The weight of the feature.
     **/
    double getAveragedWeight(int featureIndex, double defaultW) {
      if (examples == 0) return 0;
      double aw = averagedWeights.get(featureIndex, defaultW);
      double w = getWeight(featureIndex, defaultW);
      return (examples*w - aw) / (double) examples;
    }


    /**
     * Takes the dot product of this <code>AveragedWeightVector</code> with
     * the argument vector, using the hard coded default weight.
     *
     * @param exampleFeatures  The example's array of feature indices.
     * @param exampleValues    The example's array of feature values.
     * @return The computed dot product.
     **/
    public double dot(int[] exampleFeatures, double[] exampleValues) {
      return dot(exampleFeatures, exampleValues, defaultWeight);
    }


    /**
     * Takes the dot product of this <code>AveragedWeightVector</code> with
     * the argument vector, using the specified default weight when one is
     * not yet present in this vector.
     *
     * @param exampleFeatures  The example's array of feature indices.
     * @param exampleValues    The example's array of feature values.
     * @param defaultW         The default weight.
     * @return The computed dot product.
     **/
    public double dot(int[] exampleFeatures, double[] exampleValues,
                      double defaultW) {
      double sum = 0;

      for (int i = 0; i < exampleFeatures.length; i++) {
        double w = getAveragedWeight(exampleFeatures[i], defaultW);
        sum += w * exampleValues[i];
      }

      return sum;
    }


    /**
     * Takes the dot product of the regular, non-averaged, Perceptron weight
     * vector with the given vector, using the hard coded default weight.
     *
     * @param exampleFeatures  The example's array of feature indices.
     * @param exampleValues    The example's array of feature values.
     * @return The computed dot product.
     **/
    public double simpleDot(int[] exampleFeatures, double[] exampleValues) {
      return super.dot(exampleFeatures, exampleValues, defaultWeight);
    }


    /**
     * Takes the dot product of the regular, non-averaged, Perceptron weight
     * vector with the given vector, using the specified default weight when
     * a feature is not yet present in this vector.
     *
     * @param exampleFeatures  The example's array of feature indices.
     * @param exampleValues    The example's array of feature values.
     * @param defaultW         An initial weight for new features.
     * @return The computed dot product.
     **/
    double simpleDot(int[] exampleFeatures, double[] exampleValues,
                     double defaultW) {
      return super.dot(exampleFeatures, exampleValues, defaultW);
    }


    /**
     * Performs pairwise addition of the feature values in the given vector
     * scaled by the given factor, modifying this weight vector, using the
     * specified default weight when a feature from the given vector is not
     * yet present in this vector.  The default weight is used to initialize
     * new feature weights.
     *
     * @param exampleFeatures  The example's array of feature indices.
     * @param exampleValues    The example's array of feature values.
     * @param factor           The scaling factor.
     **/
    public void scaledAdd(int[] exampleFeatures, double[] exampleValues,
                          double factor) {
      scaledAdd(exampleFeatures, exampleValues, factor, defaultWeight);
    }


    /**
     * Performs pairwise addition of the feature values in the given vector
     * scaled by the given factor, modifying this weight vector, using the
     * specified default weight when a feature from the given vector is not
     * yet present in this vector.
     *
     * @param exampleFeatures  The example's array of feature indices.
     * @param exampleValues    The example's array of feature values.
     * @param factor           The scaling factor.
     * @param defaultW         An initial weight for new features.
     **/
    public void scaledAdd(int[] exampleFeatures, double[] exampleValues,
                          double factor, double defaultW) {
      for (int i = 0; i < exampleFeatures.length; i++) {
        int featureIndex = exampleFeatures[i];
        double currentWeight = getWeight(featureIndex, defaultW);
        double w = currentWeight + factor*exampleValues[i];

        double difference = w - currentWeight;
        updateAveragedWeight(featureIndex, examples*difference);

        setWeight(featureIndex, w);
      }

      ++examples;
    }


    /**
     * Adds a new value to the current averaged weight indexed
     * by the supplied feature index.
     *
     * @param featureIndex The feature index.
     * @param w            The value to add to the current weight.
     **/
    void updateAveragedWeight(int featureIndex, double w) {
      double newWeight = averagedWeights.get(featureIndex, defaultWeight) + w;
      averagedWeights.set(featureIndex, newWeight, defaultWeight);
    }


    /**
     * Outputs the contents of this <code>SparseWeightVector</code> into the
     * specified <code>PrintStream</code>.  The string representation starts
     * with a <code>"Begin"</code> annotation, ends with an
     * <code>"End"</code> annotation, and without a <code>Lexicon</code>
     * passed as a parameter, the weights are simply printed in the order of
     * their integer indices.
     *
     * @param out  The stream to write to.
     **/
    public void write(PrintStream out) {
      out.println("Begin AveragedWeightVector");
      for (int i = 0; i < averagedWeights.size(); ++i)
        out.println(getAveragedWeight(i, 0));
      out.println("End AveragedWeightVector");
    }


    /**
     * Outputs the contents of this <code>SparseWeightVector</code> into the
     * specified <code>PrintStream</code>.  The string representation starts
     * with a <code>"Begin"</code> annotation, ends with an
     * <code>"End"</code> annotation, and lists each feature with its
     * corresponding weight on the same, separate line in between.
     *
     * @param out  The stream to write to.
     * @param lex  The feature lexicon.
     **/
    public void write(PrintStream out, Lexicon lex) {
      out.println("Begin AveragedWeightVector");

      Map map = lex.getMap();
      Map.Entry[] entries =
              (Map.Entry[]) map.entrySet().toArray(new Map.Entry[0]);
      Arrays.sort(entries,
              new Comparator() {
                public int compare(Object o1, Object o2) {
                  Map.Entry e1 = (Map.Entry) o1;
                  Map.Entry e2 = (Map.Entry) o2;
                  int i1 = (Integer) e1.getValue();
                  int i2 = (Integer) e2.getValue();
                  if ((i1 < weights.size()) != (i2 < weights.size()))
                    return i1 - i2;
                  return ((Feature) e1.getKey()).compareTo(e2.getKey());
                }
              });

      int i, biggest = 0;
      for (i = 0; i < entries.length; ++i) {
        String key =
                entries[i].getKey().toString()
                        + ((Integer) entries[i].getValue() < weights.size()
                        ? "" : " (pruned)");
        biggest = Math.max(biggest, key.length());
      }

      if (biggest % 2 == 0) biggest += 2;
      else ++biggest;

      for (i = 0; i < entries.length; ++i) {
        String key =
                entries[i].getKey().toString()
                        + ((Integer) entries[i].getValue() < weights.size()
                        ? "" : " (pruned)");
        out.print(key);
        for (int j = 0; key.length() + j < biggest; ++j) out.print(" ");

        int index = (Integer) entries[i].getValue();
        double weight = getAveragedWeight(index, 0);
        out.println(weight);
      }

      out.println("End AveragedWeightVector");
    }


    /**
     * Writes the weight vector's internal representation in binary form.
     *
     * @param out  The output stream.
     **/
    public void write(ExceptionlessOutputStream out) {
      super.write(out);
      out.writeInt(examples);
      averagedWeights.write(out);
    }


    /**
     * Reads the representation of a weight vector with this object's
     * run-time type from the given stream, overwriting the data in this
     * object.
     *
     * <p> This method is appropriate for reading weight vectors as written
     * by {@link #write(ExceptionlessOutputStream)}.
     *
     * @param in The input stream.
     **/
    public void read(ExceptionlessInputStream in) {
      super.read(in);
      examples = in.readInt();
      averagedWeights.read(in);
    }


    /**
     * Returns a copy of this <code>AveragedWeightVector</code>.
     *
     * @return A copy of this <code>AveragedWeightVector</code>.
     **/
    public Object clone() {
      AveragedWeightVector clone = (AveragedWeightVector) super.clone();
      clone.averagedWeights = (DVector) averagedWeights.clone();
      return clone;
    }


    /**
     * Returns a new, empty weight vector with the same parameter settings as
     * this one.
     *
     * @return An empty weight vector.
     **/
    public SparseWeightVector emptyClone() {
      return new AveragedWeightVector();
    }
  }
}

