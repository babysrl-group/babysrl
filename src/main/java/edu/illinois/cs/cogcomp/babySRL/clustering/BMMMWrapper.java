package edu.illinois.cs.cogcomp.babySRL.clustering;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A wrapper for the BMMM tool (just tagging for now).
 */
public class BMMMWrapper extends Tagger {

    /**
     * Tags a BabySRL corpus with BMMM categories. Since the BMMM provides a hard clustering, there is no
     * need to tag in context.
     * <br/>
     * <b>NB:</b> Unknown words will be tagged with most frequent category.
     *
     * @param corpus The BabySRL corpus to be tagged
     * @param bmmmFile The output of BMMM; contains a large collection of tagged words
     * @throws FileNotFoundException
     */
    public void tagCorpus(BabySRLCorpus corpus, String bmmmFile) throws IOException {
        // Create the word/tag map
        Map<String, String> wordTagMap = new HashMap<>();
        List<String> lines = LineIO.read(bmmmFile);
        Counter<String> tagCounter = new Counter<>();

        for (String line : lines) {
            for (String wordTag : line.split("\\s+")) {
                int slashIndex = wordTag.lastIndexOf('/');
                String word = wordTag.substring(0, slashIndex).toLowerCase();
                String tag = wordTag.substring(slashIndex + 1);
                tagCounter.incrementCount(tag);
                if (!wordTagMap.containsKey(word)) wordTagMap.put(word, tag);
            }
        }
        // Get the most frequent tag
        String mostFreqTag = tagCounter.getMax().getFirst();

        // Now tag the corpus
        tagCorpusHardClusters(corpus, wordTagMap, mostFreqTag);
    }

    public static void main(String[] args) throws IOException {
        BabySRLCorpus corpus = new BabySRLCorpus();
        String language = "danish";
        String type = "test";
        String corpusFile = "data/ccg/" + language + "/" + language + "." + type + ".naacl.gz";
        corpus.readFile(corpusFile);
        String bmmmFile = "data/ccg/" + language + "/" + language + ".taggedU";
        new BMMMWrapper().tagCorpus(corpus, bmmmFile);
        String outputFile = "data/ccg/" + language + "/" + language + "." + type + ".naacl.tagged.gz";
        corpus.writeCorpus(outputFile);
    }
}
