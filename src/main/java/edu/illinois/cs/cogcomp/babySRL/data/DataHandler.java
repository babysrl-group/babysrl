package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.babySRL.clustering.Tagger;
import edu.illinois.cs.cogcomp.babySRL.core.NounIdentifier;
import edu.illinois.cs.cogcomp.babySRL.core.RandomVerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.core.VerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Tags;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * An interface for the data used to train the classifiers. Provides access to the list of {@link BabySRLSentence}s
 * that make up the training corpus.
 */
public class DataHandler {
    private static Logger logger = LoggerFactory.getLogger(DataHandler.class);
    private static final Random rand = new Random(23);

    private List<BabySRLSentence> trainSentences, testSentences;
    private static BabySRLProperties properties = BabySRLProperties.getInstance();

    /**
     * Main BabySRL data constructor. Use this for all experiments.
     */
    public DataHandler() {
        BabySRLCorpus trainCorpus = new BabySRLCorpus();
        BabySRLCorpus testCorpus = new BabySRLCorpus();
        try {
            // This is the training corpus
            for (String file : properties.getTrainFileNames())
                trainCorpus.readFile(file, properties.getMultiplePredicates());

            // This is the test corpus
            for (String file : properties.getTestFileNames())
                testCorpus.readFile(file, properties.getMultiplePredicates());

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Get the train/test splits
        if (properties.randomTrainTestSplits()) {
            List<BabySRLSentence> allSents = new ArrayList<>();
            allSents.addAll(trainCorpus.getSentences());
            allSents.addAll(testCorpus.getSentences());
            Collections.shuffle(allSents, rand);
            int trainTestBoundary = (int) Math.round(allSents.size() * .8);
            trainSentences = new ArrayList<>();
            testSentences = new ArrayList<>();
            for (int i = 0; i < trainTestBoundary; i++)
                trainSentences.add(allSents.get(i));
            for (int i = trainTestBoundary; i < allSents.size(); i++)
                testSentences.add(allSents.get(i));
        }
        else {
            trainSentences = trainCorpus.getSentences();
            testSentences = testCorpus.getSentences();
        }

        // Re-tag the corpus to fix spurious HMM assignments
        if (properties.retagHMM())
            Tagger.reTagCorpus(trainSentences, testSentences);
    }

    public void runHeuristics(int numSeedNouns, int round) {
        if (properties.getTags() == Tags.hmm) {
            if (numSeedNouns == -1) {
                NounIdentifier.addGoldNounPredictions(testSentences);
            }
            else {
                // Use a true (non-seeded) random generator for picking the seed nouns
                NounIdentifier nounIdentifier = new NounIdentifier(trainSentences, properties.getSeedNounFile(),
                        numSeedNouns, new Random());
                nounIdentifier.generateNounStats(trainSentences);
                nounIdentifier.addNounPredictions(trainSentences);
                nounIdentifier.addNounPredictions(testSentences);
            }

            VerbIdentifier verbIdentifier = new VerbIdentifier();
            verbIdentifier.generateVerbStats(trainSentences);
            verbIdentifier.addVerbPredictions(trainSentences);
            verbIdentifier.addVerbPredictions(testSentences);
            RandomVerbIdentifier randVerbIdentifier = new RandomVerbIdentifier();
            // Choose a truly random verb (each token except punctuation is a candidate)
            randVerbIdentifier.addTrueRandVerbPredictions(trainSentences);
            randVerbIdentifier.addTrueRandVerbPredictions(testSentences);
            // Choose a random verb (each token except function words is a candidate)
            randVerbIdentifier.addFunctionWordRandVerbPredictions(trainSentences);
            randVerbIdentifier.addFunctionWordRandVerbPredictions(testSentences);
            // Choose a random verb after the noun heuristic has removed all nouns
            randVerbIdentifier.addNounRandVerbPredictions(trainSentences);
            randVerbIdentifier.addNounRandVerbPredictions(testSentences);

            logger.debug("Finished running Noun/Verb heuristics.");

            if (round != -1 && properties.createReports()) {
                String file = getReportFileName(numSeedNouns, round);
                printReport(testSentences, verbIdentifier, file);
            }
        }
    }

    public void setAnimacyFeedback() {
        List<String> animateWords = new ArrayList<>();
        List<String> inanimateWords = new ArrayList<>();
        List<String> seedNounList;
        try {
            seedNounList = LineIO.read(properties.getAnimacyNounFile());
            for (String line : seedNounList) {
                String[] splits = line.split("\\s+");
                if (splits[1].equals("1"))
                    animateWords.add(splits[0]);
                else inanimateWords.add(splits[0]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        List<String> animateStates = new ArrayList<>();
        List<String> inanimateStates = new ArrayList<>();
        if (properties.aggregateAnimacy()) {
            // State is considered animate if it appears with a known animate >= 4 times
            // Similarly for inanimate states
            Counter<String> animateStateCounts = new Counter<>();
            Counter<String> inanimateStateCounts = new Counter<>();
            for (BabySRLSentence s : trainSentences) {
                for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                    String word = s.getWord(wordInd);
                    String hmmTag = s.getHmmTag(wordInd);
                    if (animateWords.contains(word))
                        animateStateCounts.incrementCount(hmmTag);
                    else if (inanimateWords.contains(word))
                        inanimateStateCounts.incrementCount(hmmTag);
                }
            }

            for (String state : animateStateCounts.items())
                if (animateStateCounts.getCount(state) >= 4 && isEligibleState(state))
                    animateStates.add(state);
            for (String state : inanimateStateCounts.items())
                // A state is inanimate only if it's not animate
                if (inanimateStateCounts.getCount(state) >= 4 && isEligibleState(state) && !animateStates.contains(state))
                    inanimateStates.add(state);
        }

        // FEEDBACK LEVEL 1
        // Add the animacy information
        for (BabySRLSentence sentence : trainSentences) {
            for (BabySRLArgumentInstance trainInstance : sentence.getArgumentInstances()) {
                int wordInd = trainInstance.getToken();
                String word = sentence.getWord(wordInd);
                String hmmTag = sentence.getHmmTag(wordInd);
                boolean wordAnimate = animateWords.contains(word);
                boolean wordInanimate = inanimateWords.contains(word);
                boolean aggregateAnimate = properties.aggregateAnimacy() && animateStates.contains(hmmTag);
                boolean aggregateInanimate = properties.aggregateAnimacy() && inanimateStates.contains(hmmTag);
                // If the word is contained in the animates list
                if (wordAnimate)
                    sentence.setAnimacyLabel(wordInd, "A0");
                if (wordInanimate)
                    sentence.setAnimacyLabel(wordInd, "A1");
                // If state is contained in the aggregated animacy lists (unless the word has animacy information)
                if (aggregateAnimate && !wordInanimate)
                    sentence.setAnimacyLabel(wordInd, "A0");
                if (aggregateInanimate && !wordAnimate)
                    sentence.setAnimacyLabel(wordInd, "A1");
                else if (properties.seedPluralsMatch()) {
                    if (animateWords.contains(BabySRLCorpus.knownVariants.get(word)))
                        sentence.setAnimacyLabel(wordInd, "A0");
                    else if (inanimateWords.contains(BabySRLCorpus.knownVariants.get(word)))
                        sentence.setAnimacyLabel(wordInd, "A1");
                }
                // Default label is UNK (see BabySRLSentence.addLine())
            }

            // FEEDBACK LEVEL 2
            // If there is at least one animate, all UNKs must be non-agents
            if (sentence.numAnimates() > 0)
                for (BabySRLArgumentInstance trainInstance : sentence.getArgumentInstances()) {
                    int wordInd = trainInstance.getToken();
                    if (sentence.getAnimacyLabel(wordInd).equals("UNK"))
                        sentence.setAnimacyLabel(wordInd, "A1");
                }

            // FEEDBACK LEVEL 3 happens during training (Trainer.train() lines 36-42)
        }
    }

    /**
     * Returns whether the state is a function-word state or not
     * @param state The HMM state to test
     * @return True if the state is not a function-word state
     */
    private boolean isEligibleState(String state) {
        // The property will be -1 if no content/function word split exists
        return !(Integer.parseInt(state) < properties.getFunctionWordState());
    }

    public void resetPredictions() {
        for (BabySRLSentence s : trainSentences) s.resetPredictions();
        for (BabySRLSentence s : testSentences) s.resetPredictions();
    }

    public static String getReportFileName(int numSeedNouns, int round) {
        return properties.getTempDir() + properties.getChildren()[0] + ":" + numSeedNouns + "-" + round;
    }

    private void printReport(List<BabySRLSentence> sentences, VerbIdentifier verbIdentifier, String file) {
        List<String> outLines = new ArrayList<>();
        for (BabySRLSentence s : sentences) {
            int predictedArgNum = s.getPredictedArgNum();
            List<String> patternProbs = new ArrayList<>();
            List<Integer> candidates = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                if (!VerbIdentifier.isAllowedVerb(s, wordInd)) continue;
                candidates.add(wordInd);
            }
            if (candidates.isEmpty()) patternProbs.add("No candidates left");
            for (int wordInd : candidates) {
                String hmmTag = s.getHmmTag(wordInd);
                String pattern = hmmTag + "-" + predictedArgNum;
                String patternPrint = s.getWord(wordInd) + "/" + hmmTag;
                if (s.isPredictedVerbPred(wordInd))
                    patternPrint = "*" + patternPrint + "*";
                if (!verbIdentifier.predicateHistograms.containsKey(pattern)) patternProbs.add(patternPrint + ": NaN");
                else {
                    double prob = verbIdentifier.predicateHistograms.get(pattern);
                    patternProbs.add(patternPrint + ": " + StringUtils.getFormattedTwoDecimal(prob));
                }
            }
            String line = s.getSentStr() + "\n";
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++){
                line += s.getWord(wordInd) + "/" + s.getHmmTag(wordInd);
                if (s.isPredictedNoun(wordInd)) line += "(N)";
                line += " ";
            }
            line += "\n\tCompound nouns: " + s.getCompoundNounInfo();
            if (s.getGoldArgumentInstances().size() > 1) line += "\n\tMultiple gold predicates:\t";
            else line += "\n\tSingle gold predicate:\t";
            for (int predIndex : s.getGoldArgumentInstances().keySet())
                line += s.getWord(predIndex) + "\t";
            line += "\n\tVerb probs (" + predictedArgNum + " arguments) " + patternProbs;
            outLines.add(line);
        }
        try {
            LineIO.write(file, outLines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Create the HMM-level histograms
        outLines = new ArrayList<>();
        Map<String, List<String>> probsMap = new HashMap<>();
        Map<String, List<String>> wordsMap = new HashMap<>();
        Counter<String> stateCounter = new Counter<>();
        Counter<String> stateCounterWinner = new Counter<>();
        Map<String, Double> predicateHistograms = verbIdentifier.predicateHistograms;
        for (BabySRLSentence s : sentences) {
            List<Integer> candidates = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                if (!VerbIdentifier.isAllowedVerb(s, wordInd)) continue;
                candidates.add(wordInd);
            }
            if (candidates.isEmpty()) continue;

            for (int wordInd : candidates) {
                String hmmTag = s.getHmmTag(wordInd);
                stateCounter.incrementCount(hmmTag);
                if (s.isPredictedVerbPred(wordInd))
                    stateCounterWinner.incrementCount(hmmTag);
                String word = s.getWord(wordInd);
                addToMap(wordsMap, hmmTag, word);
                for (int argNo = 0; argNo < 4; argNo++) {
                    String pattern = hmmTag + "-" + argNo;
                    String str;
                    if (!predicateHistograms.containsKey(pattern)) str = argNo + ": NaN";
                    else
                        str = argNo + ": " + StringUtils.getFormattedTwoDecimal(predicateHistograms.get(pattern));
                    addToMap(probsMap, hmmTag, str);
                }
            }
        }
        for (String state : stateCounter.getSortedItemsHighestFirst()) {
            String line = state + "(" + stateCounterWinner.getCount(state) + "/" + stateCounter.getCount(state) + ") :\n";
            line += "\t" + wordsMap.get(state) + "\n";
            line += "\t" + probsMap.get(state);
            outLines.add(line);
        }
        try {
            LineIO.write(file + "-hist.txt", outLines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addToMap(Map<String, List<String>> map, String hmmTag, String string) {
        List<String> temp;
        if (map.containsKey(hmmTag))
            temp = map.get(hmmTag);
        else temp = new ArrayList<>();
        if (!temp.contains(string)) temp.add(string);
        map.put(hmmTag, temp);
    }

    public List<BabySRLSentence> getTrainSentences() {
        return trainSentences;
    }

    public List<BabySRLSentence> getTestSentences() {
        return testSentences;
    }
}
