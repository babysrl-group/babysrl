package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;

import java.util.Random;

/**
 * Created by christos on 7/5/16.
 */
public class RandVerbSimulation {

    private static final Random r = new Random();

    private static double simulation(int sentNum, double hmmAccuracy, double stdAccuracy,
                                     double avgCandidatesNum, double stdCandidatesNum,
                                     double avgVerbsNum, double stdVerbNum) {

        // The probability of random verb guessing is proportional to:
        // - the number of candidate words (non-noun)
        // - the number of true verbs (# propositions)
        // - the accuracy of the HMM (how many true verbs are left)
        // - the accuracy of the noun heuristic (how many true verbs are left)
        double correct = 0;
        for (int sent = 0; sent < sentNum; sent++) {
            float numCandidates;
            if (stdCandidatesNum > 0)
                numCandidates = Math.round((r.nextGaussian() * stdCandidatesNum + avgCandidatesNum));
            else numCandidates = Math.round((r.nextGaussian() + avgCandidatesNum));
            // How many true verbs are there in the original utterance?
            float numTrueVerbs = Math.round(r.nextGaussian() * stdVerbNum + avgVerbsNum);
            // How many of the candidates are actually nouns and how many are not verbs? ~ nounRecall*hmmAccuracy
            float numNonVerbCandidates = Math.round(r.nextGaussian() * stdAccuracy + (1 - hmmAccuracy));
            if (numCandidates - numNonVerbCandidates > 0)
                correct += numTrueVerbs/(double)(numCandidates-numNonVerbCandidates);
        }
        return correct/ sentNum * 100;
    }

    public static void main(String[] args) {
        OneVariableStats adam = new OneVariableStats();
        OneVariableStats eve = new OneVariableStats();
        OneVariableStats sarah = new OneVariableStats();
        for (int i = 0; i < 10; i++) {
            adam.add(simulation(415, 0.83, 0.1, 1.49, 0.92, 1.26, 0.5));
            eve.add(simulation(450, 0.83, 0.1, 1.61, 1.08, 1.34, 0.58));
            sarah.add(simulation(522, 0.83, 0.1, 1.56, 0.96, 1.30, 0.54));
        }
        System.out.println(adam.mean());
        System.out.println(eve.mean());
        System.out.println(sarah.mean());
    }
}
