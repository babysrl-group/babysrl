package edu.illinois.cs.cogcomp.babySRL.inference.lbjava;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.inference.Classifier;
import edu.illinois.cs.cogcomp.babySRL.inference.FeatureExtractor;
import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.math.ArgMax;
import edu.illinois.cs.cogcomp.lbjava.learn.SparseAveragedPerceptron;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * A wrapper for LBJava's {@link SparseAveragedPerceptron} so that it can be used as a multiclass classifier.
 */
public class LBJavaClassifier extends Classifier {
    private static final double RATE = 0.1;
    public static final double THRESHOLD = 4.0;
    public static final double THICKNESS = 1.5;

    private Map<String, SparseAveragedPerceptronLocal> classifiers;

    private static LBJavaClassifier cmbClassifier = new LBJavaClassifier("cmb",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_COUNT, FeatureTypes.NOUN_PATTERN, FeatureTypes.VERB_POSITION}, false));
    private static LBJavaClassifier lexClassifier = new LBJavaClassifier("lex",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE}, false));
    private static LBJavaClassifier ncntClassifier = new LBJavaClassifier("ncnt",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_COUNT}, false));
    private static LBJavaClassifier npatClassifier = new LBJavaClassifier("npat",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_PATTERN}, false));
    private static LBJavaClassifier vposClassifier = new LBJavaClassifier("vpos",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.VERB_POSITION}, false));
    private static LBJavaClassifier vposTrueRandClassifier = new LBJavaClassifier("vpos-true-rand",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.TRUE_RANDOM_VERB_POSITION}, false));
    private static LBJavaClassifier vposFuncRandClassifier = new LBJavaClassifier("vpos-func-rand",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.FUNC_RANDOM_VERB_POSITION}, false));
    private static LBJavaClassifier vposNounRandClassifier = new LBJavaClassifier("vpos-noun-rand",
            new FeatureExtractor(new FeatureTypes[]{FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_RANDOM_VERB_POSITION}, false));

    public static Classifier[] getClassifierSet() {
        return new Classifier[]{lexClassifier, npatClassifier, vposClassifier, vposTrueRandClassifier, vposFuncRandClassifier, vposNounRandClassifier};
    }

    private LBJavaClassifier(String name, FeatureExtractor fex) {
        this.name = name;
        this.fex = fex;
        classifiers = new HashMap<>();
        for (String label : BabySRLCorpus.allLabels)
            classifiers.put(label, new SparseAveragedPerceptronLocal(RATE, THRESHOLD, THICKNESS));
    }

    @Override
    public String prediction(BabySRLArgumentInstance instance) {
        ArgMax<String, Double> argMax = new ArgMax<>("", Double.NEGATIVE_INFINITY);
        int[] featureArray = fex.getFeatureArray(instance);
        double[] values = getValues(featureArray);
        for (String label : classifiers.keySet())
            argMax.update(label, classifiers.get(label).score(featureArray, values));
        return argMax.getArgmax();
    }

    @Override
    public Pair<String, Double> predictionWithScore(BabySRLArgumentInstance instance) {
        ArgMax<String, Double> argMax = new ArgMax<>("", Double.NEGATIVE_INFINITY);
        int[] featureArray = fex.getFeatureArray(instance);
        double[] values = getValues(featureArray);
        for (String label : classifiers.keySet())
            argMax.update(label, classifiers.get(label).score(featureArray, values));
        return new Pair<>(argMax.getArgmax(), argMax.getMaxValue());
    }

    public void promote(BabySRLArgumentInstance instance, String label) {
        int[] featureArray = fex.getFeatureArray(instance);
        double[] values = getValues(featureArray);
        classifiers.get(label).promote(featureArray, values, RATE);
    }

    public void demote(BabySRLArgumentInstance instance, String label) {
        int[] featureArray = fex.getFeatureArray(instance);
        double[] values = getValues(featureArray);
        classifiers.get(label).demote(featureArray, values, RATE);
    }

    public Map<String, Double> predict(BabySRLArgumentInstance instance){
        int[] featureArray = fex.getFeatureArray(instance);
        double[] values = getValues(featureArray);
        Map<String, Double> scores = new HashMap<>();
        for (String label : classifiers.keySet())
            scores.put(label, classifiers.get(label).score(featureArray, values));
        return scores;
    }

    private double[] getValues(int[] featureArray) {
        double[] values = new double[featureArray.length];
        Arrays.fill(values, 1.0);
        return values;
    }

    @Override
    public double getWeight(String label, String feature) {
        SparseAveragedPerceptronLocal labelClassifier = classifiers.get(label);
        return labelClassifier.awv.dot(new int[]{getFex().encode(feature)}, new double[]{1.0});
    }

    public void close() {}
}
