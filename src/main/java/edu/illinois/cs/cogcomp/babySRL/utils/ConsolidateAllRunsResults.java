package edu.illinois.cs.cogcomp.babySRL.utils;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Code to consolidate the results of an experiment run. Creates an average of the 3 children and
 * adds all the elements in a single tab-separated file (to be used in R).
 */
public class ConsolidateAllRunsResults {

	public static void main(String[] args) throws IOException {
		List<String> outLines = new ArrayList<>();

		// Get the title line from one of the files; if the file is not there, exit with error
		String file = "experiments/gold-noCV-stateAggr-noFix/temp/adam:5-0-hist.txt";
		if (!IOUtils.exists(file)) {
			System.err.println("Cannot find file: " + file);
			System.err.println("Missing files/folders needed for results consolidation. Exiting...");
			System.exit(-1);
		}
		List<String> lines = LineIO.read(file);

		String line = lines.get(lines.size() - 2);
		// Remove the "#nouns" column heading and replace it with "seed-nouns"
		line = line.substring(line.indexOf('\t'));
		String newTitleLine = "supervision\taggregation\tchild\tseed-nouns";
		String titleLine = newTitleLine + line;
		outLines.add(titleLine);
		for (String supervision: new String[]{"gold", "noisy30", "animacy"}) {
			for (String aggr: new String[]{"stateAggr", "lexAggr", "noAggr"}) {
				String folder = "experiments/"+supervision+"-noCV-"+aggr+"-noFix/temp";
				for (String child: new String[]{"adam", "eve", "sarah"}) {
					for (int numSeedNouns = 5; numSeedNouns <= 75; numSeedNouns += 5) {
						for (int trial = 0; trial < 10; trial++) {
							file = folder + "/" + child + ":" + numSeedNouns + "-" + trial + "-hist.txt";
							lines = LineIO.read(file);
							line = lines.get(lines.size() - 1);
							String outLine = supervision + "\t" + aggr + "\t" + child + "\t" + line;
							outLines.add(outLine);
						}
					}
				}
			}
		}
		LineIO.write("experiments-r/all-trials-results.tsv", outLines);
	}
}
