package edu.illinois.cs.cogcomp.babySRL.utils;

/**
 * A utility function that returns the values for dynamic noun threshold.
 */
public class NounThreshold {
    private static BabySRLProperties properties = BabySRLProperties.getInstance();

    public enum ThresholdType {CONSTANT, LINEAR, LOG, EXP, INV_EXP}

    private static final int LOWER_LIMIT = properties.getKnownNounThreshold();
    private static final int UPPER_LIMIT = 15;
    private double[] values;

    private double valuesMin, valuesMax;

    public NounThreshold(ThresholdType thresholdType) {
        values = new double[properties.getNumSeedNouns()];

        for (int i = 1; i <= values.length; i++) {
            if (thresholdType == ThresholdType.CONSTANT) values[i - 1] = LOWER_LIMIT;
            else if (thresholdType == ThresholdType.LINEAR) values[i - 1] = i;
            else if (thresholdType == ThresholdType.LOG) values[i - 1] = Math.log(i);
            else if (thresholdType == ThresholdType.EXP) values[i - 1] = Math.exp((double) i / 20);
            else if (thresholdType == ThresholdType.INV_EXP) values[i - 1] = Math.E / Math.exp(1 / (double) i);
        }


        valuesMin = values[0];
        valuesMax = values[values.length-1];
    }

    public int get(int seedNouns) {
        return (int) Math.round(scale(values[seedNouns-1]));
    }

    public double scale(double valueIn) {
        if (valueIn == LOWER_LIMIT) return valueIn;
        return (valueIn - valuesMin) * ((UPPER_LIMIT - LOWER_LIMIT)/(valuesMax-valuesMin)) + LOWER_LIMIT;

    }
}
