package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides access to SNoW via a command-line interface.
 */
public class SnowWrapper {

    private int labelsIDStart, labelsIDEnd;
    private final Process p;
    private BufferedReader stdError, stdOut;
    private BufferedWriter stdIn;

    public static final double RATE = 0.1;
    public static final double THRESHOLD = 4.0;
    public static final double INITIAL = 0.0;

    /** The thickness of the perceptron separator; used during training */
    public static final double THICKNESS = 1.5;

    private SnowWrapper(String command, int labelsIDStart, int labelsIDEnd, String networkFile) throws IOException,
            InterruptedException {
        this.labelsIDStart = labelsIDStart;
        this.labelsIDEnd = labelsIDEnd;
        ProcessBuilder builder = new ProcessBuilder(command.split(" "));
        p = builder.start();
        stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
        stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
        stdIn = new BufferedWriter(new OutputStreamWriter(p.getOutputStream()));

        // Ignore the welcome message
        int linesToIgnore = 8;
        if (IOUtils.exists(networkFile)) linesToIgnore = 7;
        for (int i = 0; i < linesToIgnore; i++) stdOut.readLine();
    }

    /**
     * Sends a set of features to SNoW, receives all the activations and returns the highest one.
     * @param str The comma-separated feature list
     * @return The best label
     * @throws IOException
     */
    public int predictBest(String str) throws IOException {
        return predictBestWithActivation(str).getFirst();
    }

    public Pair<Integer, Double> predictBestWithActivation(String str) throws IOException {
        Map<Integer, Double> predictions = predict(str);
        int maxLabel = -1;
        double maxActivation = -Double.MAX_VALUE;
        for (Map.Entry<Integer, Double> prediction : predictions.entrySet()) {
            int label = prediction.getKey();
            double activation = prediction.getValue();
            if (activation > maxActivation) {
                maxActivation = activation;
                maxLabel = label;
            }
        }
        return new Pair<>(maxLabel, maxActivation);
    }

    /**
     * Sends a set of features to SNoW and returns all the activations.
     * @param str The comma-separated feature list
     * @return A list of {@link Pair}s containing each label and its activation
     * @throws IOException
     */
    public Map<Integer, Double> predict(String str) throws IOException {
        Map<Integer, Double> predictions = new HashMap<>();
        send("e" + str + ":\n");
        // There are ALL_LABELS+2 extra lines ("Example x" + empty line at the end)
        // Ignore the first line
        stdOut.readLine();
        // Every other line has the following format
        // LABEL:      confidence      activation      softmax
        for (int labelID = labelsIDStart; labelID <= labelsIDEnd; labelID++) {
            String s = stdOut.readLine();
            String[] line = s.split("\\s+");
            int label = Integer.parseInt(line[0].substring(0, line[0].length()-1));
            double activation = Double.parseDouble(line[2].trim());
            predictions.put(label, activation);
        }
        // Ignore the last line
        stdOut.readLine();
        return predictions;
    }

    public void promote(String str, int label) throws IOException {
        send("p" + str + ";" + label + ":\n");
    }

    public void demote(String str, int label) throws IOException {
        send("d" + str + ";" + label + ":\n");
    }

    private void send(String str) throws IOException {
        stdIn.write(str);
        stdIn.flush();
    }

    public void close() throws InterruptedException, IOException {
        stdIn.close();
        stdOut.close();
        stdError.close();
        int exitStatus = p.waitFor();
        if (exitStatus != 0)
            System.out.println("SNoW exiting with status: " + exitStatus);
    }


    public static SnowWrapper buildWrapper(Pair<Integer, Integer> learningTargets, String networkFile, boolean forget)
            throws IOException, InterruptedException {
        int labelsIDStart = learningTargets.getFirst();
        int labelsIDEnd = learningTargets.getSecond();
        if (forget) IOUtils.rm(networkFile);
		String binary = "snow";
		if (System.getProperty("os.name").toLowerCase().contains("mac"))
            binary = "snow-mac";
        else if (System.getProperty("os.name").toLowerCase().contains("windows"))
            binary = "snow.exe";
		String command = "./bin/" + binary + " -interactive -F " + networkFile +
                " -P " + RATE + "," + THRESHOLD + "," + INITIAL + ":" +
                labelsIDStart + "-" + labelsIDEnd + " -o allactivations";
        return new SnowWrapper(command, labelsIDStart, labelsIDEnd, networkFile);
    }
}
