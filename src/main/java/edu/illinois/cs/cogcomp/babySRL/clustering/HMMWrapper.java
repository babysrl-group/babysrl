package edu.illinois.cs.cogcomp.babySRL.clustering;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * A wrapper for the binary HMM tool (ohmm_train/ohmm_test)
 */
public class HMMWrapper extends Tagger {
    private static BabySRLProperties properties = BabySRLProperties.getInstance();

    public void tagCorpus(BabySRLCorpus corpus, String modelName) throws IOException {
        String tempFileIn = properties.getTempDir() + "tempIn";
        corpus.writeSingleLineFile(tempFileIn);
        // ARGS: testfile modelfile outfile -s SMOOTHING
        // outfile will be tempFileIn.{marginal,viterbi} (we will use the marginal)
        String args = tempFileIn + " " + modelName + " " + tempFileIn + " -t 1.0 -s 1.0";
        Process p = Runtime.getRuntime().exec("bin/ohmm_test " + args);

        BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

        int exitStatus = 0;
        try {
            exitStatus = p.waitFor();
        } catch (InterruptedException e) {
            System.err.println("HMM thread interrupted");
            System.exit(-1);
        }
        if (exitStatus != 0) {
            System.err.println("Tagging with HMM failed");
            String s;
            while ((s = stdError.readLine()) != null) {
                System.err.println(s);
            }
            stdError.close();
            System.exit(-2);
        }

        List<BabySRLSentence> sentences = corpus.getSentences();
        List<String> hmmTaggedSents = LineIO.read(tempFileIn + ".marginal");
        if (hmmTaggedSents.size() != sentences.size()) {
            System.err.println("HMM-tagged corpus doesn't have the same size as BabySRL corpus");
            System.exit(-3);
        }

        // Add the HMM tag information
        for (int sentInd = 0; sentInd < sentences.size(); sentInd++) {
            String taggedSent = hmmTaggedSents.get(sentInd);
            BabySRLSentence sentence = sentences.get(sentInd);

            List<String> tags = new ArrayList<>();
            for (String wordTag : taggedSent.split("\\s+")) {
                String tag = wordTag.substring(wordTag.lastIndexOf('/') + 1);
                tags.add(tag);
            }
            sentence.setHmmTags(tags);
        }
    }

    public static class HMMTrainer implements Callable<Pair<Integer, Double>> {
        private final String args;
        private final int id;
        public HMMTrainer(int id, String trainFile, String modelName, String args) {
            if (!IOUtils.exists(trainFile)) throw new RuntimeException("File " + trainFile + " not found!");
            this.args = trainFile + " " + modelName + id + " " + args;
            this.id = id;
        }

        @Override
        public Pair<Integer, Double> call() throws IOException, InterruptedException {
            System.out.println("Starting training HMM #" + id);
            Process p = Runtime.getRuntime().exec("bin/ohmm_train " + args);
            BufferedReader stdOut = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));

            // Print a progress
            int count = 0;
            while (stdError.readLine() != null) {
                if (count % 50 == 0) System.out.print('.');
                count++;
            }
            stdError.close();

            int exitStatus = p.waitFor();
            if (exitStatus != 0) return new Pair<>(id, 0.0);
            // The final LogLikelihood should be the only thing on the output buffer
            // First line is empty
            stdOut.readLine();
            String outLine = stdOut.readLine();
            outLine = outLine.substring(outLine.indexOf("LL:") + 3);
            return new Pair<>(id, Double.parseDouble(outLine));
        }
    }

    /**
     * Stores the n most frequent words per class.
     *
     * @param corpus The HMM-tagged BabySRL corpus
     * @param n Number of most frequent words per class to store
     */
    public static Map<String, String> storeHMMTagWords(BabySRLCorpus corpus, int n) {
        Map<String, String> wordsPerTag = new HashMap<>();
        Map<String, Counter<String>> hmmToWords = new HashMap<>();

        for (BabySRLSentence s : corpus.getSentences()) {
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String hmmTag = s.getHmmTag(wordInd);

                // Add tag-to-word counts
                if (!hmmToWords.containsKey(hmmTag)) {
                    Counter<String> counter = new Counter<>();
                    hmmToWords.put(hmmTag, counter);
                }
                hmmToWords.get(hmmTag).incrementCount(s.getWord(wordInd));
            }
        }
        // Store the N most frequent words per class
        for (String tag : hmmToWords.keySet()) {
            // Sort the words in the tag from least to most frequent
            List<String> sortedWords = hmmToWords.get(tag).getSortedItems();
            int size = sortedWords.size();
            String words = "";
            for (int i = 1; i <= n; i++) {
                if (size-i < 0) continue;
                words += sortedWords.get(size-i) + "\t";
            }
            if (size > n) words += "+" + (size-n);
            wordsPerTag.put(tag, words.trim());
        }
        return wordsPerTag;
    }

	public static void main(String[] args) throws IOException, InterruptedException {
		BabySRLProperties properties = BabySRLProperties.getInstance();
        String bestHMMFileName = properties.getHmmDir() + "/hmmModel-best-funct";
        for (String child : properties.getTrainFileNames()) {
            System.out.println("Tagging " + child);
            BabySRLCorpus corpus = new BabySRLCorpus();
            corpus.readFile(child);
            new HMMWrapper().tagCorpus(corpus, bestHMMFileName);
            corpus.writeCorpus(child + ".tagged");
        }
    }
}
