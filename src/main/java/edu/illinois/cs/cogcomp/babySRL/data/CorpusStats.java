package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.babySRL.core.RandomVerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.babySRL.utils.NounThreshold;
import edu.illinois.cs.cogcomp.core.stats.Counter;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;

import java.io.FileNotFoundException;
import java.util.*;

public class CorpusStats {

    public static void getHMMStatesStats() {
        DataHandler handler = new DataHandler();
        handler.runHeuristics(70, -1);
        Counter<String> stateCounter = new Counter<>();
        Counter<String> predCounter = new Counter<>();
        for (BabySRLSentence sentence : handler.getTrainSentences()) {
            for (int wordId = 0; wordId < sentence.getWordCount(); wordId++) {
                String state = sentence.getHmmTag(wordId);
                if (RandomVerbIdentifier.isFunctionWord(sentence, wordId)) continue;
                if (sentence.isPredictedVerbPredRandom(RandomVerbIdentifier.RAND_NOUNS, wordId))
                    predCounter.incrementCount("rand-" + state);
                stateCounter.incrementCount(state);
            }
        }
        for (String state : stateCounter.getSortedItemsHighestFirst()) {
            System.out.println(state + " " + stateCounter.getCount(state));
        }
        for (String state : predCounter.getSortedItemsHighestFirst()) {
            System.out.println(state + " " + predCounter.getCount(state));
        }
    }

    public static Counter<String> getA0A1Stats(BabySRLCorpus corpus) {
        Counter<String> counter = new Counter<>();
        for (BabySRLSentence sentence : corpus.getSentences()) {
            for (int predIndex = 0; predIndex < sentence.getPredicateNum(); predIndex++) {
                if (sentence.getGoldCoreArgNum(predIndex) != 2) continue;
                String sequence = "";
                for (int i = 0; i < sentence.getWordCount(); i++) {
                    sequence += sentence.getGoldArg(i, predIndex) + "-";
                }
                if (sequence.matches(".*0.*1.*")) counter.incrementCount("A0A1");
                else counter.incrementCount("OTHER");
            }
        }
        return counter;
    }

    public static Counter<String> getNounVerbStats(BabySRLCorpus corpus) {
        Counter<String> counter = new Counter<>();
        String lastSent = "";
        for (BabySRLSentence sentence : corpus.getSentences()) {
            // Discount sentences that appear multiple times (because of multiple predicates)
            if (sentence.getSentStr().equals(lastSent)) continue;
            lastSent = sentence.getSentStr();

            int nounNum = 0, verbNum = 0, prepNum = 0, predNum = 0;
            for (int wordId = 0; wordId < sentence.getWordCount(); wordId++) {
                if (sentence.isNoun(wordId)) nounNum++;
                else if (sentence.isVerb(wordId)) {
                    verbNum++;
                    predNum++;
                }
                else if (sentence.isPrep(wordId)) {
                    prepNum++;
                    predNum++;
                }
            }
            counter.incrementCount("Sent");
            counter.incrementCount("V"+verbNum);
            counter.incrementCount("N"+nounNum);
            counter.incrementCount("P"+prepNum);
            // Number of predicates (either verbs or prepositions)
            counter.incrementCount("D"+predNum);
            // Decomposition of predicates
            counter.incrementCount("D"+predNum+":V"+verbNum+"-P"+prepNum);
        }
        return counter;
    }

    public static Counter<String> getNounArgStats(BabySRLCorpus corpus) {
        Counter<String> counter = new Counter<>();
        // I shouldn't discount the duplicate sentences since their arguments
        // should count as negative evidence if there is more than one predicates
        for (BabySRLSentence sentence : corpus.getSentences()) {
            for (int predIndex = 0; predIndex < sentence.getPredicateNum(); predIndex++) {
                for (int wordId = 0; wordId < sentence.getWordCount(); wordId++) {
                    boolean isNoun = sentence.isNoun(wordId);
                    boolean isArg = sentence.getGoldArg(wordId, predIndex).matches("[BI]-A[0-4]");

                    if (isNoun) counter.incrementCount("N");
                    if (isArg) counter.incrementCount("A");
                    if (isNoun && !isArg) counter.incrementCount("xN");
                    if (isArg && !isNoun) counter.incrementCount("xA");
                    if (isNoun && isArg) counter.incrementCount("N-A");
                    if (isNoun || isArg) counter.incrementCount("total");
                }
            }
        }
        return counter;
    }

    public static Map<String, Counter<String>> getAmbiguousWords(BabySRLCorpus corpus) {
        Map<String, Counter<String>> wordTagMap = new HashMap<>();
        String lastSent = "";
        for (BabySRLSentence sentence : corpus.getSentences()) {
            // Discount sentences that appear multiple times (because of multiple predicates)
            if (sentence.getSentStr().equals(lastSent)) continue;
            lastSent = sentence.getSentStr();

            for (int wordId = 0; wordId < sentence.getWordCount(); wordId++) {
                String word = sentence.getWord(wordId);
                String tag = sentence.getSimplifiedTag(wordId);
                Counter<String> tags;
                if (wordTagMap.containsKey(word))
                    tags = wordTagMap.get(word);
                else tags = new Counter<>();
                tags.incrementCount(tag);
                wordTagMap.put(word, tags);
            }
        }
        return wordTagMap;
    }

	/**
	 * Check whether a predicate has the same number of (core) arguments as there are nouns in the sentence
	 *
	 * @param corpus The corpus containing (potentially) multiple predicates per sentence
	 * @return A counter containing the number of sentences that fall under one or the other condition (prefixed by
	 * whether this is a multi- or single-predicate sentence)
	 */
	public static Counter<String> getVerbArityMatch(BabySRLCorpus corpus) throws FileNotFoundException {
		Counter<String> counter = new Counter<>();
		for (BabySRLSentence sentence : corpus.getSentences()) {
			String prefix;
			if (sentence.getGoldArgumentInstances().size() > 1) prefix = "multi-";
			else prefix = "single-";
			int numNouns = 0;
			for (int wordId = 0; wordId < sentence.getWordCount(); wordId++)
				if (sentence.isNoun(wordId)) numNouns++;
			for (int predIndex : sentence.getGoldArgumentInstances().keySet()) {
				counter.incrementCount(prefix + Boolean.toString(sentence.getGoldCoreArgNum(predIndex) == numNouns));
				counter.incrementCount("all-" + Boolean.toString(sentence.getGoldCoreArgNum(predIndex) == numNouns));
			}
		}
		return counter;
	}

    private static String format(double number) {
        return StringUtils.getFormattedTwoDecimal(number);
    }

    private static List<String> getListFromCounter(Counter<String> counter, String regex) {
        List<String> list = new ArrayList<>();
        for (String item : counter.getSortedItemsHighestFirst()) {
            if (item.matches(regex)) list.add(item);
        }
        return list;
    }

    public static void main(String[] args) throws FileNotFoundException {
		BabySRLProperties properties = BabySRLProperties.getInstance();
        BabySRLCorpus corpus = new BabySRLCorpus();
		for (String file : properties.getTrainFileNames())
			corpus.readFile(file);
		for (String file : properties.getTestFileNames())
			corpus.readFile(file);

        Counter<String> stats;
//		  stats = getA0A1Stats(corpus);
//        double proportion = stats.getCount("A0A1") / stats.getTotal() * 100;
//        System.out.println("A0A1% (of all 2-arg sents)\n" + format(proportion) + "\t"
//                + (int)stats.getCount("A0A1") + "/" + (int)stats.getTotal());
//        System.out.println();
//
//        stats = getNounArgStats(corpus);
//        double totalCandidates = stats.getCount("total");
//        double nounProportion = stats.getCount("xN") / totalCandidates * 100;
//        double argProportion = stats.getCount("xA") / totalCandidates * 100;
//        double nounArgsProportion = stats.getCount("N-A") / totalCandidates * 100;
//        System.out.println("Noun arguments [0-4]:\t" + format(nounArgsProportion));
//        System.out.println("Exclusive nouns:\t" + format(nounProportion));
//        System.out.println("Exclusive arguments:\t" + format(argProportion));
//        System.out.println();
//
//        stats = getNounVerbStats(corpus);
//        System.out.println("Total\t" + (int)stats.getCount("Sent"));
//        for (String item : getListFromCounter(stats, "V[0-9]+"))
//            System.out.println(item + "\t" + (int) stats.getCount(item));
//        System.out.println();
//        for (String item : getListFromCounter(stats, "N[0-9]+"))
//            System.out.println(item + "\t" + (int) stats.getCount(item));
//        System.out.println();
//        for (String item : getListFromCounter(stats, "P[0-9]+"))
//            System.out.println(item + "\t" + (int) stats.getCount(item));
//        System.out.println();
//        for (String item : getListFromCounter(stats, "D[0-9]+"))
//            System.out.println(item.replace("D", "Pred") + "\t" + (int) stats.getCount(item));
//        System.out.println();
//        for (String item : getListFromCounter(stats, "D[0-9]+:V[0-9]+-P[0-9]+"))
//            System.out.println(item.replace("D", "") + "\t" + (int) stats.getCount(item));
//        System.out.println();
//
//        Map<String, Counter<String>> wordTagMap = getAmbiguousWords(corpus);
//        Map<String, Integer> wordTagNumMap = new HashMap<>(wordTagMap.size());
//        Counter<String> wordCounter = new Counter<>();
//        int total = wordTagMap.size();
//        int ambWordNum = 0;
//        double tagsNum = 0;
//        for (String word : wordTagMap.keySet()) {
//            Counter<String> tags = wordTagMap.get(word);
//            if (tags.size() > 1) ambWordNum++;
//            tagsNum += tags.size();
//            wordTagNumMap.put(word, tags.size());
//            wordCounter.incrementCount(word, tags.getTotal());
//        }
//        System.out.println("Total words:\t" + total);
//        System.out.println("Ambiguous words:\t" + ambWordNum);
//        System.out.println("Average num of tags:\t" + format(tagsNum / total));
//        int topK = 0;
//        System.out.println("Most ambiguous words:");
//        List<String> list = Sorters.sortMapByValue(wordTagNumMap, new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                return o2 - o1;
//            }
//        });
//        for (String word : list) {
//            if (topK == 10) break;
//            List<String> tagsByFreq = wordTagMap.get(word).getSortedItemsHighestFirst();
//            String tagString = "";
//            for (String tag : tagsByFreq) {
//                tagString += tag + "=" + wordTagMap.get(word).getCount(tag) + "\t";
//            }
//            System.out.println(word + "\t"
//                    + wordCounter.getCount(word) + "\t"
//                    + tagString.trim());
//            topK++;
//        }
//		stats = getVerbArityMatch(corpus);
//		for (String item : getListFromCounter(stats, "single-.*"))
//			System.out.println(item + "\t" + (int) stats.getCount(item));
//		for (String item : getListFromCounter(stats, "multi-.*"))
//			System.out.println(item + "\t" + (int) stats.getCount(item));
//		for (String item : getListFromCounter(stats, "all-.*"))
//            System.out.println(item + "\t" + (int) stats.getCount(item));
        getHMMStatesStats();
    }
}
