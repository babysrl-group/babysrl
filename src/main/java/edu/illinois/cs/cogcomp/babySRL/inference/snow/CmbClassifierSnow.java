package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;

/**
 * A {@link SnowClassifier} with the Combined features (word, predicate-word, noun pattern, verb position)
 *
 * @author Christos Christodoulopoulos
 */
public class CmbClassifierSnow extends SnowClassifier {

    public CmbClassifierSnow() {
        super("cmb", FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_COUNT, FeatureTypes.NOUN_PATTERN, FeatureTypes.VERB_POSITION);
    }
}
