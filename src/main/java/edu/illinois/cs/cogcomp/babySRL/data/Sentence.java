package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * A data structure that contains only the word tokens and their PoS tags.
 * @author Christos Christodoulopoulos
 */
public abstract class Sentence {
    int wordCount;
    private String sentStr;
    List<String> words;
    List<String> goldTags;
    private static BabySRLProperties properties = BabySRLProperties.getInstance();

    Sentence() {
        sentStr = "";
        words = new ArrayList<>();
        goldTags = new ArrayList<>();
    }

    void addLine(String line, int wordIndex, int posIndex) {
        String[] splits = line.split("\\s+");
        String word = splits[wordIndex];
        if (properties.removeQMarks() && word.equals("?")) word = ".";
        String pos = splits[posIndex];

        sentStr += word + " ";
        goldTags.add(pos);
        words.add(word);
        wordCount++;
    }

    public String getSentStr() {
        return sentStr.trim();
    }
    public String getWord(int index) {
        return words.get(index);
    }
    public String getGoldTag(int index) {
        return goldTags.get(index);
    }
    public int getWordCount() {
        return wordCount;
    }

    String getNounPattern(int wordInd) {
        String pattern = "";
        for (int i = 0; i < goldTags.size(); i++) {
            if (i == wordInd)
                pattern += "_";
            else if (isNoun(i))
                pattern += "N";
        }
        return pattern;
    }

    int getNextNoun(int wordInd) {
        for (int i = wordInd + 1; i < goldTags.size(); i++) {
            if (isNoun(i)) return i;
        }
        return -1;
    }

    public boolean isNoun(int index) {
        String tag = goldTags.get(index);
        return tag.startsWith("n") || tag.startsWith("pr");
    }

    boolean isVerb(int index) {
        String tag = goldTags.get(index);
        return tag.startsWith("v");
    }

    boolean isPrep(int index) {
        String tag = goldTags.get(index);
        return tag.equals("in") || tag.equals("to");
    }

    String getSimplifiedTag(int index) {
        String tag = goldTags.get(index);
        if (tag.startsWith("v") || tag.equals("md") || tag.startsWith("aux")) return "verb";

        if (tag.startsWith("n")) return "noun";
        if (tag.startsWith("w")) return "wh";
        if (tag.startsWith("pr")) return "pronoun";
        return tag;
    }
}
