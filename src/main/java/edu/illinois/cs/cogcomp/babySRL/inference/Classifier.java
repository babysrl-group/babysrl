package edu.illinois.cs.cogcomp.babySRL.inference;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;

import java.io.IOException;
import java.util.Map;

public abstract class Classifier {
    protected String name;
    protected FeatureExtractor fex;

    public String getName() {
        return name;
    }

    public abstract String prediction(BabySRLArgumentInstance instance);

    public abstract Pair<String, Double> predictionWithScore(BabySRLArgumentInstance instance);

    public abstract Map<String, Double> predict(BabySRLArgumentInstance instance) throws IOException;

    public abstract void promote(BabySRLArgumentInstance instance, String label);

    public abstract void demote(BabySRLArgumentInstance instance, String label);

    public abstract double getWeight(String label, String feature);

    public abstract void close() throws IOException, InterruptedException;

    public FeatureExtractor getFex() {
        return fex;
    }
}
