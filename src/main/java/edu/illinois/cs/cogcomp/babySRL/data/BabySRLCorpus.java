package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * An interface for reading, writing and manipulating the BabySRL corpus
 */
public class BabySRLCorpus {
    public enum MultiplePredicates { all, first, last, none }
    public enum Supervision {gold, animacy, noisy10, noisy30, noisy10animacy }
    public enum Tags { gold, hmm }
    public enum Scoring {semantic, syntactic}
	public enum Labels { all, core, a0a1 }

    private List<BabySRLSentence> sentences;

    /** All the allowed argument labels in the BabySRL corpus */
    public static String[] allLabels = {"A0", "A1", "A2", "A3", "A4", "AM-TMP", "AM-DIS", "AM-DIR", "AM-MOD",
            "AM-EXT", "AM-ADV", "AM-NEG", "AM-LOC", "AM-RCL", "AM-PRD", "AM-PRP", "AM-MNR", "AM-CAU", "AM-PNC",
            "AM-REC", "O", "V", "rel"};
    static {
        if (BabySRLProperties.getInstance().getSupervision() == Supervision.animacy) {
            allLabels = new String[]{"A0", "A1"};
        }
    }

    public static Map<String, String> knownVariants;
    static {
        knownVariants = new HashMap<>();
        String nounFile = IOUtils.stripFileExtension(BabySRLProperties.getInstance().getSeedNounFile());
        try {
            for (String line : LineIO.read(nounFile + ".variants.txt")) {
                String[] split = line.split("\t");
                knownVariants.put(split[0], split[1]);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Unable to read variants file: " + nounFile + ".variants.txt");
        }
    }

    public BabySRLCorpus() {
        sentences = new ArrayList<>();
    }

    /**
     * A BabySRL corpus reader. Reads an HMM-tagged file in .col format and adds a list of {@link BabySRLSentence}s
     * to the corpus. Also used during HMM training where the HMM tags are all 0.
     * @param file The HMM-tagged column-formatted file
     * @param multiplePredicates How to deal with duplicate sentences (i.e. utterances that have
     *                           more than one predicates)
     * @throws FileNotFoundException If corpus file is missing
     */
    public void readFile(String file, MultiplePredicates multiplePredicates) throws FileNotFoundException {
        List<String> fileLines = LineIO.read(file);

        BabySRLSentence sentence = new BabySRLSentence();
        for (String line : fileLines) {
            if (line.isEmpty()) {
				sentence.setGoldPredArguments();
                if (sentences.size() > 1 && sentence.getSentStr().equals(sentences.get(sentences.size() - 1).getSentStr())) {
                    BabySRLSentence prevSent = sentences.get(sentences.size() - 1);
                    prevSent.addSentence(sentence);
                }
                else sentences.add(sentence);
                sentence = new BabySRLSentence();
                continue;
            }
            sentence.addLine(line);
        }

        if (multiplePredicates == MultiplePredicates.all) return;
        // Remove duplicates
        List<BabySRLSentence> toRemove = new ArrayList<>();
        for (BabySRLSentence s : sentences) {
            if (s.getPredicateNum() > 1) {
                if (multiplePredicates == MultiplePredicates.first) {
                    s.keepFirstPredicate();
                }
                else if (multiplePredicates == MultiplePredicates.last){
                    s.keepLastPredicate();
                }
                else if (multiplePredicates == MultiplePredicates.none) {
                    toRemove.add(s);
                }
            }
        }
        for (BabySRLSentence s : toRemove)
            sentences.remove(s);
    }

    public void readFile(String file) throws FileNotFoundException {
        readFile(file, MultiplePredicates.all);
    }

    public List<BabySRLSentence> getSentences() {
        return sentences;
    }

    /**
     * Create a single-sentence-per-line version of the BabySRL corpus
     * @param file The output file
     */
    public void writeSingleLineFile(String file) throws IOException {
        List<String> outLines = new ArrayList<>(sentences.size());

        for (BabySRLSentence s : sentences) {
            outLines.add(s.getSentStr());
        }

        LineIO.write(file, outLines);
    }

    public void writeCorpus(String outputFile) throws IOException {
        List<String> outLines = new ArrayList<>();
        for (BabySRLSentence sentence : getSentences()) {
            outLines.add(sentence.getSentColumns());
        }
        LineIO.write(outputFile, outLines);
    }
}
