package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.babySRL.inference.Evaluator;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.OneVariableStats;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class that contains the results for each round of the supervised experiment and can produce aggregate statistics.
 * We need to create a copy of this class for each classifier we use (see {@link Evaluator}).
 */
public class SupervisedExperimentResults {
	private Logger logger = LoggerFactory.getLogger(SupervisedExperimentResults.class);

	private OneVariableStats testPrec = new OneVariableStats();
	private OneVariableStats testRec = new OneVariableStats();
	private OneVariableStats testF1 = new OneVariableStats();
	private double latestTestF1;

	private Map<String, OneVariableStats> gorpResults = new HashMap<>();
	private Map<String, Double> latestGorpF1 = new HashMap<>();

	private List<SupervisedResultRecord> resultRecords = new ArrayList<>();

	public void addResultRecord(SupervisedResultRecord resultRecord) {
		resultRecords.add(resultRecord);
	}

	public void addTestSetResult(EvaluationRecord eval) {
		testPrec.add(eval.getPrecision()*100);
		testRec.add(eval.getRecall()*100);
		testF1.add(eval.getF1()*100);
		latestTestF1 = eval.getF1()*100;
	}

	public void addGorpResult(String pattern, double percentage) {
		if (!gorpResults.containsKey(pattern)) {
			logger.debug("Adding {} pattern", pattern);
			gorpResults.put(pattern, new OneVariableStats());
		}
		gorpResults.get(pattern).add(percentage);
		latestGorpF1.put(pattern, percentage);
	}

	public static String getMeanResultsCompactTitle(String name) {
		return name+"-testF1\t"+name+"-testF1-std\t"+name+"-AgorpsA0\t"+name+"-AgorpsA0-std\t"+
				name+"-AgorpBA0A1\t"+name+"-AgorpBA0A1-std\t"+name+"-ABgorpA0A1\t"+name+"-ABgorpA0A1-std";
	}
	public static String getLatestMeanResultsCompactTitle(String name) {
		return name+"-testF1\t"+name+"-AgorpsA0\t"+name+"-AgorpBA0A1\t"+name+"-ABgorpA0A1";
	}

	public String getMeanResultsCompact() {
		return format(testF1.mean()) + "\t" +
				format(testF1.stdErr()) + "\t" +
				format(gorpResults.get("AgorpsA0").mean()) + "\t" +
				format(gorpResults.get("AgorpsA0").stdErr()) + "\t" +
				format(gorpResults.get("AgorpBA0A1").mean()) + "\t" +
				format(gorpResults.get("AgorpBA0A1").stdErr()) + "\t" +
				format(gorpResults.get("ABgorpA0A1").mean()) + "\t" +
				format(gorpResults.get("ABgorpA0A1").stdErr());
	}

	public String getLatestResultsCompact() {
		return format(latestTestF1) + "\t" +
				format(latestGorpF1.get("AgorpsA0")) + "\t" +
				format(latestGorpF1.get("AgorpBA0A1")) + "\t" +
				format(latestGorpF1.get("ABgorpA0A1"));
	}

	private static String format(double num) {
		return StringUtils.getFormattedTwoDecimal(num);
	}

	public void printResultRecords(String fileName) throws IOException {
		for (SupervisedResultRecord resultRecord : resultRecords)
			LineIO.append(fileName, resultRecord.toString());
	}
}
