package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.babySRL.core.RandomVerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Scoring;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Tags;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;

import java.util.*;

/**
 * An extension of {@link Sentence} that contains all the information encoded in
 * the babySRL train/test data
 * @author Christos Christodoulopoulos
 */
@SuppressWarnings("unused")
public class BabySRLSentence extends Sentence {
    private List<Integer> goldPredicatePos;
    private Map<Integer, String> goldPredicateLemma;

    /** The list of gold SRL labels (one for each predicate), including the predicate */
    private Map<Integer, List<String>> goldPredSRLs;

	private List<String> tempGoldSRLs;

    /** A list of words predicted as verb predicates */
    private List<Integer> predictedVerbPredicates;

    /** A list of words predicted as non-verb predicates mapped to a descriptor
     * of the type of random identifier used ({@link RandomVerbIdentifier}) */
    private Map<String, List<Integer>> predictedVerbPredicatesRandom;

    /** A list of words predicted as noun */
    private List<Integer> predictedNouns;

    /** A list of compound nouns marked as pairs of integers (startIndex, endIndex) */
    private List<Pair<Integer, Integer>> compoundNouns;

    /** A list of words predicted as argument (takes compound nouns into account) */
    private List<Integer> predictedArguments;

    private List<String> hmmTags;
    private List<String> chunks;

    /** A collection of the noun SRL arguments of a sentence (one for each predicate). Used during evaluation. */
    private Map<Integer, List<BabySRLArgumentInstance>> goldArgumentInstances;

    /** The list of animacy-based labels */
    private List<String> animacySRLs;

    private List<String> animacySRLsBackup;

    /** Use a fixed random seed */
    private static final Random random = new Random(2);

    private final static BabySRLProperties properties = BabySRLProperties.getInstance();

    public int candidateNum;
    public int goldPredAmongCandidates;

    /** A 0/1 counter to keep track of whether more than 3 arguments were found by the verb heuristic. */
    public int maxArgsFound;

    /** A 0/1 counter to keep track of whether the noun-counting heuristic leads to a tie. */
    public int heuristicTie;

    /** The list of all argument instances in the sentence. Can be gold or induced depending on the settings. */
	private List<BabySRLArgumentInstance> argumentInstances;

    public BabySRLSentence() {
        super();
        goldPredicatePos = new ArrayList<>();
        goldPredicateLemma = new HashMap<>();
        goldPredSRLs = new HashMap<>();
		tempGoldSRLs = new ArrayList<>();
        predictedNouns = new ArrayList<>();
        compoundNouns = new ArrayList<>();
        predictedVerbPredicates = new ArrayList<>();
        predictedVerbPredicatesRandom = new HashMap<>();
        hmmTags = new ArrayList<>();
        chunks = new ArrayList<>();
        goldArgumentInstances = new HashMap<>();
        animacySRLs = new ArrayList<>();
    }

    void addSentence(BabySRLSentence other) {
        // 'other' should only have a single predicate at this stage
        int otherPredPos = other.goldPredicatePos.get(0);

		// Ignore repetitions (sentences that contain exactly the same SRL)
		if (otherPredPos == goldPredicatePos.get(0)) return;

		goldPredicatePos.add(otherPredPos);
        goldPredicateLemma.put(otherPredPos, other.goldPredicateLemma.get(otherPredPos));
        goldPredSRLs.put(otherPredPos, other.goldPredSRLs.get(otherPredPos));
        goldArgumentInstances.put(otherPredPos, other.goldArgumentInstances.get(otherPredPos));
    }

    void addLine(String line) {
        // We need lower-casing for babySRL
        super.addLine(line.toLowerCase(), 5, 4);

        String[] splits = line.split("\\s+");
        // Gold labels may have BIO prefixes
        String goldLabel = splits[0];
        if (goldLabel.startsWith("B") || goldLabel.startsWith("I"))
            goldLabel = goldLabel.substring(goldLabel.indexOf('-') + 1);

        if (properties.getTrainLabels() == BabySRLCorpus.Labels.a0a1){
            if (!goldLabel.matches("(B-)*A[0-1]")) goldLabel = "UNK";
        }
        else if (properties.getTrainLabels() == BabySRLCorpus.Labels.core) {
            if (!goldLabel.matches("(B-)*A[0-4]")) goldLabel = "UNK";
        }

        String hmm = splits[1];
        int wordPos = Integer.parseInt(splits[2]);
        String chunk = splits[3];
        String head = splits[10];

        // Use the first word identified as verb as the verb
        if (head.equals("H")) {
            goldPredicatePos.add(wordPos);
			goldPredicateLemma.put(wordPos, splits[8]);
        }
        tempGoldSRLs.add(goldLabel);
        hmmTags.add(hmm);
        chunks.add(chunk);

        // Initialise the animacy-based SRL list
        animacySRLs.add("UNK");
    }

    void setGoldPredArguments() {
        List<BabySRLArgumentInstance> babySRLArgumentInstances;

        // We should only have a single predicate at this stage
        int predIndex = goldPredicatePos.get(0);
		goldPredSRLs.put(predIndex, tempGoldSRLs);
        if (goldArgumentInstances.containsKey(predIndex))
            babySRLArgumentInstances = goldArgumentInstances.get(predIndex);
        else babySRLArgumentInstances = new ArrayList<>();

        for (int index = 0; index < getWordCount(); index++) {
            if (isGoldArgument(index)) {
                // Since these are only going to be used with `Tags.gold`
                // the random predicate index shouldn't have any impact at this stage
                babySRLArgumentInstances.add(new BabySRLArgumentInstance(this, index, predIndex, null));
            }
        }
        goldArgumentInstances.put(predIndex, babySRLArgumentInstances);
    }

    int getPredicateNum() {
        return goldPredicatePos.size();
    }

    void keepFirstPredicate() {
		for (int i = 1; i < getPredicateNum(); i++) {
			int predIndex = goldPredicatePos.remove(i);
			goldArgumentInstances.remove(predIndex);
			goldPredSRLs.remove(predIndex);
            goldPredicateLemma.remove(predIndex);
        }
    }

    void keepLastPredicate() {
		for (int i = 0; i < getPredicateNum() - 1; i++) {
			int predIndex = goldPredicatePos.remove(i);
			goldArgumentInstances.remove(predIndex);
			goldPredSRLs.remove(predIndex);
            goldPredicateLemma.remove(predIndex);
        }
    }

    public int getGoldCoreArgNum(int predIndex) {
        int goldArgNum = 0;
        for (String goldArg : goldPredSRLs.get(predIndex)) {
            if (goldArg.matches("(B-)*A[0-4]"))
                goldArgNum++;
        }
        return goldArgNum;
    }

	public int getGoldA0A1ArgNum(int predIndex) {
		int goldArgNum = 0;
		for (String goldArg : goldPredSRLs.get(predIndex)) {
			if (goldArg.matches("(B-)*A[0-1]"))
				goldArgNum++;
		}
		return goldArgNum;
	}

    public String getHmmTag(int index) {
        return hmmTags.get(index);
    }
    public String getGoldArg(int index, int predIndex) {
        // In the case of incorrect predIndex, just pick a random verb
        if (!goldPredSRLs.containsKey(predIndex)) {
            Integer[] keys = goldPredSRLs.keySet().toArray(new Integer[goldPredSRLs.size()]);
            predIndex = keys[new Random().nextInt(keys.length)];
        }
        return goldPredSRLs.get(predIndex).get(index);
    }

    public void setHmmTags(List<String> hmmTags) {
        this.hmmTags = hmmTags;
    }

    /**
     * @return The {@link BabySRLSentence} in column format
     */
    String getSentColumns() {
        String columns = "";
        String headInfo;
        for (int predIndex : goldPredicatePos) {
            for (int i = 0; i < wordCount; i++) {
                if (i < predIndex)
                    headInfo = "B";
                else if (i == predIndex)
                    headInfo = "H";
                else headInfo = "A";
                columns += goldPredSRLs.get(predIndex).get(i) + "\t" + hmmTags.get(i) + "\t" + i + "\t" +
                        chunks.get(i) + "\t" + goldTags.get(i) + "\t" +
                        words.get(i) + "\t-\t-\t" + goldPredicateLemma.get(predIndex) +
                        "\t-\t" + headInfo + "\t" + "\n";
            }
            columns += "\n";
        }
        return columns;
    }

    public void setPredictedNoun(int wordInd) {
        if (properties.isCompoundNouns() && wordInd > 0) {
            int prevIndex = wordInd - 1;
            // If the previous word is a noun, add it to the list of compounds
            // but only if it's the second noun (otherwise add it to an existing compound)
            if (predictedNouns.contains(prevIndex)) {
                boolean found = false;
                for (int i = 0; i < compoundNouns.size(); i++) {
                    Pair<Integer, Integer> index = compoundNouns.get(i);
                    if (index.getSecond() == prevIndex) {
                        found = true;
                        compoundNouns.set(i, new Pair<>(index.getFirst(), wordInd));
                    }
                }
                if (!found) compoundNouns.add(new Pair<>(prevIndex, wordInd));
            }
        }
        // Add everything to the list of predicted arguments
        // We can consult the list of compounds separately later
        predictedNouns.add(wordInd);
    }

    public boolean isPredictedNoun(int wordInd) {
        return predictedNouns.contains(wordInd);
    }

    public void setAnimacyLabel(int wordInd, String label) {
        animacySRLs.set(wordInd, label);
    }

    public String getAnimacyLabel(int wordInd) {
        return animacySRLs.get(wordInd);
    }

	public int numAnimates() {
		int count = 0;
		for (String label : animacySRLs) {
			if (label.equals("A0")) count++;
		}
		return count;
	}

    String getCompoundNounInfo() {
        String compound = "";

        for (Pair<Integer, Integer> index : compoundNouns) {
            String compoundNoun = createCompoundNoun(index.getFirst(), index.getSecond());
            if (compoundNoun != null)
                compound += compoundNoun;
        }
        return compound;
    }

    private String createCompoundNoun(int start, int end) {
        if (start < 0 || end < 0 || (end - start < 1)) return null;
        String compound = "";
        for (int i = start; i <= end; i++) {
            compound += getWord(i) + "/" + getGoldTag(i) + " ";
        }
        return compound.trim() + "#";
    }

	public int getPredictedArgNum() {
		return getPredictedArguments().size();
	}

    public void setPredictedVerbPred(int wordInd) {
        predictedVerbPredicates.add(wordInd);
    }

    public void setPredictedVerbPredicatesRandom(String identifier, int wordInd) {
        List<Integer> predictions;
        if (predictedVerbPredicatesRandom.containsKey(identifier))
            predictions = predictedVerbPredicatesRandom.get(identifier);
        else predictions = new ArrayList<>();
        predictions.add(wordInd);
        predictedVerbPredicatesRandom.put(identifier, predictions);
    }

    public boolean isPredictedVerbPred(int wordInd) {
        return predictedVerbPredicates.contains(wordInd);
    }
    public boolean isPredictedVerbPredRandom(String identifier, int wordInd) {
        return predictedVerbPredicatesRandom.get(identifier).contains(wordInd);
    }
    public boolean isGoldVerbPred(int wordInd) {
        if (properties.getScoring() == Scoring.semantic) {
            if (goldPredicatePos.contains(wordInd)) return true;
        }
        else if (properties.getScoring() == Scoring.syntactic) {
            if (goldPredicatePos.contains(wordInd)) return true;
            if (goldTags.get(wordInd).startsWith("v")) return true;
            if (goldTags.get(wordInd).equals("aux")) return true;
        }
        return false;
    }

    public boolean isGoldArgument(int wordInd) {
        if (properties.getScoring() == Scoring.semantic) {
            for (List<String> predGoldSRLs : goldPredSRLs.values()) {
                if (predGoldSRLs.get(wordInd).matches("(B-)*A[0-4]")) return true;
            }
        }
        else if (properties.getScoring() == Scoring.syntactic) {
            return goldTags.get(wordInd).startsWith("n") || goldTags.get(wordInd).startsWith("prp");
        }
        return false;
    }

    private int getPredictedPredicateIndex() {
        if (predictedVerbPredicates.size() > 0)
            return predictedVerbPredicates.get(0);
        return -1;
    }

    private int getRandomPredicateIndex(String identifier) {
        if (predictedVerbPredicatesRandom.get(identifier).size() > 0)
            return predictedVerbPredicatesRandom.get(identifier).get(0);
        return -1;
    }

    void resetPredictions() {
        predictedNouns.clear();
        predictedArguments = null;
        compoundNouns.clear();
        predictedVerbPredicates.clear();
        predictedVerbPredicatesRandom.clear();
        argumentInstances = null;
    }

    public void storeAnimacyAssignments() {
        animacySRLsBackup = new ArrayList<>(animacySRLs);
    }

    public void resetAnimacyAssignments() {
        animacySRLs = new ArrayList<>(animacySRLsBackup);
    }

	public List<BabySRLArgumentInstance> getArgumentInstances() {
		return getArgumentInstances(false);
	}

    public List<BabySRLArgumentInstance> getArgumentInstances(boolean useGold) {
        if (argumentInstances != null) return argumentInstances;
        argumentInstances = new ArrayList<>();

        Tags tags = properties.getTags();
        if (useGold || tags == Tags.gold) {
            for (int predIndex : goldArgumentInstances.keySet())
                argumentInstances.addAll(goldArgumentInstances.get(predIndex));

        }
        else if (tags == Tags.hmm) {
            int predIndex = getPredictedPredicateIndex();
            Map<String, Integer> randPredIndex = new HashMap<>();
            randPredIndex.put(RandomVerbIdentifier.RAND_TRUE, getRandomPredicateIndex(RandomVerbIdentifier.RAND_TRUE));
            randPredIndex.put(RandomVerbIdentifier.RAND_FUNCTION_WORDS, getRandomPredicateIndex(RandomVerbIdentifier.RAND_FUNCTION_WORDS));
            randPredIndex.put(RandomVerbIdentifier.RAND_NOUNS, getRandomPredicateIndex(RandomVerbIdentifier.RAND_NOUNS));
            // If there is no predicate, return an empty list
            if (predIndex == -1) return argumentInstances;
            for (int argIndex : getPredictedArguments())
                argumentInstances.add(new BabySRLArgumentInstance(this, argIndex, predIndex, randPredIndex));
        }
        return argumentInstances;
    }

    public Map<Integer, List<BabySRLArgumentInstance>> getGoldArgumentInstances() {
        return goldArgumentInstances;
    }

    String getNounPatternInduced(int wordInd) {
        String pattern = "";
        for (int i = 0; i < words.size(); i++) {
            if (i == wordInd)
                pattern += "_";
            else if (getPredictedArguments().contains(i))
                pattern += "N";
        }
        return pattern;
    }

	public List<String> getHmmTags() {
		return hmmTags;
	}

    /**
     * Returns the arguments predicted by the noun heuristic taking into account any compound nouns
     * that might have been created.
     *
     * @return A list of predicted arguments
     * @see edu.illinois.cs.cogcomp.babySRL.core.NounIdentifier
     */
    private List<Integer> getPredictedArguments() {
        if (predictedArguments != null) return predictedArguments;
        predictedArguments = new ArrayList<>();
        int i = 0;
        while (i < wordCount) {
            boolean found = false;
            for (Pair<Integer, Integer> compound : compoundNouns) {
                if (i >= compound.getFirst() && i<= compound.getSecond()) {
                    predictedArguments.add(randomFromCompound(compound));
                    found = true;
                    i = compound.getSecond();
                }
            }
            if (!found && predictedNouns.contains(i)) predictedArguments.add(i);
            i++;
        }
        return predictedArguments;
    }

    private int randomFromCompound(Pair<Integer, Integer> compound) {
        List<Integer> choices = new ArrayList<>();
        for (int i = compound.getFirst(); i <= compound.getSecond(); i++) choices.add(i);
        return choices.get(random.nextInt(choices.size()));
    }

    String getNoisyLabel(int token, int predIndex, BabySRLCorpus.Supervision supervision) {
        double prob = (supervision == BabySRLCorpus.Supervision.noisy10) ? .1 : .3;
        if (random.nextDouble() > prob) return getGoldArg(token, predIndex);
        return "UNK";
    }
}
