package edu.illinois.cs.cogcomp.babySRL.core;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.util.*;

/**
 * A baseline to the {@link VerbIdentifier}, it chooses randomly either between all non-function words or
 * between non-noun candidates (equivalent to the input of the {@link VerbIdentifier}).
 */
public class RandomVerbIdentifier {
    private static BabySRLProperties properties = BabySRLProperties.getInstance();
    /** A true random choice over all words (including function words but excluding punctuation */
    public static final String RAND_TRUE = "rand-true";
    /** Random choice between all content words */
    public static final String RAND_FUNCTION_WORDS = "rand-fucnt";
    /** Random choice between all non-noun function words */
    public static final String RAND_NOUNS = "rand-noun";

    public void addTrueRandVerbPredictions(List<BabySRLSentence> sentences) {
        // Counts the number of times each state has been randomly selected as predicate
        Map<String, Counter<Boolean>> predictionMap = new HashMap<>();
        for (BabySRLSentence s : sentences) {
            // Add all the allowed predicates in a list a pick one at random
            List<Integer> candidates = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                if (isPunctuation(s, wordInd)) continue;
                candidates.add(wordInd);
            }
            int randomVerbIndex = getRandomVerbIndex(candidates);
            if (randomVerbIndex < 0 && properties.aggregateRandVerbPred())
                continue;
            if (!properties.aggregateRandVerbPred()) {
                s.setPredictedVerbPredicatesRandom(RAND_TRUE, randomVerbIndex);
            }
            else VerbIdentifier.aggregatePredictions(predictionMap, s, randomVerbIndex);
        }
        // If we are aggregating the verb predictions we need to revisit the sentences and assign the
        // state with the best ratio (#times max pred/#times appeared) as our prediction
        if (properties.aggregateRandVerbPred())
            setAggregatedPrediction(sentences, predictionMap, RAND_TRUE);
    }

    public void addFunctionWordRandVerbPredictions(List<BabySRLSentence> sentences) {
        // Counts the number of times each state has been randomly selected as predicate
        Map<String, Counter<Boolean>> predictionMap = new HashMap<>();
        for (BabySRLSentence s : sentences) {
            // Add all the allowed predicates in a list a pick one at random
            List<Integer> candidates = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                if (isFunctionWord(s, wordInd)) continue;
                candidates.add(wordInd);
            }
            int randomVerbIndex = getRandomVerbIndex(candidates);
            if (randomVerbIndex < 0 && properties.aggregateRandVerbPred())
                continue;
            if (!properties.aggregateRandVerbPred()) {
                s.setPredictedVerbPredicatesRandom(RAND_FUNCTION_WORDS, randomVerbIndex);
            }
            else VerbIdentifier.aggregatePredictions(predictionMap, s, randomVerbIndex);
        }
        // If we are aggregating the verb predictions we need to revisit the sentences and assign the
        // state with the best ratio (#times max pred/#times appeared) as our prediction
        if (properties.aggregateRandVerbPred())
            setAggregatedPrediction(sentences, predictionMap, RAND_FUNCTION_WORDS);
    }

    public void addNounRandVerbPredictions(List<BabySRLSentence> sentences) {
        // Counts the number of times each state has been randomly selected as predicate
        Map<String, Counter<Boolean>> predictionMap = new HashMap<>();
        for (BabySRLSentence s : sentences) {
            // Add all the allowed predicates in a list a pick one at random
            List<Integer> candidates = new ArrayList<>();
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                if (!VerbIdentifier.isAllowedVerb(s, wordInd)) continue;
                candidates.add(wordInd);
            }
            int randomVerbIndex = getRandomVerbIndex(candidates);
            if (randomVerbIndex < 0 && properties.aggregateRandVerbPred())
                continue;
            if (!properties.aggregateRandVerbPred()) {
                s.setPredictedVerbPredicatesRandom(RAND_NOUNS, randomVerbIndex);
            }
            else VerbIdentifier.aggregatePredictions(predictionMap, s, randomVerbIndex);
        }
        // If we are aggregating the verb predictions we need to revisit the sentences and assign the
        // state with the best ratio (#times max pred/#times appeared) as our prediction
        if (properties.aggregateRandVerbPred())
            setAggregatedPrediction(sentences, predictionMap, RAND_NOUNS);
    }

    private void setAggregatedPrediction(List<BabySRLSentence> sentences, Map<String, Counter<Boolean>> predictionMap,
                                         String identifier) {
        for (BabySRLSentence s : sentences) {
            int bestVerbIndex = VerbIdentifier.getBestVerbIndex(predictionMap, s);
            // Set predicted predicate
            s.setPredictedVerbPredicatesRandom(identifier, bestVerbIndex);
        }
    }

    static int getRandomVerbIndex(List<Integer> candidates) {
        if (candidates.isEmpty()) return -1;
        return candidates.get(new Random().nextInt(candidates.size()));
    }

    private static boolean isPunctuation(BabySRLSentence sentence, int wordInd) {
        return sentence.getWord(wordInd).matches("\\p{Punct}");
    }
    @SuppressWarnings("RedundantIfStatement")
    public static boolean isFunctionWord(BabySRLSentence sentence, int wordInd) {
        // Ignore punctuation (from HMMStateIdent.pl, line 548)
        if (sentence.getWord(wordInd).matches("\\p{Punct}")) return  true;
        // Finally, ignore all function word states
        // (the property will be -1 if no content/function word split exists)
        if (Integer.parseInt(sentence.getHmmTag(wordInd)) < properties.getFunctionWordState()) return true;
        return false;
    }
}
