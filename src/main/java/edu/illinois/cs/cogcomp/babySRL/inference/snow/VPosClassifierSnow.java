package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;

/**
 * A {@link SnowClassifier} with the VPos features (word, predicate-word, verb position)
 *
 * @author Christos Christodoulopoulos
 */
public class VPosClassifierSnow extends SnowClassifier {

    public VPosClassifierSnow() {
        super("vpos", FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.VERB_POSITION);
    }
}
