package edu.illinois.cs.cogcomp.babySRL.inference;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;

/**
 * The set of all possible feature types associated with a {@link BabySRLArgumentInstance}.
 */
public enum FeatureTypes {
    /** The surface form of the candidate */
    WORD,
    /** The <b>lemma</b> of the predicate */
    PREDICATE,
    /** The relative position of the candidate wrt other nouns (e.g. 2nd of 3) */
    NOUN_PATTERN,
	/** How many nouns exist in a sentence */
	NOUN_COUNT,
    /** The relative position of the candidate wrt the predicate (before or after) */
    VERB_POSITION,
    /** Same as {@link #VERB_POSITION} but with a random predicate prediction instead of the verb heuristic */
    TRUE_RANDOM_VERB_POSITION,
    FUNC_RANDOM_VERB_POSITION,
    NOUN_RANDOM_VERB_POSITION
}
