package edu.illinois.cs.cogcomp.babySRL.utils;

import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.*;
import edu.illinois.cs.cogcomp.core.utilities.configuration.ResourceManager;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * A handler for the project properties. Uses <i>config/babySRL.properties</i> by default.
 */
public class BabySRLProperties {
    private static BabySRLProperties instance;
    private final ResourceManager properties;
    private static final String propertiesFile = "config/babySRL.properties";

    private String tempDir, dataDir, hmmDir, trainTestDir, seedNounFile, hmmTrainFile, hmmFunctionWordsFile,
            animacyNounFile, gorpDir;
    private String[] children;
    private int numRoundsSupervised, numRoundsUnsupervised, hmmStates, numSeedNouns,
            knownNounThreshold, functionWordState;
    private boolean removeQMarks, randSeeds, seedPluralsMatch, compoundNouns, aggregateVerbPred,
            aggregateRandVerbPred, fixHMM, aggregateLexical, createReports, randomTrainTestSplits, aggregateAnimacy;
    private double fixHMMThreshold;
    private MultiplePredicates multiplePredicates;
    private Scoring scoring;
    private Supervision supervision;
    private Tags tags;
	private Labels trainLabels, testLabels;
    private NounThreshold.ThresholdType thresholdType;

    public BabySRLProperties() throws IOException {
        properties = new ResourceManager(propertiesFile);
		dataDir = getString("DataDir");
		hmmDir = getString("HMMDir");
		tempDir = getString("TempDir");
		numRoundsSupervised = getInt("NumRoundsSupervised");
		numRoundsUnsupervised = getInt("NumRoundsUnsupervised");
        hmmStates = getInt("HMMStates");
        numSeedNouns = getInt("NumSeedNouns");
		trainTestDir = getString("TrainTestDir");
		children = getCommaSeparatedValues("Children");
        seedNounFile = getString("SeedNounFile");
        knownNounThreshold = getInt("KnownNounThreshold");
        hmmTrainFile = getString("HMMTrainFile");
        hmmFunctionWordsFile = getString("HMMFunctionWordsFile", "");
        functionWordState = getInt("FunctionWordState", "-1");
        removeQMarks = getBoolean("RemoveQuestionMarks");
        randSeeds = getBoolean("RandSeeds");
        seedPluralsMatch = getBoolean("SeedPluralsMatch");
        multiplePredicates = MultiplePredicates.valueOf(getString("MultiplePredicates"));
        scoring = Scoring.valueOf(getString("Scoring"));
        supervision = Supervision.valueOf(getString("Supervision"));
        tags = Tags.valueOf(getString("Tags"));
        compoundNouns = getBoolean("CompoundNouns");
		trainLabels = Labels.valueOf(getString("TrainLabels"));
		testLabels = Labels.valueOf(getString("TestLabels"));
		aggregateVerbPred = getBoolean("AggregateVerbPred");
		aggregateRandVerbPred = getBoolean("AggregateRandVerbPred");
        aggregateLexical = getBoolean("AggregateLexical");
		animacyNounFile = getString("AnimacyNounFile");
		gorpDir = getString("GorpDir");
        fixHMM = getBoolean("FixHMM");
        fixHMMThreshold = getDouble("FixHMMThreshold");
        thresholdType = NounThreshold.ThresholdType.valueOf(getString("ThresholdType"));
        createReports = getBoolean("CreateReports");
        randomTrainTestSplits = getBoolean("RandomTrainTestSplits");
        aggregateAnimacy = getBoolean("AggregateAnimacy");
    }

    public static BabySRLProperties getInstance() {
        if (instance == null)
            try {
                instance = new BabySRLProperties();
            } catch (IOException e) {
                System.err.println("Could not load properties file");
                System.exit(-1);
            }
        return instance;
    }

    /**
     * Retrieves a property and replaces any variables it may have. Assumes variable names are
     * written in the form {VAR}
     * @param propertyName The name of the property to be retrieved
     * @return The property value (as a String) with its variables replaced.
     */
    private String getString(String propertyName, String defaultValue) {
        String property = properties.getString(propertyName, defaultValue);
        if (property.contains("{")) {
            String variable = property.substring(property.indexOf('{') + 1, property.indexOf('}'));
            String variableValue = getString(variable, defaultValue);
            property = property.replace("{" + variable + "}", variableValue);
        }
        return property;
    }

    private String getString(String propertyName) {
        return getString(propertyName, null);
    }

    private int getInt(String propertyName, String defaultValue) {
        return Integer.parseInt(getString(propertyName, defaultValue));
    }

    private int getInt(String propertyName) {
        return Integer.parseInt(getString(propertyName, null));
    }

    private double getDouble(String propertyName) {
        return Double.parseDouble(getString(propertyName, null));
    }

    private boolean getBoolean(String propertyName) {
        return Boolean.parseBoolean(getString(propertyName));
    }

    private String[] getCommaSeparatedValues(String propertyName) {
        return getString(propertyName).split(",");
    }

	public String[] getChildren() {
		return children;
	}

	public String getChildrenString() {
    	if (children.length == 1) return children[0];
    	String childrenStr = "";
		for (String child : children) {
			childrenStr = child + ",";
		}
		return childrenStr.substring(0, childrenStr.length() - 1);
	}

	public String getPredAggregationString() {
    	if (aggregateLexical) return "lex";
    	if (aggregateVerbPred) return "state";
    	return "no";
	}

	public String[] getTrainFileNames() {
		List<String> trainFiles = new ArrayList<>();
		for (String child : children) {
			trainFiles.add(trainTestDir + "/train." + child);
		}
		return trainFiles.toArray(new String[trainFiles.size()]);
	}
	public String[] getTestFileNames() {
		List<String> testFiles = new ArrayList<>();
		for (String child : children) {
			testFiles.add(trainTestDir + "/test." + child);
		}
		return testFiles.toArray(new String[testFiles.size()]);
	}

	public int getHmmStates() {
        return hmmStates;
    }
    public String getTempDir() {
        return tempDir + File.separator;
    }
	String getTrainTestDir() {
        return trainTestDir;
    }
	public String getGorpDir() {
        return dataDir + File.separator + gorpDir;
    }
	public String getHmmDir() {
        return hmmDir;
    }
	public int getNumRoundsSupervised() {
        return numRoundsSupervised;
    }
	public int getNumRoundsUnsupervised() {
        return numRoundsUnsupervised;
    }
	public int getNumSeedNouns() {
        return numSeedNouns;
    }
    public String getSeedNounFile() {
        return seedNounFile;
    }
    public String getAnimacyNounFile() {
        return animacyNounFile;
    }
    public int getKnownNounThreshold() {
        return knownNounThreshold;
    }
    public String getHmmTrainFile() {
        return hmmTrainFile;
    }
    public String getHmmFunctionWordsFile() {
        return hmmFunctionWordsFile;
    }
    public int getFunctionWordState() {
        return functionWordState;
    }
    public boolean removeQMarks() {
        return removeQMarks;
    }
    public boolean useRandSeeds() {
        return randSeeds;
    }
    public boolean seedPluralsMatch() {
        return seedPluralsMatch;
    }
    public MultiplePredicates getMultiplePredicates() {
        return multiplePredicates;
    }
    public Scoring getScoring() {
        return scoring;
    }
    public Supervision getSupervision() {
        return supervision;
    }
    public Tags getTags() {
        return instance.tags;
    }
    public boolean isCompoundNouns() {
        return compoundNouns;
    }
    public Labels getTestLabels() {
		return testLabels;
	}
    public Labels getTrainLabels() {
        return trainLabels;
    }
	public boolean aggregateVerbPred() {
		return aggregateVerbPred;
	}
    public boolean aggregateRandVerbPred() {
        return aggregateRandVerbPred;
    }
    public boolean aggregateLexical() {
        return aggregateLexical;
    }
    public boolean retagHMM() {
        return fixHMM;
    }
    public double getRetagThreshold() {
        return fixHMMThreshold;
    }
    public NounThreshold.ThresholdType getThresholdType() {
        return thresholdType;
    }
    public boolean createReports() {
        return createReports;
    }
    public boolean randomTrainTestSplits() {
        return randomTrainTestSplits;
    }
    public boolean aggregateAnimacy() {
        return aggregateAnimacy;
    }

    public static void setTags(Tags tags) {
        instance.tags = tags;
    }
}
