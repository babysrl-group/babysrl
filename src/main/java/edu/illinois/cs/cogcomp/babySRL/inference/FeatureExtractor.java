package edu.illinois.cs.cogcomp.babySRL.inference;

import edu.illinois.cs.cogcomp.babySRL.core.RandomVerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.ArrayUtilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Responsible for feature generation and management of the lexicon (feature-to-index mapping)
 */
public class FeatureExtractor {
    private static Logger logger = LoggerFactory.getLogger(FeatureExtractor.class);

    private Map<String, Integer> feat2Ind;
    private Map<Integer, String> ind2Feat;
    private int globalIndex;

    private FeatureTypes[] featureTypes;

    public FeatureExtractor(FeatureTypes[] featureTypes, boolean readFromFile) {
        this.featureTypes = featureTypes;
        if (readFromFile) {
            ind2Feat = readLexicon();
            feat2Ind = new HashMap<>();
            for (int index : ind2Feat.keySet())
                feat2Ind.put(ind2Feat.get(index), index);
            globalIndex = ind2Feat.size() + 1;
        }
        else {
            feat2Ind = new HashMap<>();
            ind2Feat = new HashMap<>();
            globalIndex = 1;

            // Encode all labels first
            for (String label : BabySRLCorpus.allLabels)
                encode(label);
        }
    }

    /**
     * A range of labels, used by the {@link Classifier} as {@code targets} during training/testing.
     * We assume that the labels are serially encoded during initialisation.
     *
     * @return The range of encoded labels.
     */
    public Pair<Integer, Integer> getLearningTargets() {
        String firstLabel = BabySRLCorpus.allLabels[0];
        String lastLabel = BabySRLCorpus.allLabels[BabySRLCorpus.allLabels.length-1];

        return new Pair<>(encode(firstLabel), encode(lastLabel));
    }

    public String getFeatureString(BabySRLArgumentInstance instance) {
        String featsStr = "";
        for (FeatureTypes type : featureTypes)
            featsStr += "," + encode(getFeature(instance, type));
        return featsStr;
    }

    public int[] getFeatureArray(BabySRLArgumentInstance instance) {
        List<Integer> feats = new ArrayList<>();
        for (FeatureTypes type : featureTypes)
            feats.add(encode(getFeature(instance, type)));
        return ArrayUtilities.asIntArray(feats);
    }

    private String getFeature(BabySRLArgumentInstance instance, FeatureTypes type) {
        String featStr = "";
        switch (type) {
            case WORD:
                featStr += "word:" + instance.getWordsFeature();
                break;
            case PREDICATE:
                featStr += "pred:" + instance.getPredicateFeature();
                break;
            case NOUN_PATTERN:
                featStr += "npat:" + instance.getNounPatternFeature();
                break;
			case NOUN_COUNT:
				featStr += "ncnt:" + instance.getSentence().getArgumentInstances().size();
				break;
            case VERB_POSITION:
                featStr += "vpos:" + instance.getVerbPositionFeature();
                break;
            case TRUE_RANDOM_VERB_POSITION:
                featStr += "vpos-true-r:" + instance.getRandomVerbPositionFeature(RandomVerbIdentifier.RAND_TRUE);
                break;
            case FUNC_RANDOM_VERB_POSITION:
                featStr += "vpos-func-r:" + instance.getRandomVerbPositionFeature(RandomVerbIdentifier.RAND_FUNCTION_WORDS);
                break;
            case NOUN_RANDOM_VERB_POSITION:
                featStr += "vpos-noun-r:" + instance.getRandomVerbPositionFeature(RandomVerbIdentifier.RAND_NOUNS);
                break;
        }
        return featStr;
    }

    public int encode(String featureString) {
        if (feat2Ind.containsKey(featureString))
            return feat2Ind.get(featureString);

        // Add the feature
        feat2Ind.put(featureString, globalIndex);
        ind2Feat.put(globalIndex, featureString);

        globalIndex++;

        return globalIndex - 1;
    }

    public String decode(int index) {
        if (ind2Feat.containsKey(index))
            return ind2Feat.get(index);
        return null;
    }

    public void saveLexicon() {
        try {
            FileOutputStream fos = new FileOutputStream(getLexiconFileName());
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(ind2Feat);
            oos.close();
            fos.close();
            // Write translated version of SNoW's network file
            List<String> translatedNetLines = new ArrayList<>();
            if (!IOUtils.exists(getNetworkFileName())) return;
            for (String line : LineIO.read(getNetworkFileName())) {
                if (line.startsWith("target")) {
                    int start = 7;
                    int end = line.indexOf(' ', start);
                    int target = Integer.parseInt(line.substring(start, end));
                    translatedNetLines.add(line.substring(0, start) + decode(target) + line.substring(end));
                }
                else if (!line.isEmpty()) {
                    String[] splits = line.split("\\s+");
                    String featStr = splits[4];
                    int feature;
                    if (featStr.length() > 5)
                        feature = -1;
                    else feature = Integer.parseInt(featStr);
                    String counts = splits[6];
                    String updates = splits[7];
                    String weight = splits[8];
                    translatedNetLines.add(decode(feature) + "\t" + counts + "\t" + updates + "\t"  + weight);
                }
                else translatedNetLines.add(line);
            }
            LineIO.write(getNetworkFileName()+".txt", translatedNetLines);
        }
        catch (IOException e) {
            System.err.println("Unable to save " + getLexiconFileName());
        }
    }

	@SuppressWarnings("unchecked")
    private Map<Integer, String> readLexicon() {
        Map<Integer, String> ind2Feat = new HashMap<>();
        try {
            FileInputStream fis = new FileInputStream(getLexiconFileName());
            ObjectInputStream ois = new ObjectInputStream(fis);
            ind2Feat = (HashMap) ois.readObject();
            ois.close();
            fis.close();
        }catch(IOException | ClassNotFoundException ioe) {
            logger.debug("Unable to read {}", getLexiconFileName());
        }
        return ind2Feat;
    }

    public static void removeFiles(FeatureTypes[] featureTypes) {
        String signature = BabySRLProperties.getInstance().getTempDir();
        signature += getSignature(featureTypes);
        try {
            IOUtils.rm(signature + ".lex");
            IOUtils.rm(signature + ".net");
            IOUtils.rm(signature + ".net.txt");
        } catch (IOException e) {
            logger.debug("Unable to delete {} files", signature);
        }
    }

    private String getLexiconFileName() {
        String signature = BabySRLProperties.getInstance().getTempDir();
        signature += getSignature(featureTypes);
        return signature + ".lex";
    }

    public String getNetworkFileName() {
        String signature = BabySRLProperties.getInstance().getTempDir();
        signature += getSignature(featureTypes);
        return signature + ".net";
    }

    private static String getSignature(FeatureTypes[] featureTypes) {
        String separator = "-";
        String signature = "";
        for (FeatureTypes type : featureTypes) {
            switch (type) {
                case WORD:
                    signature += separator + "word" ;
                    break;
                case PREDICATE:
                    signature += separator + "pred";
                    break;
                case NOUN_PATTERN:
                    signature += separator + "npat";
                    break;
				case NOUN_COUNT:
					signature += separator + "ncnt";
					break;
                case VERB_POSITION:
                    signature += separator + "vpos";
                    break;
                case TRUE_RANDOM_VERB_POSITION:
                    signature += separator + "vpos-true-r";
                    break;
                case FUNC_RANDOM_VERB_POSITION:
                    signature += separator + "vpos-func-r";
                    break;
                case NOUN_RANDOM_VERB_POSITION:
                    signature += separator + "vpos-noun-r";
                    break;
            }
        }
        // Remove initial separator
        return signature.substring(1);
    }

    public FeatureTypes[] getFeatureTypes() {
        return featureTypes;
    }
}
