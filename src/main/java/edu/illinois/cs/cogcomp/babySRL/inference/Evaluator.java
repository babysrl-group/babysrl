package edu.illinois.cs.cogcomp.babySRL.inference;

import edu.illinois.cs.cogcomp.babySRL.core.RandomVerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.core.SupervisedExperimentResults;
import edu.illinois.cs.cogcomp.babySRL.core.SupervisedResultRecord;
import edu.illinois.cs.cogcomp.babySRL.core.UnsupervisedExperimentResults;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLArgumentInstance;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLSentence;
import edu.illinois.cs.cogcomp.babySRL.data.DataHandler;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;
import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.experiments.EvaluationRecord;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.stats.Counter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Evaluator {
	private static BabySRLProperties properties = BabySRLProperties.getInstance();

	public static void testSupervisedClassifier(Classifier classifier, DataHandler testParser,
												SupervisedExperimentResults supervisedExperimentResults)
			throws IOException, InterruptedException {
		EvaluationRecord eval = Trainer.test(testParser, classifier);
		supervisedExperimentResults.addTestSetResult(eval);
	}

    public static void evaluateGorp(int round, int seedNouns, Classifier classifier, SupervisedExperimentResults supervisedExperimentResults) {
        BabySRLCorpus.Tags originalTagSetting = properties.getTags();
        // Need to always set the Tags to gold since we assume knowledge of all nouns in the Gorp setting
        BabySRLProperties.setTags(BabySRLCorpus.Tags.gold);
		String prefix = "Agorps";
        String[] patterns = {"A0", "A1", "A2", "Other"};
        getPatternResults(round, seedNouns, classifier, prefix, patterns, supervisedExperimentResults);

        prefix = "AgorpB";
        patterns = new String[]{"A0A1", "A0A2", "A0A0", "A1A1", "A0Other", "OtherA1"};
        getPatternResults(round, seedNouns, classifier, prefix, patterns, supervisedExperimentResults);

        prefix = "ABgorp";
        patterns = new String[]{"A0A1", "A0A2", "A0A0", "A1A1", "A0Other", "OtherA1"};
        getPatternResults(round, seedNouns, classifier, prefix, patterns, supervisedExperimentResults);
        BabySRLProperties.setTags(originalTagSetting);
    }

    private static void getPatternResults(int round, int seedNouns, Classifier classifier, String prefix, String[] patterns,
                                          SupervisedExperimentResults supervisedExperimentResults) {
        String gorpDir = properties.getGorpDir();
        BabySRLCorpus testCorpus = new BabySRLCorpus();

        // Create file for error analysis
        String outFile = properties.getTempDir();
        try {
            for (String childName : properties.getChildren()) {
                String file = gorpDir + File.separator + prefix + "." + childName;
                testCorpus.readFile(file, properties.getMultiplePredicates());
                outFile += classifier.getName() + "-" + IOUtils.stripFileExtension(IOUtils.getFileName(file)) + "-errors.txt";
            }
            IOUtils.rm(outFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Counter<String> counter = new Counter<>();
        List<BabySRLSentence> sentences = testCorpus.getSentences();
        for (BabySRLSentence sentence : sentences) {
            String pattern = "";
            // Evaluate with unique agent constraint
            if (properties.getSupervision() == BabySRLCorpus.Supervision.animacy) {
                @SuppressWarnings("unchecked")
                // Use the gold pred/arg structure for the gorp sentences
                Pair<String, Double>[] predictions = new Pair[sentence.getArgumentInstances(true).size()];
                List<BabySRLArgumentInstance> argumentInstances = sentence.getArgumentInstances(true);
                for (int i = 0; i < argumentInstances.size(); i++) {
                    BabySRLArgumentInstance testInstance = argumentInstances.get(i);
                    predictions[i] = classifier.predictionWithScore(testInstance);
                }
                // If there are more than one agent predictions, choose the highest scoring one
                Trainer.testWithConstraint(predictions);
                for (Pair<String, Double> p : predictions) {
                    String prediction = p.getFirst();
                    if (!prediction.matches("A[012]")) prediction = "Other";
                    pattern += prediction;
                }
            }
            else {
                // Use the gold pred/arg structure for the gorp sentences
                for (BabySRLArgumentInstance example : sentence.getArgumentInstances(true)) {
                    String prediction = classifier.prediction(example);
                    if (!prediction.matches("A[012]")) prediction = "Other";
                    pattern += prediction;
                }
            }
            counter.incrementCount(pattern);
            String child = properties.getChildrenString();
            boolean isCorrectPattern;
            switch (prefix) {
                case "Agorps":
                    isCorrectPattern = pattern.equals("A0");
                    break;
                case "ABgorp":
                    isCorrectPattern = pattern.equals("A0A0");
                    break;
                default:
                    isCorrectPattern = pattern.equals("A0A1");
                    break;
            }
            supervisedExperimentResults.addResultRecord(new SupervisedResultRecord(seedNouns, child,
                    classifier.getName(), properties.getSupervision().name(), properties.getPredAggregationString(),
                    prefix, sentence.getSentStr(), isCorrectPattern, round));

            if (properties.createReports()) {
                try {
                    if (outFile.contains("Agorps") && !pattern.equals("A0"))
                        LineIO.append(outFile, sentence.getSentStr() + "\t" + pattern);
                    else if (outFile.contains("ABgorp") && !pattern.equals("A0A0"))
                        LineIO.append(outFile, sentence.getSentStr() + "\t" + pattern);
                    else if (outFile.contains("AgorpB") && !pattern.equals("A0A1"))
                        LineIO.append(outFile, sentence.getSentStr() + "\t" + pattern);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        for (String pattern : patterns) {
			double numerator = counter.getCount(pattern);
			double d = numerator / counter.getTotal() * 100;
			supervisedExperimentResults.addGorpResult(prefix+pattern, d);
		}
    }

    public static void evaluateUnsupervised(DataHandler parser, UnsupervisedExperimentResults results) {
        EvaluationRecord nounRecord = new EvaluationRecord();
        EvaluationRecord verbRecord = new EvaluationRecord();
        Map<String, EvaluationRecord> randomRecord = new HashMap<>();
        Counter<String> sentCandidateCounter = new Counter<>();
        Counter<String> sentGoldPredInCandidatesCounter = new Counter<>();
        Counter<String> maxArgsFoundCounter = new Counter<>();
        Counter<String> heuristicTieCounter = new Counter<>();

        for (BabySRLSentence s : parser.getTestSentences()) {
            evaluateSentenceUnsupervised(s, nounRecord, verbRecord, randomRecord, sentCandidateCounter,
                    sentGoldPredInCandidatesCounter, maxArgsFoundCounter, heuristicTieCounter);
        }
        results.addNounIdentifierResult(nounRecord);
        results.addVerbIdentifierResult(verbRecord);
        results.addVerbIdentifierRandResult(randomRecord);
        results.addAvgCandidatesNum(sentCandidateCounter.mean());
        results.addGoldInCandidatesNum(sentGoldPredInCandidatesCounter.mean());
        results.addMaxArgsFound(maxArgsFoundCounter.mean());
        results.addHeuristicTieCounter(heuristicTieCounter.mean());
    }

    public static void evaluateIncremental(BabySRLSentence s, UnsupervisedExperimentResults results,
                                           EvaluationRecord nounRecord, EvaluationRecord verbRecord,
                                           Map<String, EvaluationRecord> randomRecord) {
        evaluateSentenceUnsupervised(s, nounRecord, verbRecord, randomRecord, null, null, null, null);
        results.addNounIdentifierResult(nounRecord);
        results.addVerbIdentifierResult(verbRecord);
        results.addVerbIdentifierRandResult(randomRecord);
    }

    private static void evaluateSentenceUnsupervised(BabySRLSentence s,
                                                     EvaluationRecord nounRecord,
                                                     EvaluationRecord verbRecord,
                                                     Map<String, EvaluationRecord> randomRecord,
                                                     Counter<String> sentCandidateCounter,
                                                     Counter<String> sentGoldPredInCandidatesCounter,
                                                     Counter<String> maxArgsFoundCounter,
                                                     Counter<String> heuristicTieCounter) {
        for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
            // Argument scores
            if (s.isGoldArgument(wordInd)) nounRecord.incrementGold();
            if (s.isPredictedNoun(wordInd)) {
                nounRecord.incrementPredicted();
                if (s.isGoldArgument(wordInd)) nounRecord.incrementCorrect();
            }
            // Predicate scores
            if (s.isGoldVerbPred(wordInd)) verbRecord.incrementGold();
            if (s.isPredictedVerbPred(wordInd)) {
                verbRecord.incrementPredicted();
                if (s.isGoldVerbPred(wordInd)) verbRecord.incrementCorrect();
            }
            // Random predicate scores
            scoreRandom(s, randomRecord, wordInd, RandomVerbIdentifier.RAND_TRUE);
            scoreRandom(s, randomRecord, wordInd, RandomVerbIdentifier.RAND_FUNCTION_WORDS);
            scoreRandom(s, randomRecord, wordInd, RandomVerbIdentifier.RAND_NOUNS);
        }
        if (sentCandidateCounter != null) {
            sentCandidateCounter.incrementCount(s.getSentStr(), s.candidateNum);
        }
        if (sentGoldPredInCandidatesCounter != null) {
            sentGoldPredInCandidatesCounter.incrementCount(s.getSentStr(), s.goldPredAmongCandidates);
        }
        if (maxArgsFoundCounter != null) {
            maxArgsFoundCounter.incrementCount(s.getSentStr(), s.maxArgsFound);
        }
        if (heuristicTieCounter != null) {
            heuristicTieCounter.incrementCount(s.getSentStr(), s.heuristicTie);
        }
    }

    private static void scoreRandom(BabySRLSentence s, Map<String, EvaluationRecord> randomRecords,
                                    int wordInd, String identifier) {
        if (!randomRecords.containsKey(identifier)) randomRecords.put(identifier, new EvaluationRecord());
        EvaluationRecord record = randomRecords.get(identifier);
        if (s.isGoldVerbPred(wordInd)) record.incrementGold();
        if (s.isPredictedVerbPredRandom(identifier, wordInd)) {
            record.incrementPredicted();
            if (s.isGoldVerbPred(wordInd)) record.incrementCorrect();
        }
    }

    @SuppressWarnings("unused")
    private static void scoreHMM(BabySRLCorpus corpus) throws FileNotFoundException {
        Counter<String> goldTagCounts = new Counter<>();
        Counter<String> hmmTagCounts = new Counter<>();
        Counter<String> coocCounts = new Counter<>();

        for (BabySRLSentence s : corpus.getSentences()) {
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String goldTag = s.getGoldTag(wordInd);
                String hmmTag = s.getHmmTag(wordInd);
                if (hmmTag.equals("-1")) continue;
                goldTagCounts.incrementCount(goldTag);
                hmmTagCounts.incrementCount(hmmTag);
                coocCounts.incrementCount(hmmTag + "-" + goldTag);
            }
        }

        //Many-to-one mapping
        manyToOne(corpus, hmmTagCounts, goldTagCounts, coocCounts);
    }

    private static void manyToOne(BabySRLCorpus corpus, Counter<String> hmmTagCounts,
                           Counter<String> goldTagCounts, Counter<String> coocCounts) {
        Map<String, String> manyToOneMap = new HashMap<>();
        for (String hmmTag : hmmTagCounts.items()) {
            String mostFreqGoldTag = null;
            double mostFreqGoldTagCount = 0;
            for (String goldTag : goldTagCounts.items()) {
                String key = hmmTag+"-"+goldTag;
                if (!coocCounts.contains(key)) continue;
                if (coocCounts.getCount(key) > mostFreqGoldTagCount) {
                    mostFreqGoldTag = goldTag;
                    mostFreqGoldTagCount = coocCounts.getCount(key);
                }
            }
            manyToOneMap.put(hmmTag, mostFreqGoldTag);
        }

        int correctCount = 0;
        int total = 0;
        for (BabySRLSentence s : corpus.getSentences()) {
            for (int wordInd = 0; wordInd < s.getWordCount(); wordInd++) {
                String hmmTag = s.getHmmTag(wordInd);
                if (hmmTag.equals("-1")) continue;
                String goldTag = s.getGoldTag(wordInd);
                if (manyToOneMap.get(hmmTag).equalsIgnoreCase(goldTag)) correctCount++;
                total++;
            }
        }
        double m1 = (correctCount/(double)total)*100;
        System.out.println("M-1:\t" + m1);
    }
}
