package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.babySRL.core.RandomVerbIdentifier;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Supervision;
import edu.illinois.cs.cogcomp.babySRL.data.BabySRLCorpus.Tags;
import edu.illinois.cs.cogcomp.babySRL.utils.BabySRLProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Used by LBJava to access the sentence level features of each candidate.
 */
public class BabySRLArgumentInstance {
    private BabySRLSentence sentence;
    private int token;
    private int predIndex;
    private Map<String, Integer> randPredIndexMap;

    public BabySRLArgumentInstance(BabySRLSentence sentence, int token, int predIndex, Map<String, Integer> randPredIndex) {
        this.sentence = sentence;
        this.token = token;
        this.predIndex = predIndex;
        // If the random predicate index is `null` it means we are returning gold predicate/argument instances
        if (randPredIndex == null) {
            this.randPredIndexMap = new HashMap<>();
            this.randPredIndexMap.put(RandomVerbIdentifier.RAND_TRUE, predIndex);
            this.randPredIndexMap.put(RandomVerbIdentifier.RAND_FUNCTION_WORDS, predIndex);
            this.randPredIndexMap.put(RandomVerbIdentifier.RAND_NOUNS, predIndex);
        }
        else {
            this.randPredIndexMap = randPredIndex;
        }
    }

    public String getWordsFeature() {
        return sentence.getWord(token);
    }

    public String getNounPatternFeature() {
        if (BabySRLProperties.getInstance().getTags() == Tags.gold)
            return sentence.getNounPattern(token);
        return sentence.getNounPatternInduced(token);
    }

    public String getVerbPositionFeature() {
		if (predIndex < 0) return "Null";
        return (predIndex > token) ? "Before" : "After";
    }

    public String getRandomVerbPositionFeature(String identifier) {
        int randPredIndex = randPredIndexMap.get(identifier);
        if (randPredIndex < 0) return "Null";
        return (randPredIndex > token) ? "Before" : "After";
    }

    public String getPredicateFeature() {
        return sentence.getWord(predIndex);
    }

    public String getLabel() {
        Supervision supervision = BabySRLProperties.getInstance().getSupervision();
        String label = "";
        switch (supervision) {
            case gold:
                // veridical supervision (gold SRL labels)
                label = sentence.getGoldArg(token, predIndex);
                break;
            case animacy:
                // animate nouns get an A0 label (unless more than one, in which case the classifier gets to vote)
                label = sentence.getAnimacyLabel(token);
                break;
            case noisy10:
            case noisy30:
                // a noisy version of gold, where the labels disappear with probability p = .1 or .3
                label = sentence.getNoisyLabel(token, predIndex, supervision);
                break;
            case noisy10animacy:
                // a noisy version of gold, where all the labels except animacy nouns disappear with probability p = .1
                if (sentence.getAnimacyLabel(token).equals("UNK"))
                    label = sentence.getNoisyLabel(token, predIndex, Supervision.noisy10);
                else label = sentence.getGoldArg(token, predIndex);
                break;
            default:
                System.err.println("Wrong supervision type given");
                System.exit(-1);
        }
        // Remove the BIO prefix (if any)
        if (label.startsWith("B") || label.startsWith("I"))
            return label.substring(label.indexOf("-") + 1);
        return label;
    }

    public BabySRLSentence getSentence() {
        return sentence;
    }

    public int getToken() {
        return token;
    }

    public int getPredIndex() {
        return predIndex;
    }
}
