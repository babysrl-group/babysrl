package edu.illinois.cs.cogcomp.babySRL.data;

import edu.illinois.cs.cogcomp.core.io.LineIO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

/**
 * Used to create the "gorp" sentences for testing the BabySRL.
 * Using a fixed list of animate nouns to balance the A0/A1 settings.
 */
public class BabySRLTestCorpus {
    public static void createGorpSents(String... children) throws IOException {
        for (String child : children) {
            List<String> nounsAnimate = new ArrayList<>();
            List<String> nounsInanimate = new ArrayList<>();
            Map<String, Boolean> definiteNouns = new HashMap<>();
            for (String line : readFileWithComments("data/gorp/noun-lists/" + child + ".txt")) {
                String[] splits = line.split("\t");
                String word = splits[0];
                if (splits[1].equals("1"))
                    nounsAnimate.add(word);
                else nounsInanimate.add(word);
                definiteNouns.put(word, splits[3].equals("1"));
            }

            String[] randAnimateNouns = new String[10];

            // Fix the random seed to get the same lists every time
            Random random = new Random(1);
            for (int i = 0; i < 10; i++) {
//                randAnimateNouns[i] = nounsAnimate.get(random.nextInt(nounsAnimate.size()));
                // If we don't need random picks (because we only have 10 nouns) use this:
                randAnimateNouns[i] = nounsAnimate.get(i);
            }

            List<String> aGorpsLines = new ArrayList<>();
            List<String> aGorpBLines = new ArrayList<>();
            List<String> aBGorpLines = new ArrayList<>();
            List<String> aGorpBCLines = new ArrayList<>();
            List<String> aBGorpCLines = new ArrayList<>();

            // Get a balanced list of animate nouns by creating all possible
            // combinations of the randomly selected nouns with themselves
            // To get the unbalanced set, just replace the two for loops with
            // one, ranging from 0 to 100, and pick a new random String from
            // nounsAnimate (like in cNoun)
            for (int i = 0; i < 10; i++) {
                // A = animate
                String aNoun = randAnimateNouns[i];
                int index = 0;
                // Construct the sentence "The A gorps" one line at a time, choosing whether should add a determiner
                if (definiteNouns.get(aNoun)) {
                    aGorpsLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                    aGorpsLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                }
                else
                    aGorpsLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                aGorpsLines.add("B-V\tO\t"+(index++)+"\tB-VP\tVBP\tgorps\t-\tx\tgorp\t-a\tH");
                aGorpsLines.add("O\tO\t"+(index)+"\tO\t.\t.\t-C\tx\tgorp\t-a\tA");
                aGorpsLines.add("");

                for (int j = 0; j < 10; j++) {
                    // Avoid the diagonal (NB: this will create 90 sentences)
                    if (i == j) continue;

                    // B = animate
                    String bNoun = randAnimateNouns[j];

                    // C = inanimate
                    String cNoun = nounsInanimate.get(random.nextInt(nounsInanimate.size()));

                    index = 0;
                    // Construct the sentence "The A gorp the B" one line at a time
                    if (definiteNouns.get(aNoun)) {
                        aGorpBLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                        aGorpBLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    }
                    else
                        aGorpBLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    aGorpBLines.add("B-V\tO\t"+(index++)+"\tB-VP\tVBP\tgorp\t-\tx\tgorp\t-a\tH");
                    if (definiteNouns.get(bNoun)) {
                        aGorpBLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tA");
                        aGorpBLines.add("B-A1\tO\t"+(index++)+"\tI-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tA");
                    }
                    else
                        aGorpBLines.add("B-A1\tO\t"+(index++)+"\tB-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tA");
                    aGorpBLines.add("O\tO\t"+(index)+"\tO\t.\t.\t-C\tx\tgorp\t-a\tA");
                    aGorpBLines.add("");

                    index = 0;
                    // Construct the sentence "The A and the B gorp" one line at a time
                    if (definiteNouns.get(aNoun)) {
                        aBGorpLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                        aBGorpLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    }
                    else
                        aBGorpLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    aBGorpLines.add("O\tO\t"+(index++)+"\tI-NP\tCC\tand\t-\tx\tgorp\t-a\tB");
                    if (definiteNouns.get(bNoun)) {
                        aBGorpLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                        aBGorpLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tB");
                    }
                    else
                        aBGorpLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tB");
                    aBGorpLines.add("B-V\tO\t"+(index++)+"\tB-VP\tVBP\tgorp\t-\tx\tgorp\t-a\tH");
                    aBGorpLines.add("O\tO\t"+(index)+"\tO\t.\t.\t-C\tx\tgorp\t-a\tA");
                    aBGorpLines.add("");

                    index = 0;
                    // Construct the sentence "The A gorp the B the C" one line at a time
                    if (definiteNouns.get(aNoun)) {
                        aGorpBCLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                        aGorpBCLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    }
                    else
                        aGorpBCLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    aGorpBCLines.add("B-V\tO\t"+(index++)+"\tB-VP\tVBP\tgorp\t-\tx\tgorp\t-a\tH");
                    if (definiteNouns.get(bNoun)) {
                        aGorpBCLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tA");
                        aGorpBCLines.add("B-A2\tO\t"+(index++)+"\tI-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tA");
                    }
                    else
                        aGorpBCLines.add("B-A2\tO\t"+(index++)+"\tB-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tA");
                    // C nouns are always definite
                    aGorpBCLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\t-\tx\tgorp\t-a\tA");
                    aGorpBCLines.add("B-A1\tO\t"+(index++)+"\tI-NP\tNN\t" + cNoun + "\t-\tN\tgorp\t-a\tA");
                    aGorpBCLines.add("O\tO\t"+(index)+"\tO\t.\t.\t-C\tx\tgorp\t-a\tA");
                    aGorpBCLines.add("");

                    index = 0;
                    // Construct the sentence "The A and the B gorp the C" one line at a time
                    if (definiteNouns.get(aNoun)) {
                        aBGorpCLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                        aBGorpCLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    }
                    else
                        aBGorpCLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + aNoun + "\t-\tN\tgorp\t-a\tB");
                    aBGorpCLines.add("O\tO\t"+(index++)+"\tI-NP\tCC\tand\t-\tx\tgorp\t-a\tB");
                    if (definiteNouns.get(bNoun)) {
                        aBGorpCLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\tO-\tx\tgorp\t-a\tB");
                        aBGorpCLines.add("B-A0\tO\t"+(index++)+"\tI-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tB");
                    }
                    else
                        aBGorpCLines.add("B-A0\tO\t"+(index++)+"\tB-NP\tNN\t" + bNoun + "\t-\tN\tgorp\t-a\tB");
                    aBGorpCLines.add("B-V\tO\t"+(index++)+"\tB-VP\tVBP\tgorp\t-\tx\tgorp\t-a\tH");
                    // C nouns are always definite
                    aBGorpCLines.add("O\tO\t"+(index++)+"\tB-NP\tDT\tthe\t-\tx\tgorp\t-a\tA");
                    aBGorpCLines.add("B-A1\tO\t"+(index++)+"\tI-NP\tNN\t" + cNoun + "\t-\tN\tgorp\t-a\tA");
                    aBGorpCLines.add("O\tO\t"+(index)+"\tO\t.\t.\t-C\tx\tgorp\t-a\tA");
                    aBGorpCLines.add("");
                }
            }
            LineIO.write("data/gorp/Agorps." + child, aGorpsLines);
            LineIO.write("data/gorp/AgorpB." + child, aGorpBLines);
            LineIO.write("data/gorp/ABgorp." + child, aBGorpLines);
            LineIO.write("data/gorp/AgorpBC." + child, aGorpBCLines);
            LineIO.write("data/gorp/ABgorpC." + child, aBGorpCLines);
        }
    }

    private static List<String> readFileWithComments(String fileName) throws FileNotFoundException {
        Scanner scanner = new Scanner(new FileInputStream(fileName));

        List<String> list = new ArrayList<>();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            if (line.startsWith("#")) continue;
            list.add(line);
        }
        scanner.close();
        return list;
    }

    public static void main(String[] args) throws IOException {
        createGorpSents("adam-a0a1", "eve-a0a1", "sarah-a0a1");
    }
}
