package edu.illinois.cs.cogcomp.babySRL.inference.snow;

import edu.illinois.cs.cogcomp.babySRL.inference.FeatureTypes;

/**
 * A {@link SnowClassifier} with the NPat features (word, predicate-word, noun pattern)
 *
 * @author Christos Christodoulopoulos
 */
public class NPatClassifierSnow extends SnowClassifier {

    public NPatClassifierSnow() {
        super("npat", FeatureTypes.WORD, FeatureTypes.PREDICATE, FeatureTypes.NOUN_PATTERN);
    }
}
