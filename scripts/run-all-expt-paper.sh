#!/bin/bash

function changeConfig {
    if [[ `uname` == 'Darwin' ]]; then
        sed -i '' "$1" config/babySRL.properties
    else
        sed -i "$1" config/babySRL.properties
    fi
}

## Supervision type ("gold", "animacy", "noisy30")
function setSupervision {
    changeConfig "s/Supervision = .*/Supervision = $1/"
}

## Aggregation strategy ("state", "lex", "no")
function setAggr {
if [ $1 = "state" ]; then
    changeConfig "s/AggregateVerbPred = .*/AggregateVerbPred = true/"
    changeConfig "s/AggregateRandVerbPred = .*/AggregateRandVerbPred = true/"
    changeConfig "s/AggregateLexical = .*/AggregateLexical = false/"
elif [ $1 = "lex" ]; then
    changeConfig "s/AggregateVerbPred = .*/AggregateVerbPred = true/"
    changeConfig "s/AggregateRandVerbPred = .*/AggregateRandVerbPred = true/"
    changeConfig "s/AggregateLexical = .*/AggregateLexical = true/"
else
    changeConfig "s/AggregateVerbPred = .*/AggregateVerbPred = false/"
    changeConfig "s/AggregateRandVerbPred = .*/AggregateRandVerbPred = false/"
    changeConfig "s/AggregateLexical = .*/AggregateLexical = false/"
fi
}

hmm="noFix"
cv="noCV"

mkdir experiments
mkdir experiments-r
for supervision in gold noisy30 animacy; do
    setSupervision ${supervision}
    for aggr in state lex no; do
        setAggr ${aggr}
        for child in adam eve sarah; do
            echo "Running $supervision $aggr for $child"
            changeConfig "s/Children = .*/Children = ${child}/"
            mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.Main" -Dexec.args="runSupervisedExperiment true" > ${child}.txt
        done
        experimentDir=experiments/${supervision}-${cv}-${aggr}Aggr-${hmm}
        mkdir -p ${experimentDir}
        mv *.txt temp ${experimentDir}

        # Consolidate the results in a csv file for R
        mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.utils.ConsolidateResults" -Dexec.args="${experimentDir}"
        cp ${experimentDir}/all.txt experiments-r/${experimentDir:12}.csv
    done
done

# Consolidate the results from all the runs
mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.utils.ConsolidateAllRunsResults"

# Consolidate the individual record trial results
head -1 experiments/`ls experiments | head -1`/temp/results-record.tsv > experiments-r/all-results-record.tsv
head -1 experiments/`ls experiments | head -1`/temp/classifier-weights.tsv > experiments-r/all-classifier-weights.tsv
for dir in `ls experiments`; do
  sed '1d' experiments/${dir}/temp/results-record.tsv >> experiments-r/all-results-record.tsv
  sed '1d' experiments/${dir}/temp/classifier-weights.tsv >> experiments-r/all-classifier-weights.tsv
done
