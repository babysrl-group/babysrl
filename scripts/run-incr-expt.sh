#!/bin/bash

# Incremental version of the supervised experiments over multiple seed nouns
for seeds in 5 10 15 20 25 30 35 40 45 50 55 60 65 70 75; do
    for child in adam eve sarah; do
        sed -i "s/Children = .*/Children = ${child}/" config/babySRL.properties
        mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.Main" -Dexec.args="runIncremental $seeds" > ${child}-${seeds}.txt
    done
done