#!/bin/bash

## Runs the BabySRL supervised experiment with the current configuration settings for all 3 children

for child in adam eve sarah; do
    sed -i "s/Children = .*/Children = ${child}/" config/babySRL.properties
    mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.Main" -Dexec.args="runSupervisedExperiment true" > ${child}.txt
done