#!/bin/bash

# Prefix for experiment dir (set in run-all-expt.sh)
prefix=$1
# Directory to store the Shiny R app's data
shinyAppDir=$2

mkdir -p ${shinyAppDir}
for directory in `find experiments -name "${prefix}-*"`; do
    mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.utils.ConsolidateResults" -Dexec.args="$directory"
    cp ${directory}/all.txt ${shinyAppDir}/${directory:12}.csv
done
