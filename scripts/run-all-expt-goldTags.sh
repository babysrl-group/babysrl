#!/bin/bash
# Require the input argument
if [ "$#" -ne 1 ]; then
  echo "You need to specify the type of supervision."
  echo "Enter one of the following: "
  echo "    gold, animacy, noisy10, noisy30, noisy10animacy"
  echo "See 'Supervision' in config/babySRL.properties for a description"
  exit
fi

sed -i "s/Tags = .*/Tags = gold/" config/babySRL.properties

## Directory prefix to discriminate the experiments
prefix=$1
if [ ${prefix} = "animacyAggr" ]; then
    sed -i "s/Supervision = .*/Supervision = animacy/" config/babySRL.properties
    sed -i "s/AggregateAnimacy = .*/AggregateAnimacy = true/" config/babySRL.properties
elif [ ${prefix} = "animacy" ]; then
    sed -i "s/Supervision = .*/Supervision = animacy/" config/babySRL.properties
    sed -i "s/AggregateAnimacy = .*/AggregateAnimacy = false/" config/babySRL.properties
else
    sed -i "s/Supervision = .*/Supervision = $prefix/" config/babySRL.properties
fi

## Set cross-validation ("crossval", "")
function setCrossval {
if [ $1 = "crossval" ]; then
    sed -i "s/RandomTrainTestSplits = .*/RandomTrainTestSplits = true/" config/babySRL.properties
else
    sed -i "s/RandomTrainTestSplits = .*/RandomTrainTestSplits = false/" config/babySRL.properties
fi
}

mkdir experiments
for cv in crossval noCV; do
    setCrossval ${cv}
    echo "Running $cv"
    for child in adam eve sarah; do
       sed -i "s/Children = .*/Children = ${child}/" config/babySRL.properties
       mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.Main" -Dexec.args="runSupervisedExperiment true" > ${child}.txt
    done
    mkdir experiments/gold-${prefix}-${cv}/
    mv *.txt temp experiments/gold-${prefix}-${cv}/
    cp config/babySRL.properties experiments/gold-${prefix}-${cv}/
done