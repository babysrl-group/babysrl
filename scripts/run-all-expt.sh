#!/bin/bash
# Require the input argument
if [ "$#" -ne 1 ]; then
  echo "You need to specify the type of supervision."
  echo "Enter one of the following: "
  echo "    gold, animacy, noisy10, noisy30, noisy10animacy"
  echo "See 'Supervision' in config/babySRL.properties for a description"
  exit
fi

## Directory prefix to discriminate the experiments gold, animacy, noisy10, noisy30, noisy10animacy
prefix=$1
if [ ${prefix} = "animacyAggr" ]; then
    echo "Aggregated animacy feedback"
    sed -i "s/Supervision = .*/Supervision = animacy/" config/babySRL.properties
    sed -i "s/AggregateAnimacy = .*/AggregateAnimacy = true/" config/babySRL.properties
elif [ ${prefix} = "animacy" ]; then
    echo "Animacy feedback"
    sed -i "s/Supervision = .*/Supervision = animacy/" config/babySRL.properties
    sed -i "s/AggregateAnimacy = .*/AggregateAnimacy = false/" config/babySRL.properties
else
    echo "$prefix feedback"
    sed -i "s/Supervision = .*/Supervision = $prefix/" config/babySRL.properties
fi

## Aggregation strategy ("state", "lex", "no")
function setAggr {
if [ $1 = "state" ]; then
    sed -i "s/AggregateVerbPred = .*/AggregateVerbPred = true/" config/babySRL.properties
    sed -i "s/AggregateRandVerbPred = .*/AggregateRandVerbPred = true/" config/babySRL.properties
    sed -i "s/AggregateLexical = .*/AggregateLexical = false/" config/babySRL.properties
elif [ $1 = "lex" ]; then
    sed -i "s/AggregateVerbPred = .*/AggregateVerbPred = true/" config/babySRL.properties
    sed -i "s/AggregateRandVerbPred = .*/AggregateRandVerbPred = true/" config/babySRL.properties
    sed -i "s/AggregateLexical = .*/AggregateLexical = true/" config/babySRL.properties
else
    sed -i "s/AggregateVerbPred = .*/AggregateVerbPred = false/" config/babySRL.properties
    sed -i "s/AggregateRandVerbPred = .*/AggregateRandVerbPred = false/" config/babySRL.properties
    sed -i "s/AggregateLexical = .*/AggregateLexical = false/" config/babySRL.properties
fi
}

## HMM fix ("fix10", "noFix")
function setHMMFix {
if [ $1 = "fix10" ]; then
    sed -i "s/FixHMM = .*/FixHMM = true/" config/babySRL.properties
else
    sed -i "s/FixHMM = .*/FixHMM = false/" config/babySRL.properties
fi
}

## Set cross-validation ("crossval", "noCV")
function setCrossval {
if [ $1 = "crossval" ]; then
    sed -i "s/RandomTrainTestSplits = .*/RandomTrainTestSplits = true/" config/babySRL.properties
else
    sed -i "s/RandomTrainTestSplits = .*/RandomTrainTestSplits = false/" config/babySRL.properties
fi
}

mkdir experiments
for aggr in state lex no; do
    setAggr ${aggr}
    for hmm in fix10 noFix; do
        setHMMFix ${hmm}
        for cv in crossval noCV; do
            setCrossval ${cv}
            echo "Running $aggr $hmm $cv"
            for child in adam eve sarah; do
               sed -i "s/Children = .*/Children = ${child}/" config/babySRL.properties
               mvn exec:java -Dexec.mainClass="edu.illinois.cs.cogcomp.babySRL.Main" -Dexec.args="runSupervisedExperiment true" > ${child}.txt
            done
            mkdir -p experiments/${prefix}-${cv}-${aggr}Aggr-${hmm}/
            mv *.txt temp experiments/${prefix}-${cv}-${aggr}Aggr-${hmm}/
            cp config/babySRL.properties experiments/${prefix}-${cv}-${aggr}Aggr-${hmm}/
        done
    done
done
